import React from 'react'

interface AddProps {
    width?: string
    height?: string
    fill?: string
}

export const Add = ({ width, height, fill }: AddProps) => {
    return (
        <svg
            width={width || 'auto'}
            height={height || 'auto'}
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <rect x="2" y="6" width="12" height="1.5" rx="0.75" fill={fill || 'black'} />
            <rect x="2" y="10" width="12" height="1.5" rx="0.75" fill={fill || 'black'} />
            <rect x="2" y="14" width="8" height="1.5" rx="0.75" fill={fill || 'black'} />
            <rect x="12" y="14" width="10" height="1.5" rx="0.75" fill={fill || 'black'} />
            <rect
                x="16.25"
                y="19.75"
                width="10"
                height="1.5"
                rx="0.75"
                transform="rotate(-90 16.25 19.75)"
                fill={fill || 'black'}
            />
        </svg>
    )
}
