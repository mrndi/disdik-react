export interface iconProps {
    width?: number;
    height?: number;
    fill?: string;
    size?: number;
    color?: string;
    stroke?: string;
}
