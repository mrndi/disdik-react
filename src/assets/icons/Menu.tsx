import React from 'react'

interface MenuProps {
    width?: number
    height?: number
    stroke?: string
}

export const Menu = ({ width, height, stroke }: MenuProps) => {
    return (
        <svg
            width={width || 'auto'}
            height={height || 'auto'}
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M2 18.3333H22M2 5H22H2ZM2 11.6667H22H2Z"
                stroke={stroke || 'black'}
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </svg>
    )
}
