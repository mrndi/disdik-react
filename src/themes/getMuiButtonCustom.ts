import { Theme } from "@emotion/react";
import { ComponentsOverrides, ComponentsVariants } from "@mui/material";
import { ComponentsProps } from "@mui/material/styles/props"

type variantProps = {
    props: {
        variant: 'contained' | 'outlined' | 'text',
        color: 'primary' | 'secondary',
        size: 'large' | 'medium' | 'small' | 'small',
    }
    style: any
}


export const getMuiButtonCustom = (colors: any) => {
    const defaultButtonPrimaryStyle = {
        backgroundColor: colors.teal_500,
        boxShadow: 'none',
        color: 'white',
        svg: {
            fill: 'white',
        },
        '&:hover': {
            backgroundColor: colors.teal_300,
            boxShadow: 'none',
            color: 'white',
        },
    }

    const defaultButtonSecondaryStyle = {
        backgroundColor: colors.cyan_500,
        boxShadow: 'none',
        color: 'white',
        svg: {
            fill: 'white',
        },
        '&:hover': {
            backgroundColor: colors.cyan_300,
            boxShadow: 'none',
            color: 'white',
        },
    }

    const defaultButtonOutlinedPrimaryStyle = {
        boxShadow: 'none',
        color: colors.teal_500,
        border: `2px solid ${colors.teal_500}`,
        '&:hover': {
            backgroundColor: colors.grey_50,
            boxShadow: 'none',
            border: `2px solid ${colors.teal_500}`,
        },
    }

    const defaultButtonOutlinedSecondaryStyle = {
        boxShadow: 'none',
        color: colors.cyan_500,
        border: `2px solid ${colors.cyan_500}`,
        '&:hover': {
            backgroundColor: colors.grey_50,
            boxShadow: 'none',
            border: `2px solid ${colors.cyan_500}`,
        },
    }

    const defaultButtonSizeStyle = {
        lg: {
            height: '48px',
            fontSize: 18,
        },
        md: {
            height: '40px',
            fontSize: 16,
        },
        sm: {
            height: '32px',
            fontSize: 14,
        },
        xs: {
            height: '24px',
            fontSize: 12,
        },
    }
    

    const variants: variantProps[] = [ 
            // contained primary
            
            {
                props: { variant: 'contained', color: "primary", size: 'large' },
                style: {
                    ...defaultButtonPrimaryStyle,
                    ...defaultButtonSizeStyle.lg,
                },
            },
            {
                props: { variant: 'contained', color: 'primary', size: 'medium' },
                style: {
                    ...defaultButtonPrimaryStyle,
                    ...defaultButtonSizeStyle.md,
                },
            },
            {
                props: { variant: 'contained', color: 'primary', size: 'small' },
                style: {
                    ...defaultButtonPrimaryStyle,
                    ...defaultButtonSizeStyle.sm,
                },
            },
            {
                props: { variant: 'contained', color: 'primary', size: 'small' },
                style: {
                    ...defaultButtonPrimaryStyle,
                    ...defaultButtonSizeStyle.xs,
                },
            },
            // contained secondary
            {
                props: { variant: 'contained', color: 'secondary', size: 'large' },
                style: {
                    ...defaultButtonSecondaryStyle,
                    ...defaultButtonSizeStyle.lg,
                },
            },
            {
                props: { variant: 'contained', color: 'secondary', size: 'medium' },
                style: {
                    ...defaultButtonSecondaryStyle,
                    ...defaultButtonSizeStyle.md,
                },
            },
            {
                props: { variant: 'contained', color: 'secondary', size: 'small' },
                style: {
                    ...defaultButtonSecondaryStyle,
                    ...defaultButtonSizeStyle.sm,
                },
            },
            {
                props: {
                    variant: 'contained',
                    color: 'secondary',
                    size: 'small',
                },
                style: {
                    ...defaultButtonSecondaryStyle,
                    ...defaultButtonSizeStyle.xs,
                },
            },
            // outlined primary
            {
                props: { variant: 'outlined', color: 'primary', size: 'large' },
                style: {
                    ...defaultButtonOutlinedPrimaryStyle,
                    ...defaultButtonSizeStyle.lg,
                },
            },
            {
                props: { variant: 'outlined', color: 'primary', size: 'medium' },
                style: {
                    ...defaultButtonOutlinedPrimaryStyle,
                    ...defaultButtonSizeStyle.md,
                },
            },
            {
                props: { variant: 'outlined', color: 'primary', size: 'small' },
                style: {
                    ...defaultButtonOutlinedPrimaryStyle,
                    ...defaultButtonSizeStyle.sm,
                },
            },
            {
                props: { variant: 'outlined', color: 'primary', size: 'small' },
                style: {
                    ...defaultButtonOutlinedPrimaryStyle,
                    ...defaultButtonSizeStyle.xs,
                },
            },
            // outlined secondary
            {
                props: { variant: 'outlined', color: 'secondary', size: 'large' },
                style: {
                    ...defaultButtonOutlinedSecondaryStyle,
                    ...defaultButtonSizeStyle.lg,
                },
            },
            {
                props: { variant: 'outlined', color: 'secondary', size: 'medium' },
                style: {
                    ...defaultButtonOutlinedSecondaryStyle,
                    ...defaultButtonSizeStyle.md,
                },
            },
            {
                props: { variant: 'outlined', color: 'secondary', size: 'small' },
                style: {
                    ...defaultButtonOutlinedSecondaryStyle,
                    ...defaultButtonSizeStyle.sm,
                },
            },
            {
                props: { variant: 'outlined', color: 'secondary', size: 'small' },
                style: {
                    ...defaultButtonOutlinedSecondaryStyle,
                    ...defaultButtonSizeStyle.xs,
                },
            },
        ]
    

    return {
        defaultProps: undefined,
        styleOverrides: undefined,
        variants: variants
    }
}
