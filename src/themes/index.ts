import { createTheme } from '@mui/material/styles'
import { getMuiButtonCustom } from './getMuiButtonCustom'

// for naming colors, please use ref from https://tailwindcss.com/docs/customizing-colors or https://chir.ag/projects/name-that-color
export const colors = {
    // primary
    purple_100: '#F5EFFF',
    purple_200: '#A49DF4',
    purple_300: '#756CDC',
    purple_500: '#544AC2', // brand color
    purple_700: '#2F24A5',
    purple_900: '#0D0378',
    // secondary
    froly_500: '#F26B6E', // brand color
    froly_300: '#F89698',
    // accent colors for background, card, chart, graph
    amethyst_500: '#9658D0',
    burntsienna_500: '#ED6868',
    burntsienna_50: '#FDF7F7',
    blue_500: '#547DE7',
    blue_50: '#F7FBFD',
    brilliantrose_500: '#FA61A1',
    brilliantrose_50: '#FDF7FB',
    caribbean_500: '#01D59F',
    caribbean_50: '#F7FDFB',
    cerulean_500: '#01AFD5',
    cerulean_50: '#F7FCFD',
    cherub_50: '#fae2e2',
    jaffa_500: '#FF9900',
    jaffa_400: '#F2994A',
    jaffa_300: '#F99E5C',
    jaffa_200: '#F8C16E',
    jaffa_100: '#F5E197',
    jaffa_50: '#FDFAF7',
    jungle_500: '#27AE60',
    lilac_100: '#F9F7FD',
    green_100: '#E8F6F0',
    green_500: '#49CC90',
    red_100: '#FAE7E7',
    red_500: '#F93E3E',
    // grey variant
    grey_900: '#333333',
    grey_700: '#4F4F4F',
    grey_500: '#828282',
    grey_300: '#BDBDBD',
    grey_200: '#FCFCFC',
    grey_100: '#E0E0E0',
    grey_50: '#F2F2F2',
    zircon_500: '#F6F8FF', // background
    //sky variant
    sky_100: '#cffafe',
    sky_300: '#7dd3fc',
    sky_400: '#38bdf8',
    sky_500: '#0ea5e9',
    sky_700: '#0369a1',
    sky_900: '#81D4FA',
    // cyan variant
    cyan_100: '#e0fdfa',
    cyan_200: '#a5f3fc',
    cyan_300: '#67e8f9',
    cyan_400: '#22d3ee',
    cyan_500: '#06b6d4',
    cyan_700: '#4DD0E1',
    cyan_900: '#26C6DA',
    // teal variant
    teal_100: '#ccfbf1',
    teal_200: '#99f6e4',
    teal_300: '#5eead4',
    teal_400: '#2dd4bf',
    teal_500: '#14b8a6',
    teal_700: '#0f766e',
    teal_900: '#134e4a',


}

// accentColors for use iteration array of chart or graph. This should be as same as with mainTheme.pallete.accent bellow
export const accentColors = [
    colors.blue_500,
    colors.cerulean_500,
    colors.jaffa_500,
    colors.brilliantrose_500,
    colors.caribbean_500,
    colors.burntsienna_500,
    colors.amethyst_500,
    colors.jaffa_200,
    colors.jaffa_300,
    colors.jungle_500,
    colors.jaffa_400,
]

const defaultTextStyle = {
    color: colors.grey_900,
}

export const mainTheme = createTheme({
    palette: {
        mode: 'light',
        primary: {
            main: colors.teal_500,
            light: colors.teal_200,
            dark: colors.teal_700,
        }
    },

    typography: {
        fontFamily: ['Roboto', 'sans-serif'].join(','),

        h1: {
            ...defaultTextStyle,
            fontSize: 48,
            lineHeight: 'auto',
        },
        h2: {
            ...defaultTextStyle,
            fontSize: 36,
            lineHeight: 'auto',
        },
        h3: {
            ...defaultTextStyle,
            fontSize: 26,
            lineHeight: '36px',
        },
        h4: {
            ...defaultTextStyle,
            fontSize: 21,
            lineHeight: '32px',
        },
        h5: {
            ...defaultTextStyle,
            fontSize: 18,
            lineHeight: '27px',
        },
        h6: {
            ...defaultTextStyle,
            fontSize: 16,
            lineHeight: '21px',
        },
        body1: {
            ...defaultTextStyle,
            fontSize: 16,
            lineHeight: '26px',
        },
        body2: {
            ...defaultTextStyle,
            fontSize: 14,
            lineHeight: '21px',
        },
        subtitle1: {
            ...defaultTextStyle,
            fontSize: 14,
            lineHeight: '21px',
        },
        subtitle2: {
            ...defaultTextStyle,
            fontSize: 12,
            lineHeight: '17px',
        },

        button: {
            fontSize: 16,
            fontWeight: 400,
            textTransform: 'capitalize',
        },
        caption: {
            ...defaultTextStyle,
            fontSize: 16,
        },

        overline: {
            ...defaultTextStyle,
            fontSize: 12,
            lineHeight: '17px',
        },
    },

    components: {
        MuiButton: getMuiButtonCustom(colors),
    }
})

mainTheme.typography.body1 = {
    ...mainTheme.typography.body1,
    [mainTheme.breakpoints.down('md')]: {
        ...mainTheme.typography.body2,
    },
}

mainTheme.typography.button = {
    ...mainTheme.typography.button,
    [mainTheme.breakpoints.down('md')]: {
        fontSize: 14,
    },
}
