import { Router, Route, ReactLocation, createHashHistory, } from "@tanstack/react-location"
import { MutationCache, QueryCache, QueryClient, QueryClientProvider, } from "react-query";
import { SignIn, SignUp } from "./pages/auth/Auth"
import { Dashboard } from "./pages/Dashboard";
import toast, { Toaster } from "react-hot-toast";
import { createContext, useEffect, useState } from "react";
import { getToken, tokenPayload } from "./helper/AuthHelper";
import { AppLayout } from "./layout";
import { RequireAuth } from "./helper/RequireAuth";
import BookingList from "./pages/Booking/Index";
import { LinearProgress } from "@mui/material";
import { UploadDoc } from "./pages/Booking/UploadDoc";
import { getUser } from "./query/getUser";
import { api } from "./helper/ApiHelper";
import { Disposisi } from "./pages/Booking/Disposisi";

const routes: Route[] = [
  {
    path: "signin",
    element: <SignIn />,
  },
  {
    path: "signup",
    element: <SignUp />,
  },
  {
    path: "/*",
    element: <RequireAuth />,
    pendingElement: <LinearProgress />,
    children: [
      {
        path: "dashboard",
        element: <Dashboard />,
      },
      {
        path: "surat",
        children: [
          {
            path: "/",
            element: <BookingList />,
          },
          {
            path: "upload",
            element: <UploadDoc />,
          },
          {
            path: "disposisi",
            element: <Disposisi />,
          }
        ]
      },
    ]
  },
]


const hashHistory = createHashHistory()

const location = new ReactLocation({
  history: hashHistory,
})

const queryClient = new QueryClient({
  queryCache: new QueryCache({
    onError: (error: any) => {
      toast.error(error.message as string)
    },
  }),
  mutationCache: new MutationCache({
    onError: (error: any) => {
      toast.error(error.message as string)
    }
  })
})

interface UserContextType {
  id: string,
  username: string,
  name: string,
  roleId: number,
  subId: number,
  golonganId: number,
  gender: string,
  jabatan: string,
  noWA: string,
  subbidang: any
}

export const LoginContext = createContext({ login: false, setLogin: (value: boolean) => { } })
export const UserContext = createContext<UserContextType | null>(null)


function App() {
  const [login, setLogin] = useState<boolean>(false)
  const [user, setUser] = useState<UserContextType | null>(null)

  const getUserApi = async (id: string) => {
    const response: any = await api.get(`/user/${id}`)
    if (!response.ok) {
      throw new Error(response?.data?.message as string)
    }
    return setUser(response?.data?.data)
  }

  useEffect(() => {
    const token = getToken()
    if (token) {
      getUserApi(tokenPayload().id)
    }
  }, [login])


  return (
    <QueryClientProvider client={queryClient}>
      <LoginContext.Provider value={{ login, setLogin }}>
        <UserContext.Provider value={user}>
          <Router
            location={location}
            routes={routes}>
            <AppLayout />
            <Toaster />
          </Router>
        </UserContext.Provider>
      </LoginContext.Provider>
    </QueryClientProvider>
  )
}




export default App

