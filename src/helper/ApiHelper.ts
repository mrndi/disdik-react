import { create } from 'apisauce';


export const api = create({
    baseURL: import.meta.env.VITE_API_URL,
    headers: {
        'Content-Type': 'application/json',
    },
    timeout: 5000,
});

export const formApi = create({
    baseURL: import.meta.env.API_URL,
    headers: {
        'Content-Type': 'multipart/form-data',
    },
});


