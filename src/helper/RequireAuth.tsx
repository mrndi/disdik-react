import { Navigate, Outlet } from "@tanstack/react-location"
import { getToken } from "./AuthHelper"




export const RequireAuth = () => {
    const token = getToken()
    if (!token) {
        return <Navigate to="signin" />
    }
    return <Outlet />
}