import jwt_decode from 'jwt-decode'
import { api } from './ApiHelper'

interface IdecodedToken {
    iat: number,
    exp: number,
    payload: {
        name: string,
        username: string,
        id: string,
        subId: number,
        gender: string,
        roleId: number,
        golonganId: number,
        jabatan: string,
        noWA: string,
        subbidang: any,
    }
}



export const getToken = () => {
    const token = localStorage.getItem('signToken')!
    return token
}

export const setToken = (token: string) => {
    if (!token) {
        localStorage.removeItem('signToken')
    }
    localStorage.setItem('signToken', token)
}

export const isTokenExpired = () => {
    const decoded: IdecodedToken = jwt_decode(getToken())
    return decoded.iat > Date.now()
}


export const userSignOut = () => {
    localStorage.removeItem('signToken')
    return window.location.href = '/#/signin'
}

export const tokenPayload = () => {
    const token = getToken()
    const decoded: IdecodedToken = jwt_decode(token)
    return decoded.payload
}

// export const refreshToken = async () => {
//     const newToken: string = await getRefreshedToken() as any
//     setToken(newToken)
// }

// export const getRefreshedToken = () => {
//     return api.post('/signIn')
// }

api.axiosInstance.interceptors.response.use(
    (response) => {
        return response
    }
    , async (error) => {
        const originalRequest = error.config
        if (error.response.status === 401 && !originalRequest._retry) {
            userSignOut()
        }
        return Promise.reject(error)
    }
)

