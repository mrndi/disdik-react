const one = 1
const minusTwo = -2

export const convertDate = (str: number) => {
    const date = new Date(str),
        mnth = (`0 ${date.getMonth() + one}`).slice(minusTwo),
        day = (`0 ${date.getDate()}`).slice(minusTwo)
    return [date.getFullYear(), mnth, day].join('-')
}

export const convertDateToString = (str: string) => {
    const event = new Date(str)
    const getDay = event.toLocaleString('en-EN', { weekday: 'short' })
    const getDate = event.toLocaleString('en-EN', { day: 'numeric' })
    const getMonth = event.toLocaleString('en-EN', { month: 'short', year: 'numeric' })

    return `${getDay}, ${getDate} ${getMonth}`
}