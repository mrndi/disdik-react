import React from "react"



interface formProps {
    onSubmit: (e: React.FormEvent<HTMLFormElement>) => void
    onRoles: [{
        id: number,
        role: string
    }]
    onSubbidang: [{
        id: number,
        namaBidang: string
    }]
    onGolongan: [{
        id: number,
        golongan: string
    }]
}
export const RegisterForm = ({ onSubmit, onGolongan, onRoles, onSubbidang }: formProps) => {
    return (
        <form onSubmit={onSubmit} className="p-5 sm:px-10 md:p-10 md:px-8">
            <div className="form-group mb-6">
                <input type="text" name="name" className="form-control block
                                                w-full
                                                px-3
                                                py-1.5
                                                text-base
                                                font-normal
                                                text-gray-700
                                                bg-white bg-clip-padding
                                                border border-solid border-gray-300
                                                rounded
                                                transition
                                                ease-in-out
                                                m-0
                                                focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="Nama Lengkap" />
            </div>
            <div className="form-group mb-6">
                <input type="number" name="username" className="form-control block
                                                w-full
                                                px-3
                                                py-1.5
                                                text-base
                                                font-normal
                                                text-gray-700
                                                bg-white bg-clip-padding
                                                border border-solid border-gray-300
                                                rounded
                                                transition
                                                ease-in-out
                                                m-0
                                                focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="Nomor Induk Pegawai" />
            </div>
            <div className="form-group mb-6">
                <input type="password" name="password" className="form-control block
                                                    w-full
                                                    px-3
                                                    py-1.5
                                                    text-base
                                                    font-normal
                                                    text-gray-700
                                                    bg-white bg-clip-padding
                                                    border border-solid border-gray-300
                                                    rounded
                                                    transition
                                                    ease-in-out
                                                    m-0
                                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="Password" />
            </div>

            <div className="form-group mb-6">
                <select name="role" className="form-control block
                                                    w-full
                                                    px-3
                                                    py-1.5
                                                    text-base
                                                    font-normal
                                                    text-gray-700
                                                    bg-white bg-clip-padding
                                                    border border-solid border-gray-300
                                                    rounded
                                                    transition
                                                    ease-in-out
                                                    m-0
                                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="Pilih Jabatan">
                    <option value="">Pilih Jabatan</option>
                    {
                        onRoles?.map((role, index) => {
                            return (
                                <option key={index} value={role.id}>{role.role}</option>
                            )
                        })
                    }
                </select>
            </div>

            <div className="form-group mb-6">
                <select name="subbidang" className="form-control block
                                                    w-full
                                                    px-3
                                                    py-1.5
                                                    text-base
                                                    font-normal
                                                    text-gray-700
                                                    bg-white bg-clip-padding
                                                    border border-solid border-gray-300
                                                    rounded
                                                    transition
                                                    ease-in-out
                                                    m-0
                                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="Pilih Bidang">
                    <option value="">Pilih Bidang</option>
                    {
                        onSubbidang?.map((subbidang, index) => {
                            return (
                                <option key={index} value={subbidang.id}>{subbidang.namaBidang}</option>
                            )
                        })
                    }
                </select>
            </div>

            <div className="form-group mb-6">
                <select name="golongan" className="form-control block
                                                    w-full
                                                    px-3
                                                    py-1.5
                                                    text-base
                                                    font-normal
                                                    text-gray-700
                                                    bg-white bg-clip-padding
                                                    border border-solid border-gray-300
                                                    rounded
                                                    transition
                                                    ease-in-out
                                                    m-0
                                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="Pilih Golongan">
                    <option value="">Pilih Golongan</option>
                    {
                        onGolongan?.map((golongan, index) => {
                            return (
                                <option key={index} value={golongan.id}>{golongan.golongan}</option>
                            )
                        })
                    }
                </select>
            </div>

            <button type="submit" className="
                                                w-full
                                                px-6
                                                py-2.5
                                                bg-blue-600
                                                text-white
                                                font-medium
                                                text-xs
                                                leading-tight
                                                uppercase
                                                rounded
                                                shadow-md
                                                hover:bg-blue-700 hover:shadow-lg
                                                focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0
                                                active:bg-blue-800 active:shadow-lg
                                                transition
                                                duration-150
                                                ease-in-out">Sign up</button>
        </form>
    )

}
