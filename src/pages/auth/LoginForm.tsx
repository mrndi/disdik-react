import { Navigate, useNavigate } from '@tanstack/react-location'
import React from 'react'
import { LoginImage } from '../../assets/images'



interface formProps {
    onSubmit: (e: React.FormEvent<HTMLFormElement>) => void
    onLoad: boolean
}

export const LoginForm = ({ onSubmit, onLoad }: formProps) => {
    const navigate = useNavigate()
    return (
        <div className="container px-6 py-12 h-full">
            <div className="flex justify-center items-center flex-wrap h-full g-6 text-gray-800">
                <div className="md:w-8/12 lg:w-6/12 mb-12 md:mb-0">
                    <LoginImage width={500} height={250} className={"w-full"} />
                </div>
                <div className="md:w-8/12 lg:w-5/12 lg:ml-20">
                    <form onSubmit={onSubmit}>
                        <div className="mb-6">
                            <input
                                name="username"
                                type="text"
                                className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                placeholder="NIP"
                            />
                        </div>

                        <div className="mb-6">
                            <input
                                name="password"
                                type="password"
                                className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                placeholder="Password"
                            />
                        </div>

                        <div className="flex justify-between items-center mb-6">

                            <a
                                href="#!"
                                className="text-teal-600 hover:text-teal-700 focus:text-teal-700 active:text-teal-800 duration-200 transition ease-in-out"
                            >Forgot password?</a>
                        </div>

                        <button
                            type="submit"
                            className="inline-block px-7 py-3 bg-teal-600 text-white font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-teal-700 hover:shadow-lg focus:bg-teal-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-teal-800 active:shadow-lg transition duration-150 ease-in-out w-full"
                            data-mdb-ripple="true"
                            data-mdb-ripple-color="light"
                        >
                            {onLoad ? (
                                <div>
                                    <span className="text-white">Loading...</span>
                                </div>
                            ) : (

                                <>
                                    <span>Sign In</span>
                                </>
                            )}
                        </button>

                        <div
                            className="flex items-center my-4 before:flex-1 before:border-t before:border-gray-300 before:mt-0.5 after:flex-1 after:border-t after:border-gray-300 after:mt-0.5"
                        >
                            <p className="text-center font-semibold mx-4 mb-0">OR</p>
                        </div>

                        <button
                            type="button"
                            className="inline-block px-7 py-3 bg-white text-gray-600 font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-teal-700 hover:shadow-lg hover:text-white focus:bg-teal-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-teal-800 active:shadow-lg transition duration-150 ease-in-out w-full"
                            data-mdb-ripple="true"
                            data-mdb-ripple-color="light"
                            onClick={() => navigate({ to: '/signup', replace: true })}
                        >
                            Sign Up
                        </button>
                    </form>
                </div>
            </div>
        </div >
    )

}