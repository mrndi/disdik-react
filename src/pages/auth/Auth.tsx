import React from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import axios from "axios";
import { setToken } from "../../helper/AuthHelper";
import { LoginForm } from "./LoginForm";
import { RegisterForm } from "./RegisterForm";
import { useNavigate } from "@tanstack/react-location";
import { Toaster, toast } from "react-hot-toast";
import { api } from "../../helper/ApiHelper";
import { getGolongan } from "../../query/getGolongan";
import { getSubbidang } from "../../query/getSubbidang";
import { getRoles } from "../../query/getRoles";
import { LoginContext } from "../../App";


export const SignIn = () => {
    const navigate = useNavigate()
    const { login, setLogin } = React.useContext(LoginContext)

    const mutation = useMutation(['token'], async (data: any) => {
        const response: any = await api.post("/signin", data)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            onSuccess: (data: any) => {
                const token = data?.data?.token
                setToken(token)
                setLogin(true)
                toast.success("Login Success")
                navigate({ to: 'dashboard', replace: true })

            }
        })


    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const target = e.target as typeof e.target & {
            username: { value: string };
            password: { value: string };
        };
        const username = target.username.value;
        const password = target.password.value;
        mutation.mutate({ username, password })
    }

    return (
        <div className="flex justify-center mt-10">
            <Toaster />
            <LoginForm onSubmit={handleSubmit} onLoad={mutation.isLoading} />
        </div>
    );
}



export const SignUp = () => {
    const navigate = useNavigate()
    const queryClient = useQueryClient()

    const mutation = useMutation((data: any) => {
        const response: any = api.post("/signup", data)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    }, {
        onSuccess: (data: any) => {
            const token = data.data.token
            setToken(token)
            queryClient.invalidateQueries('user')
            navigate({ to: '/dashboard' })
        }
    })

    const roles = getRoles()
    const golongan = getGolongan()
    const subbidang = getSubbidang()

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const target = e.target as typeof e.target & {
            name: { value: string };
            username: { value: string };
            password: { value: string };
            role: { value: number };
            golongan: { value: number };
            subbidang: { value: number };
        };
        const name = target.name.value;
        const username = target.username.value;
        const password = target.password.value;
        const roleId = target.role.value;
        const golonganId = target.golongan.value;
        const subId = target.subbidang.value;

        mutation.mutate({ name, username, password, roleId, golonganId, subId })
    }

    return (
        <div className="flex justify-center mt-10 ">
            {mutation.isLoading ?
                ('loading...')
                : (
                    <>
                        {mutation.isError ? (
                            <div>{mutation.error as string}</div>
                        ) : (
                            <div>{mutation.isSuccess ?
                                'Success'
                                : <RegisterForm onSubmit={handleSubmit} onRoles={roles} onGolongan={golongan} onSubbidang={subbidang} />}</div>
                        )}
                    </>
                )}

        </div>
    )
}