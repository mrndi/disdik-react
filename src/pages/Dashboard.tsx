import React, { useContext, useState } from 'react'
import { Box, Typography, Paper, Stack, Button } from '@mui/material'
// import MyCalendar from 'components/Calendar'
// import { Overview } from 'containers/Overview'
import { Bell, Desk, Scan } from '../assets/icons'
import { Link, Navigate, useNavigate } from '@tanstack/react-location'
import { UserContext } from '../App'
import { IconAlert } from '../component/Alert'
import { colors } from '../themes'


export const Dashboard = () => {
    const userState = useContext(UserContext)
    const navigate = useNavigate()
    const isAdmin = true
    const [isQRCode, setIsQRCode] = useState(false)

    const AlertAction = () => (
        <Stack direction="row" justifyContent="flex-end" spacing={1} sx={{ width: '100%' }}>
            {isAdmin ? (
                <Button variant="outlined"
                    onClick={
                        () => navigate({ to: '/surat', replace: true, fromCurrent: true })
                    }
                >
                    <Typography sx={{ color: colors.teal_500 }}>Check Surat</Typography>
                </Button>
            ) : (
                <>
                    <Button variant="outlined" onClick={() => navigate({ to: 'surat/upload', replace: true, fromCurrent: true })}>
                        Scan QR Code
                    </Button>
                    <Button variant="outlined"
                        onClick={() => navigate({ to: '/surat', replace: true, fromCurrent: true })}
                    >
                        View My Book
                    </Button>
                </>
            )}
        </Stack>
    )

    return (
        <Box>
            <IconAlert
                icon={<Bell width={22} height={22} fill={colors.teal_500} />}
                text={
                    isAdmin
                        ? 'Ada 14 surat masuk'
                        : 'You have reserved a slot on 2nd Floor Desk 42 Full-day for today, please scan the QR code upon arrival.'
                }
                action={<AlertAction />}
            />
            <Paper
                sx={{
                    mb: 3,
                    p: 2,
                    display: { xs: 'block', sm: 'none' },
                }}
                elevation={0}
            >
                <Stack direction="row">
                    {/* <img src={MobileQR} alt="qrscan-mobile" style={{ marginRight: '24px' }} /> */}
                    <Box>
                        <Typography sx={{ mb: 1 }}>Mulai Berkirim Surat</Typography>
                        <Button
                            variant="contained"
                            color="secondary"
                            sx={{
                                backgroundColor: colors.teal_500,
                                py: 1,
                                px: 2,
                                borderRadius: '20px',
                                width: '175px',
                            }}
                            startIcon={<Desk width={22} height={22} outlined />}
                            onClick={() => navigate({ to: '/surat/upload' })}
                        >
                            <Typography sx={{ ml: 1, color: 'white !important' }}>Upload Surat</Typography>
                        </Button>
                    </Box>
                </Stack>
            </Paper>
            <Box
                sx={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    p: 0,
                    mb: 2,
                    display: { xs: 'none', sm: 'flex' },
                }}
            >
                <Box sx={{ flex: 1 }}>
                    <Typography variant="h4" component="h1" >
                        Welcome Back, {'budi' || ''}
                    </Typography>
                </Box>
                <Box
                    sx={{
                        flex: 1,
                        display: 'flex',
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        justifyContent: 'flex-end',
                        gap: 2,
                    }}
                >
                    <Button
                        variant="contained"
                        onClick={() => navigate({ to: '/surat/upload', replace: true, fromCurrent: true })}
                        startIcon={<Desk width={22} height={22} outlined />}
                        size="large"
                        sx={{ width: '200px' }}
                    // disabled={userState?.access ? false : true}
                    >
                        Kirim Surat
                    </Button>
                    <Button
                        variant="outlined"
                        onClick={() => setIsQRCode(!isQRCode)}
                        startIcon={<Scan width={24} height={24} fill={colors.teal_500} />}
                        size="large"
                        sx={{ width: '200px' }}
                        disabled={true}
                    >
                        Scan QR
                    </Button>
                </Box>
            </Box>
            <Paper
                elevation={0}
                sx={{
                    p: 3,
                    mb: 3,
                }}
            >
                {/* <Overview /> */}
            </Paper>
            <Paper elevation={0} sx={{ overflow: 'hidden' }}>
                <Box p={{ xs: 1, sm: 2 }}>
                    {/* <MyCalendar /> */}
                </Box>
            </Paper>
            {/* <ModalScanQR isQRCode={isQRCode} onClose={() => setIsQRCode(!isQRCode)} code="Hello World!" /> */}
        </Box>
    )
}