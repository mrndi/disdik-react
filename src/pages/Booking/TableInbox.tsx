import React, { useState } from 'react'
import { Link, Navigate } from '@tanstack/react-location'
import {
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Button,
    Box,
    Chip,
    Paper,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Typography,
} from '@mui/material'
import { Pagination } from '../../component/Pagination'
import { Popover } from '../../component/Form'
import { CircularProgress } from '../../component/Progress'
import { ActionPreviewDoc } from './ActionPreviewDoc'
import { ActionProses } from './ActionProses'
import { CssTableInbox, CssTableInboxMobile } from './style'
import { Available, Status, Date, Delete, Desk, Edit, Minus, Note, Repeat, VerticalMenu, Send, Envelope, Scan } from '../../assets/icons'
import { colors } from '../../themes'
import { dataWorkingDesk } from '../../fake-db/workingdesk'
import { TableHeader } from '../../containers/TableHeader'
import { convertDateToString } from '../../utils/convertDate'
import { FilePresentOutlined, HistoryEduOutlined } from '@mui/icons-material'
import { convertFileName } from '../../utils/converFileName'
import { StatusSurat } from './StatusSurat'

const startString = 0
const MAX_NOTE = 25

interface TableInboxProps {
    dataSurat: any
    isExternal?: boolean
    isOutbox?: boolean
}

interface prosesSurat {
    suratType: string
    id: string
    subId: string
    roleId: string
    penerimaId: string
    currentHolder: string
}

export const TableInbox = ({ dataSurat, isOutbox }: TableInboxProps) => {
    const [anchorEl, setAnchorEl] = useState() as any
    const [tipeSuratString, setTipeSuratString] = useState('')


    const [idSurat, setIdSurat] = useState('')
    const [subIdSurat, setSubIdSurat] = useState('')
    const [roleIdSurat, setRoleIdSurat] = useState('')
    const [penerimaIdSurat, setPenerimaIdSurat] = useState('')
    const [currentHolder, setCurrentHolder] = useState('')
    const [namaFile, setNamaFile] = useState('')
    const [isSelesai, setSelesai] = useState(false)

    //Modal
    const [isProcessDoc, setProcessDoc] = useState(false)
    const [isPreviewDoc, setPreviewDoc] = useState(false)



    //Popover: Repeat
    const [isPopoverRepeat, setOpenPopoverRepeat] = useState(false)
    const [PopoverContentRepeat] = useState() as any

    //Popover: Notes
    const [isPopoverNotes, setOpenPopoverNotes] = useState(false)
    const [PopoverNotes, setPopoverNotes] = useState() as any
    const onClickPopoverNotes = (e: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(e.currentTarget)
        setOpenPopoverNotes((prevState) => !prevState)
        setPopoverNotes(e.currentTarget.title)
    }

    const titles = [
        ...(!isOutbox ? [{ name: 'Nama Pengirim', alias: 'name' }] : []),
        { name: 'No Surat', alias: 'desk' },
        { name: 'Tanggal', alias: 'date' },
        { name: 'Perihal', alias: 'perihal' },
        { name: 'layanan', alias: 'layanan' },
        { name: 'Sifat', alias: 'sifat' },
        { name: 'Status', alias: 'status' },
        { name: 'Nama File', alias: 'file' },
        { name: 'Aksi', alias: 'action' },
    ]

    const handleSortChange = (alias: string, direction: string) => {
        console.log(alias, direction)
    }

    const handleProsesSurat = ({ id, subId, roleId, penerimaId, suratType, currentHolder }: prosesSurat) => {
        if (suratType === "suratmasuk") {
            console.log("proses surat masuk")
        }
        if (suratType === "suratkeluar") {
            console.log("proses surat keluar")
        }

        console.log(id, subId, roleId, suratType, currentHolder)
    }

    const handleInfoSurat = (
        id: string,
        subId: string,
        roleId: string,
        currentHolder: string,
        namaFile: string,
        isSelesai: boolean,
        tipeSurat: string,
        isPreview: boolean
    ) => {
        setCurrentHolder(currentHolder)
        setIdSurat(id)
        setSubIdSurat(subId)
        setRoleIdSurat(roleId)
        setNamaFile(namaFile)
        setSelesai(isSelesai)
        setTipeSuratString(tipeSurat)
        setPreviewDoc(isPreview)
    }

    return (
        <Box display={{ xs: 'none', sm: 'block' }}>
            <CssTableInbox>
                <TableContainer component={Paper} elevation={0} className="booking-table-workingdesk">
                    <Table>
                        <TableHeader titles={titles} onSortChange={handleSortChange} />
                        <TableBody>
                            {dataSurat === null ?
                                <TableRow>
                                    <TableCell />
                                    <TableCell />
                                    <TableCell>
                                        <Box sx={{ mt: 2, mb: 0.5, textAlign: 'center' }}>
                                            <CircularProgress text={'Loading...'} />
                                        </Box>
                                    </TableCell>
                                </TableRow>
                                : dataSurat?.map((data: any, idx: any) => (
                                    <TableRow key={idx} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                        {!isOutbox && (<TableCell>
                                            {data.users.name}
                                        </TableCell>)}
                                        <TableCell>{data.noSurat}</TableCell>
                                        <TableCell>{convertDateToString(data.tglPembuatan || data.tglMasuk)}</TableCell>
                                        <TableCell>
                                            <Typography className="text-checkin">{data.perihal}</Typography>
                                        </TableCell>
                                        <TableCell>
                                            <Typography className="text-checkin">{data.tujuan || data.layanan}</Typography>
                                        </TableCell>
                                        <TableCell>
                                            {data.sifat ? (
                                                <Box>
                                                    {data.sifat}
                                                </Box>
                                            ) : (
                                                <Box>
                                                    <Typography className="text-checkin">surat masuk</Typography>
                                                </Box>
                                            )}
                                        </TableCell>
                                        <TableCell>
                                            <StatusSurat status={data.status as string} />
                                        </TableCell>


                                        <TableCell>
                                            {convertFileName(data.namaFile).length > MAX_NOTE ? (
                                                <Box
                                                    onClick={onClickPopoverNotes}
                                                    title={convertFileName(data.namaFile)}
                                                    sx={{ cursor: 'pointer' }}
                                                >
                                                    {`${convertFileName(data.namaFile).substring(startString, MAX_NOTE)}...`}
                                                </Box>
                                            ) : (
                                                convertFileName(data.namaFile)
                                            )}
                                        </TableCell>
                                        <TableCell sx={{ textAlign: 'left' }}>
                                            <Stack direction="row" alignItems="center" justifyContent="flex-start">
                                                <Box
                                                    display="flex"
                                                    alignItems="center"
                                                    onClick={() => {
                                                        handleInfoSurat(
                                                            data.id,
                                                            data.holders.subId,
                                                            data.holders.roleId,
                                                            data.holders.id,
                                                            data.namaFile,
                                                            data.status === "Selesai" ? true : false,
                                                            data.layanan ? "suratmasuk" : "suratkeluar",
                                                            !isPreviewDoc
                                                        )
                                                    }}
                                                    sx={{ cursor: 'pointer' }}
                                                >
                                                    <FilePresentOutlined
                                                        width={40}
                                                        height={40}
                                                        color="primary" />
                                                </Box>
                                                <Box display="flex" alignItems="center" className="action-edit">
                                                    <Link to={`disposisi`} search={{
                                                        typeSurat: data.layanan ? "suratmasuk" : "suratkeluar",
                                                        id: data.id,
                                                    }} >
                                                        <HistoryEduOutlined width={40} height={40} color="primary" />
                                                    </Link>
                                                </Box>
                                            </Stack>
                                        </TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>

                <Pagination />

                <Popover
                    isShow={isPopoverRepeat}
                    onClose={() => setOpenPopoverRepeat(!isPopoverRepeat)}
                    anchorEl={anchorEl}
                    content={
                        <Box sx={{ maxWidth: '350px' }} p={2}>
                            <Typography variant="body2">{PopoverContentRepeat}</Typography>
                        </Box>
                    }
                />

                <Popover
                    isShow={isPopoverNotes}
                    onClose={() => setOpenPopoverNotes(!isPopoverNotes)}
                    anchorEl={anchorEl}
                    content={
                        <Box sx={{ maxWidth: '350px' }} p={2}>
                            <Typography variant="body2">{PopoverNotes}</Typography>
                        </Box>
                    }
                />

                {/* <ModalScanQR isQRCode={isQRCode} onClose={() => setIsQRCode(!isQRCode)} code="Hello World!" /> */}

                <ActionPreviewDoc
                    isShow={isPreviewDoc}
                    onClose={() => setPreviewDoc(!isPreviewDoc)}
                    data={`https://docs.google.com/gview?url=http://103.56.148.155:3000/static/file/${namaFile}&embedded=true`}
                    btnYes={() => setProcessDoc(!isProcessDoc)}
                    btnYesText={'Process'}
                    btnNoText={'Download'}
                    isDisabled={isSelesai}
                    btnNo={() => window.location.href = `http://103.56.148.155:3000/static/file/${namaFile}`}
                />

                <ActionProses
                    isShow={isProcessDoc}
                    onClose={(): void => setProcessDoc(!isProcessDoc)}
                    data={''}
                    btnYesText={''}
                    btnNoText={''}
                    btnYes={() => handleProsesSurat({ id: idSurat, subId: subIdSurat, roleId: roleIdSurat, penerimaId: penerimaIdSurat, currentHolder, suratType: tipeSuratString })}
                />
            </CssTableInbox>
        </Box >
    )
}

export const TableInboxMobile = () => {
    // const userState = useSelector((state) => state.user)
    const isAdmin = true

    //Popover
    const [anchorEl, setAnchorEl] = useState() as any
    const [isPopoverActions, setOpenPopoverActions] = useState(false)
    const onClickPopoverActions = (e: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(e.currentTarget)
        setOpenPopoverActions((prevState) => !prevState)
    }

    //Modal
    const [isQRCode, setIsQRCode] = useState(false)
    const [isProcessDoc, setProcessDoc] = useState(false)

    return (
        <Box display={{ xs: 'block', sm: 'none' }}>
            <CssTableInboxMobile>
                {dataWorkingDesk.map((data, idx) => (
                    <Box className="booking-accordion" key={idx}>
                        <Accordion className="booking-accordion-item">
                            <AccordionSummary
                                expandIcon={false}
                                id={`accordion-${data.id}`}
                                className="booking-accordion-item-summary"
                            >
                                <Stack direction="row" justifyContent="space-between" sx={{ width: '100%' }}>
                                    <Box className="box-title">
                                        <Stack direction="row" spacing={0.7} sx={{ mb: 1 }}>
                                            {isAdmin ? (
                                                <>
                                                    <Typography className="text-title limit">
                                                        {data.staff_name}
                                                    </Typography>
                                                    <Chip label="New" size="small" className="chip-new" />
                                                </>
                                            ) : (
                                                <Typography className="text-title">
                                                    {data.floor}, {data.desk}
                                                </Typography>
                                            )}
                                            {data.status === 'inactive' && data.checkin === 'yes' ? (
                                                <Available width={20} height={20} fill={colors.jungle_500} />
                                            ) : (
                                                ''
                                            )}
                                            {data.status === 'inactive' && data.checkin === 'no' ? (
                                                <Minus width={18} height={18} fill={colors.jaffa_400} />
                                            ) : (
                                                ''
                                            )}
                                        </Stack>

                                        <Stack direction="row" alignItems="center">
                                            <Stack direction="row" alignItems="flex-start" spacing={1} sx={{ mb: 0.5 }}>
                                                {isAdmin ? (
                                                    <>
                                                        <Box sx={{ mt: '2px', position: 'relative' }}>
                                                            <Desk
                                                                width={17}
                                                                height={17}
                                                                fill={colors.teal_500}
                                                                outlined
                                                            />
                                                        </Box>
                                                        <Typography className="text-summary">
                                                            {data.desk}, {data.floor}
                                                        </Typography>
                                                    </>
                                                ) : (
                                                    ''
                                                )}
                                                <Date width={18} height={18} fill={colors.teal_500} />
                                                <Typography className="text-summary">{data.date}</Typography>
                                            </Stack>
                                        </Stack>
                                    </Box>
                                    <Box className={'box-action' + (!isAdmin ? ' limit' : '')}>
                                        <Stack
                                            direction="row"
                                            alignItems="center"
                                            justifyContent={
                                                !isAdmin && data.status === 'active' ? 'space-between' : 'flex-end'
                                            }
                                        >
                                            {!isAdmin && data.status === 'active' ? (
                                                <Button
                                                    variant="outlined"
                                                    color="primary"
                                                    className="action-qrcode"
                                                    onClick={() => setIsQRCode(!isQRCode)}
                                                >
                                                    Scan Code
                                                </Button>
                                            ) : (
                                                ''
                                            )}
                                            {isAdmin || (data.status === 'active' && data.checkin === 'no') ? (
                                                <Box className="action-dots" onClick={onClickPopoverActions}>
                                                    <VerticalMenu width={30} height={30} fill={colors.teal_500} />
                                                </Box>
                                            ) : (
                                                ''
                                            )}
                                        </Stack>
                                    </Box>
                                </Stack>
                            </AccordionSummary>
                            <AccordionDetails>
                                {data.repeat_type !== 'None' ? (
                                    <Stack direction="row" alignItems="flex-start" spacing={0.7} sx={{ mb: 0.5 }}>
                                        <Box sx={{ pt: 0.5, ml: '-3px' }}>
                                            <Repeat width={23} height={24} fill={colors.teal_500} />
                                        </Box>
                                        <Box>
                                            <Typography className="text-summary">
                                                {data.repeat_type}, {data.repeat_notes}
                                            </Typography>
                                        </Box>
                                    </Stack>
                                ) : (
                                    ''
                                )}
                                <Stack direction="row" alignItems="flex-start" spacing={1.2}>
                                    <Box>
                                        <Note width={18} height={18} fill={colors.teal_500} />
                                    </Box>
                                    <Box>
                                        <Typography className="text-summary">{data.notes}</Typography>
                                    </Box>
                                </Stack>
                            </AccordionDetails>
                        </Accordion>
                    </Box>
                ))}

                <Box sx={{ mt: 2, mb: 0.5, textAlign: 'center' }}>
                    <CircularProgress text={'Loading...'} />
                </Box>

                <Popover
                    isShow={isPopoverActions}
                    onClose={() => setOpenPopoverActions(!isPopoverActions)}
                    anchorEl={anchorEl}
                    className="popover-actions"
                    style={{
                        '.MuiPaper-root': {
                            borderRadius: '10px',
                        },
                        '.MuiButton-text': {
                            justifyContent: 'flex-start',
                            padding: '10px 25px',
                            textTransform: 'capitalize',
                            color: colors.grey_900,
                            borderBottom: `1px solid ${colors.grey_50}`,
                            '&:last-child': {
                                borderBottom: 'unset',
                            },
                        },
                    }}
                    content={
                        <Stack direction="column" sx={{ width: '120px' }}>
                            <Button variant="text">Edit</Button>
                            <Button variant="text" onClick={() => setProcessDoc(!isProcessDoc)}>
                                Delete
                            </Button>
                        </Stack>
                    }
                />

            </CssTableInboxMobile>
        </Box>
    )
}
