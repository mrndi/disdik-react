import { useState } from 'react'
import { Button, Box, Typography, Stack, CardMedia } from '@mui/material'
import { Modal } from '../../component/Modal'

interface ActionProsesProps {
    isShow: boolean
    onClose: () => void
    data: string
    btnYesText: string
    btnNoText: string
    btnYes: () => void
    isDisabled?: boolean
    btnNo?: () => void
    penerima?: string
}

export const ActionProses = ({ isShow, onClose, data, btnYesText, btnNoText, btnYes, isDisabled, btnNo, penerima }: ActionProsesProps) => {
    return (
        <Modal
            isShow={isShow}
            onClose={onClose}
            sx={{ maxWidth: '100%', outline: 'none', height: '30em' }}
        >
            <Box>
                <Typography variant="h5" component="h2" sx={{ textAlign: 'center' }}>
                    Surat ini akan di proses ke :
                    <Typography variant="h5" component="h2" sx={{ textAlign: 'center', fontWeight: 'bold' }}>
                        {penerima}
                    </Typography>
                </Typography>

                <Typography variant="h6" component="h2" sx={{ textAlign: 'center' }}>
                    silahkan klik tombol proses untuk melanjutkan atau klik tombol Tolak untuk
                    mengembalikan surat ke sekertaris bidang anda
                </Typography>
            </Box>

            <Box>
                <Stack
                    direction="row"
                    justifyContent="center"
                    spacing={2}
                    sx={{
                        button: {
                            width: '150px',
                            maxWidth: '100%',
                        },
                    }}
                >
                    <Button variant="outlined" color="primary" size="large" onClick={btnNo}>
                        {btnNoText}
                    </Button>
                    <Button variant="contained" color="primary" size="large" onClick={btnYes} disabled={isDisabled}>
                        {btnYesText}
                    </Button>
                </Stack>
            </Box>
        </Modal>
    )
}
