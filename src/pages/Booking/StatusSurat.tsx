import { Autorenew, DraftsTwoTone, ThumbUpAltOutlined, HistoryOutlined, DoNotTouch } from "@mui/icons-material"
import { Stack, Typography } from "@mui/material"
import { Available } from "../../assets/icons"
import { colors } from "../../themes"

interface StatusSuratProps {
    status: string
}

export const StatusSurat = ({ status }: StatusSuratProps) => {
    switch (status) {
        case 'Draft':
            return (
                <Stack direction="row" alignItems="center">
                    <DraftsTwoTone color="warning" />
                    <Typography className="text-checkin">
                        Draft
                    </Typography>
                </Stack>)
            break;
        case 'Diterima':
            return (
                <Stack direction="row" alignItems="center">
                    <ThumbUpAltOutlined color="info" />
                    <Typography className="text-checkin">
                        Diterima
                    </Typography>
                </Stack>)
            break;

        case "Proses":
            return (
                <Stack direction="row" alignItems="center">
                    <Autorenew color="info" />
                    <Typography className="text-checkin">
                        Proses
                    </Typography>
                </Stack>)
            break;

        case "Disposisi":
            return (
                <Stack direction="row" alignItems="center">
                    <HistoryOutlined color="error" />
                    <Typography className="text-checkin">
                        Disposisi
                    </Typography>
                </Stack>)
            break;

        case "Ditolak":
            return (
                <Stack direction="row" alignItems="center">
                    <DoNotTouch color="error" />
                    <Typography className="text-checkin">
                        Ditolak
                    </Typography>
                </Stack>)
            break;

        case "Disetujui":
            return (
                <Stack direction="row" alignItems="center">
                    <Available width={20} height={20} fill={colors.jungle_500} />
                    <Typography className="text-checkin">
                        Disetujui
                    </Typography>
                </Stack>)
            break;

        case "Selesai":
            return (
                <Stack direction="row" alignItems="center">
                    <Available width={20} height={20} fill={colors.jungle_500} />
                    <Typography className="text-checkin">
                        Selesai
                    </Typography>
                </Stack>)
            break;

        default:
            return null
    }


}