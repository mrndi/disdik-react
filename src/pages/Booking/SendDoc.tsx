import React, { useContext } from 'react'
import { Autocomplete, Button, Box, Divider, Grid, MenuItem, Paper, TextField, Typography, Stack } from '@mui/material'
import KeyboardArrowDownRoundedIcon from '@mui/icons-material/KeyboardArrowDownRounded'
import { FileDrop, FormSelect, FileList, TextInput } from '../../component/Form'
import { Datepicker } from '../../component/Datepicker'
import { Arrow, Date, Envelope, User } from '../../assets/icons'
import { colors } from '../../themes'
import { dataStaff } from '../../fake-db/staff'
// import image from 'assets/images/2nd_Floor_Map.png'
import { UserContext } from '../../App'
// import FloorMap from './FloorMap'

interface SendDocProps {
    layanan: any
    onSubmit: () => void
    onCancel: () => void
    onDateChange: any
    dateSelected: any
    onFileDropChange: any
    files: any
    onLayananChange: any
    onPerihalChange: any
    onTujuanChange: any
    isSuratMasuk: boolean
    isBalas: boolean
}
const EMPTY_FILE = 0


export const SendDoc = ({
    onSubmit,
    onCancel,
    onFileDropChange,
    files,
    layanan,
    onLayananChange,
    onPerihalChange,
    onTujuanChange,
    isSuratMasuk,
    isBalas,
    onDateChange,
    dateSelected,
}: SendDocProps) => {
    const removeFile = (removedfile: any) => {
        files.filter((file: any) => file !== removedfile)
    }
    const userState = useContext(UserContext)
    const isAdmin = false

    return (
        <>
            <Grid container direction="row" sx={{ p: 0, mt: 2, ml: 0, mr: 0 }}>
                <Grid item xs={12} sm={6} pr={{ xs: 0, sm: 2 }} mb={{ xs: 4, sm: 0 }}>
                    <Typography variant="h5">Informasi Surat</Typography>
                    <Divider />
                    {/* Form */}
                    <Grid
                        container
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                        sx={{ p: 0, m: 0 }}
                    >
                        {isBalas ? (
                            <>
                                <Grid
                                    item
                                    xs={12}
                                    sx={{
                                        mt: 2,
                                        mb: 0.5,
                                    }}
                                >
                                    <Typography>Surat Balasan</Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Box display="flex" sx={{ position: 'relative' }}>
                                        <Box
                                            sx={{
                                                position: 'absolute',
                                                zIndex: '3',
                                                top: '12px',
                                                left: '15px',
                                            }}
                                        >
                                            <Envelope width={20} height={20} fill={colors.teal_500} />
                                        </Box>
                                        <Autocomplete
                                            id="select-staff"
                                            options={dataStaff}
                                            disablePortal
                                            autoHighlight
                                            getOptionLabel={(option) => option.label}
                                            popupIcon={
                                                <KeyboardArrowDownRoundedIcon
                                                    sx={{
                                                        fontSize: '22px',
                                                        color: colors.teal_500,
                                                    }}
                                                />
                                            }
                                            renderOption={(props, option) => (
                                                <Box
                                                    component="li"
                                                    sx={{
                                                        svg: { mr: 1 },
                                                    }}
                                                    {...props}
                                                >
                                                    <Envelope width={20} height={20} fill={colors.teal_500} />
                                                    {option.label}
                                                </Box>
                                            )}
                                            PaperComponent={({ children }) => (
                                                <Paper
                                                    elevation={2}
                                                    sx={{
                                                        '.MuiAutocomplete-listbox': {
                                                            p: 0,
                                                            '.MuiAutocomplete-option': {
                                                                p: '12px 15px',
                                                            },
                                                        },
                                                    }}
                                                >
                                                    {children}
                                                </Paper>
                                            )}
                                            sx={{
                                                width: '100%',
                                                '.MuiOutlinedInput-root': {
                                                    padding: '0 10px 0 38px',
                                                    color: colors.grey_900,
                                                    fontSize: '16px',
                                                    minHeight: '48px',
                                                },
                                            }}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    inputProps={{
                                                        ...params.inputProps,
                                                    }}
                                                    sx={{
                                                        backgroundColor: colors.lilac_100,
                                                        borderRadius: '4px',
                                                        padding: 0,
                                                    }}
                                                    placeholder="Pilih Surat Balasan"
                                                />
                                            )}
                                        />
                                    </Box>
                                </Grid>
                            </>
                        ) : (
                            ''
                        )}

                        {/* perihal */}
                        <Grid
                            item
                            xs={12}
                            sx={{
                                mt: 2,
                                mb: 0.5,
                            }}
                        >
                            <Typography>Perihal</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <TextInput placeholder='Perihal Surat' name='perihal' onChange={onPerihalChange} />
                        </Grid>

                        {/* layanan */}
                        {
                            isSuratMasuk
                                ?
                                (<>
                                    <Grid
                                        item
                                        xs={12}
                                        sx={{
                                            mt: 2,
                                            mb: 0.5,
                                        }}
                                    >
                                        <Typography>Layanan</Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormSelect
                                            defaultValue={'Select'}
                                            iconRight={<Arrow width={20} height={20} fill={colors.teal_500} direction="down" />}
                                            style={{ width: '100%' }}
                                            onChange={onLayananChange}
                                            value=''
                                        >
                                            {layanan &&
                                                layanan.map((data: any, idx: number) => (
                                                    <MenuItem key={idx} value={data.namaBidang as string}>
                                                        {data.namaBidang}
                                                    </MenuItem>
                                                ))}
                                        </FormSelect>
                                    </Grid>
                                </>)
                                : <>
                                    <Grid
                                        item
                                        xs={12}
                                        sx={{
                                            mt: 2,
                                            mb: 0.5,
                                        }}
                                    >
                                        <Typography>Tujuan</Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormSelect
                                            defaultValue={'Select'}
                                            iconRight={<Arrow width={20} height={20} fill={colors.teal_500} direction="down" />}
                                            style={{ width: '100%' }}
                                            onChange={onTujuanChange}
                                            value=''
                                        >
                                            {layanan &&
                                                layanan.map((data: any, idx: number) => (
                                                    <MenuItem key={idx} value={data.namaBidang as string}>
                                                        {data.namaBidang}
                                                    </MenuItem>
                                                ))}
                                        </FormSelect>
                                    </Grid>


                                    <Grid
                                        item
                                        xs={12}
                                        sx={{
                                            mt: 2,
                                            mb: 0.5,
                                        }}
                                    >
                                        <Typography>Tanggal Pembuatan Surat</Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Datepicker
                                            iconLeft={<Date width={20} height={20} fill={colors.teal_500} />}
                                            iconRight={<Arrow width={20} height={20} fill={colors.teal_500} direction='down' />}
                                            onChange={onDateChange}
                                            value={dateSelected}
                                        />
                                    </Grid>


                                </>
                        }
                    </Grid>
                    {/* End Form */}
                </Grid>
                <Grid item xs={12} sm={6} pl={{ xs: 0, sm: 2 }}>
                    <Typography variant="h5">Upload Surat</Typography>
                    <Divider />
                    {/* Form */}
                    <Grid item xs={12} container direction="row">
                        <FileDrop onChange={onFileDropChange} />

                        <>
                            {files.length > EMPTY_FILE && files.map((file: any, fileIndex: number) => (
                                <FileList
                                    key={fileIndex}
                                    file={file}
                                    onDelete={() => null}
                                    useProgress
                                    isLoading={false}
                                    isCompleted={true}
                                />
                            ))}
                        </>
                    </Grid>

                </Grid>
            </Grid>

            {/* Actions */}
            <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={2}
                sx={{
                    p: 0,
                    mt: 2,
                    mb: 1,
                    button: {
                        width: '200px',
                        maxWidth: '100%',
                        fontSize: '16px',
                    },
                }}
            >
                <Grid item xs={6} sm={'auto'}>
                    <Button variant="outlined" size="large" onClick={onCancel}>
                        Cancel
                    </Button>
                </Grid>

                <Grid item xs={6} sm={'auto'}>
                    <Button
                        variant="contained"
                        size="large"
                        onClick={onSubmit}
                    >
                        Send
                    </Button>
                </Grid>
            </Grid>
            {/* End Actions */}
        </>
    )
}

export default SendDoc
