import React, { useState, useContext } from 'react'
import { Link, useNavigate } from '@tanstack/react-location'
import { Box, Button, Grid, Typography } from '@mui/material'
import { Tab, TabPanel, TabsList } from '../../component/Tabs'
import { TabsUnstyled } from '@mui/base'
import ArrowBackIosNewRoundedIcon from '@mui/icons-material/ArrowBackIosNewRounded'
import { SendDoc } from './SendDoc'
import { colors } from '../../themes'
import { UserContext } from '../../App'
import { useMutation, useQuery } from 'react-query'
import { api } from '../../helper/ApiHelper'
import toast from 'react-hot-toast'
import { convertDate } from '../../utils/convertDate'
import { getSubbidang } from '../../query/getSubbidang'

const FIRST_INDEX: number = 0

export const UploadDoc = () => {
    const navigate = useNavigate()
    const userState = useContext(UserContext)
    const layanan = getSubbidang()
    const previousPage = -1
    const tabMenu1 = 'Kirim Surat'
    const tabMenu2 = 'Balas Surat'
    const [tabMenuActive, setTabMenuActive] = useState(tabMenu1)
    const isExternal = userState?.roleId === 7
    const isSuratMasuk = userState?.roleId === 7 || userState?.roleId === 6 || userState?.roleId === 5

    const [namaLayanan, setNamaLayanan] = useState() as any
    const [namaTujuan, setNamaTujuan] = useState() as any

    const [tanggal, setTanggal] = useState() as any

    const [perihal, setPerihal] = useState('')
    const [files, setFiles] = useState()


    const tabMenuClick = (e: any) => {
        setTabMenuActive(e.target.textContent)
    }

    //mutation for surat masuk
    const suratMasukMutation = useMutation(['suratmasuk'], async (data: any) => {
        const response: any = await api.post("/suratmasuk", data)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            onSuccess: (data: any) => {
                toast.success(data?.data?.message)
                setTimeout(() => {
                    navigate({ to: '/surat' })
                }, 800)
            }
        })

    // mutation for surat keluar
    const suratKeluarMutation = useMutation(['suratkeluar'], async (data: any) => {
        const response: any = await api.post("/suratkeluar", data)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            onSuccess: (data: any) => {
                toast.success(data?.data?.message)
                setTimeout(() => {
                    navigate({ to: '/surat' })
                }, 800)
            }
        })




    const handleLayananChange = (event: React.ChangeEvent<{ value: string }>) => {
        setNamaLayanan(event)
    }

    const handleTujuanChange = (event: React.ChangeEvent<{ value: string }>) => {
        setNamaTujuan(event)
    }

    const handlePerihalChange = (e: any) => {
        setPerihal(e.target.value)
    }

    const handleDateChange = (date: any) => {
        setTanggal(date)
    }

    const handleFileDropChange = (uploadedFiles: any[]) => {
        setFiles(uploadedFiles[FIRST_INDEX])
        // DEV NOTE: use bellow code when the api can accept many files than one
        // setFiles([...files, ...uploadedFiles])
    }

    const handleSubmitDoc = () => {
        const asalSurat = (isExternal ? 'External' : userState?.subbidang.namaBidang)

        const formData = new FormData()

        if (isSuratMasuk) {
            formData.append('perihal', perihal)
            formData.append('file', files ? files : '')
            formData.append('asalSurat', asalSurat)
            formData.append('layanan', namaLayanan)
            formData.append('userId', userState ? userState?.id : '1')

            return suratMasukMutation.mutate(formData)
        }

        if (!isSuratMasuk) {
            formData.append('perihal', perihal)
            formData.append('tujuan', namaTujuan)
            formData.append('tglPembuatan', convertDate(tanggal))
            formData.append('file', files ? files : '')
            formData.append('sifat', 'Biasa')
            formData.append('userId', userState ? userState?.id : '1')

            return suratKeluarMutation.mutate(formData)
        }
    }


    //end test
    return (
        <Box pl={{ xs: 0, sm: 3 }}>
            <Grid container direction="row" justifyContent="space-between" alignItems="center" sx={{ p: 0, m: 0 }}>
                <Grid item xs="auto">
                    <Grid
                        container
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                        sx={{ p: 0, m: 0 }}
                    >
                        <Grid item xs="auto">
                            <Link to="/surat">
                                <Box
                                    sx={{
                                        pt: 1,
                                        pb: 1,
                                        pl: 0,
                                        borderRight: `1px solid ${colors.grey_100} `,
                                        color: colors.grey_900,
                                    }}
                                >
                                    <Grid container direction="row" alignItems="center" sx={{ p: 0, m: 0 }}>
                                        <Grid item xs="auto" sx={{ display: 'flex', alignItems: 'center' }}>
                                            <ArrowBackIosNewRoundedIcon sx={{ fontSize: '18px' }} />
                                        </Grid>
                                        <Grid item xs="auto" sx={{ pl: 0.5, pr: 2 }}>
                                            Back
                                        </Grid>
                                    </Grid>
                                </Box>
                            </Link>
                        </Grid>
                        <Grid item xs="auto" sx={{ pl: 2 }}>
                            <Typography variant="h4" noWrap component="h1" sx={{ fontWeight: 'bold' }}>
                                {tabMenuActive}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>

            <Box
                sx={{
                    backgroundColor: 'white',
                    borderRadius: '10px',
                    minHeight: 'calc(100vh - 154px)',
                }}
                p={{ xs: 1.5, sm: 2 }}
                mt={2}
                mb={{ xs: 12, sm: 0 }}
            >
                <TabsUnstyled defaultValue={0}>
                    {/* Tab Menus */}
                    <Grid container direction="row" alignItems="center" sx={{ p: 0, m: 0 }} >
                        <Grid item xs={12} sm={'auto'}>
                            <Box display="flex" pl={{ xs: 0, sm: 2 }}>
                                <TabsList sx={{ m: 0 }}>
                                    <Tab onClick={tabMenuClick}>
                                        <Typography>{tabMenu1}</Typography>
                                    </Tab>
                                    {!isSuratMasuk ?
                                        <Tab onClick={tabMenuClick}>
                                            <Typography>{tabMenu2}</Typography>
                                        </Tab> :
                                        <Button disabled>
                                            <Typography color={colors.grey_300}>{tabMenu2}</Typography>
                                        </Button>
                                    }
                                </TabsList>
                            </Box>
                        </Grid>
                    </Grid>
                    {/* End Tab Menus */}
                    <TabPanel value={0} sx={{ p: 0 }}>
                        <SendDoc
                            layanan={layanan}
                            onLayananChange={handleLayananChange}
                            onPerihalChange={handlePerihalChange}
                            onSubmit={handleSubmitDoc}
                            onFileDropChange={handleFileDropChange}
                            files={files ? [files] : []}
                            isSuratMasuk={isSuratMasuk}
                            onCancel={() => navigate({ to: '/surat' })}
                            isBalas={false}
                            onTujuanChange={handleTujuanChange}
                            onDateChange={handleDateChange}
                            dateSelected={tanggal}
                        />
                    </TabPanel>
                    <TabPanel value={1} sx={{ p: 0 }}>
                        <SendDoc
                            layanan={layanan}
                            onLayananChange={handleLayananChange}
                            onPerihalChange={handlePerihalChange}
                            onSubmit={handleSubmitDoc}
                            onFileDropChange={handleFileDropChange}
                            files={files ? [files] : []}
                            isSuratMasuk={isSuratMasuk}
                            onCancel={() => navigate({ to: '/surat' })}
                            isBalas={true}
                            onTujuanChange={handleTujuanChange}
                            onDateChange={handleDateChange}
                            dateSelected={tanggal}
                        />
                    </TabPanel>
                </TabsUnstyled>
            </Box>
        </Box>
    )
}
