import { Box, Grid, Typography } from "@mui/material";
import { Link, MakeGenerics, useMatch } from "@tanstack/react-location";
import { colors } from "../../themes";
import ArrowBackIosNewRoundedIcon from '@mui/icons-material/ArrowBackIosNewRounded'
import { TableDisposisi } from "./TableDisposisi";
import { getDisposisi } from "../../query/getDisposisi";

type DisposisiGenerics = MakeGenerics<{
    Search: {
        typeSurat: string;
        id: string;
    }
}>;


export const Disposisi = () => {
    const search = useMatch<DisposisiGenerics>().search;

    const disposisi = getDisposisi(search.typeSurat!, search.id!)



    return (
        <Box pl={{ xs: 0, sm: 3 }}>
            <Grid container direction="row" justifyContent="space-between" alignItems="center" sx={{ p: 0, m: 0 }}>
                <Grid item xs="auto">
                    <Grid
                        container
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                        sx={{ p: 0, m: 0 }}
                    >
                        <Grid item xs="auto">
                            <Link to="/surat">
                                <Box
                                    sx={{
                                        pt: 1,
                                        pb: 1,
                                        pl: 0,
                                        borderRight: `1px solid ${colors.grey_100} `,
                                        color: colors.grey_900,
                                    }}
                                >
                                    <Grid container direction="row" alignItems="center" sx={{ p: 0, m: 0 }}>
                                        <Grid item xs="auto" sx={{ display: 'flex', alignItems: 'center' }}>
                                            <ArrowBackIosNewRoundedIcon sx={{ fontSize: '18px' }} />
                                        </Grid>
                                        <Grid item xs="auto" sx={{ pl: 0.5, pr: 2 }}>
                                            Back
                                        </Grid>
                                    </Grid>
                                </Box>
                            </Link>
                        </Grid>
                        <Grid item xs="auto" sx={{ pl: 2 }}>
                            <Typography variant="h4" noWrap component="h1" sx={{ fontWeight: 'bold' }}>
                                Disposisi
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>

            <Box
                sx={{
                    backgroundColor: 'white',
                    borderRadius: '10px',
                    minHeight: 'calc(100vh - 154px)',
                }}
                p={{ xs: 1.5, sm: 2 }}
                mt={2}
                mb={{ xs: 12, sm: 0 }}
            >
                <TableDisposisi dataSurat={disposisi} />
            </Box>
        </Box>

    )
}