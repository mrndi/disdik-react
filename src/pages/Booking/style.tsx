import { styled } from '@mui/system'
import { colors } from '../../themes'

export const CssTableInbox = styled('div')({
    '.booking-table-workingdesk': {
        table: {
            minWidth: 650,
            backgroundColor: colors.grey_200,
        },
        th: {
            color: colors.grey_900,
            borderBottom: 'unset',
        },
        td: {
            fontSize: '15px',
            color: colors.grey_900,
            height: '65px',
            borderBottom: 'unset',
        },
        '.chip-new': {
            marginTop: '-2px',
            marginLeft: '5px',
            lineHeight: 'normal',
            backgroundColor: colors.jaffa_100,
            border: `1px solid ${colors.jaffa_500}`,
            color: colors.grey_900,
            borderRadius: '4px',
            fontSize: '12px',
            height: 'auto',
            span: {
                padding: '2px',
            },
        },
        '.icon': {
            '&-arrow-down': {
                fontSize: '22px',
                color: colors.teal_700,
            },
            '&-check': {
                fontSize: '20px',
                color: colors.jungle_500,
            },
            '&-disturb': {
                fontSize: '20px',
                color: colors.jaffa_400,
            },
        },
        '.text': {
            '&-checkin': {
                fontSize: '15px',
                color: colors.grey_900,
                textTransform: 'capitalize',
                paddingLeft: '5px',
            },
        },
        '.action': {
            '&-edit': {
                marginLeft: '10px',
                cursor: 'pointer',
                a: {
                    height: '24px',
                    cursor: 'pointer',
                },
                img: {
                    width: '24px',
                },
            },
            '&-delete': {
                marginLeft: '10px',
                cursor: 'pointer',
                a: {
                    height: '24px',
                    cursor: 'pointer',
                },
                img: {
                    width: '24px',
                },
            },
            '&-qrcode': {
                backgroundColor: colors.teal_700,
                borderWidth: '2px',
                borderColor: colors.teal_700,
                boxShadow: 0,
                borderRadius: '4px',
                padding: '6px 12px',
                marginLeft: '16px',
                textTransform: 'capitalize',
                fontWeight: '400',
                '&:hover': {
                    boxShadow: 'unset',
                    backgroundColor: colors.teal_700,
                },
            },
            '&-cancel': {
                borderWidth: '2px',
                borderColor: colors.teal_700,
                boxShadow: 0,
                borderRadius: '4px',
                padding: '4px 12px',
                marginLeft: '16px',
                textTransform: 'capitalize',
                fontWeight: '400',
                '&:hover': {
                    borderWidth: '2px',
                    borderColor: colors.teal_700,
                    backgroundColor: 'unset',
                },
            },
        },
    },
})

export const CssTableInboxMobile = styled('div')({
    '.booking-accordion': {
        position: 'relative',
        borderRadius: '6px',
        padding: '10px 0',
        backgroundColor: colors.lilac_100,
        marginBottom: '8px',
        '&-item': {
            boxShadow: 'unset',
            backgroundColor: 'unset',
            '&-summary': {
                padding: '0',
            },
        },
        '.chip-new': {
            lineHeight: 'normal',
            backgroundColor: colors.jaffa_100,
            border: `1px solid ${colors.jaffa_500}`,
            color: colors.grey_900,
            borderRadius: '4px',
            fontSize: '12px',
            height: '20px',
            span: {
                padding: '2px',
            },
        },
        '.icon': {
            '&-book': {
                width: '16px',
                position: 'relative',
                top: '2px',
            },
            '&-calendar-outlined': {
                width: '14px',
                position: 'relative',
                top: '3px',
            },
            '&-repeat': {
                width: '16px',
                position: 'relative',
                top: '4px',
            },
            '&-notes': {
                width: '16px',
                position: 'relative',
                top: '2px',
            },
        },
        '.text': {
            '&-title': {
                color: colors.grey_900,
                fontSize: '15px',
            },
            '&-summary': {
                color: colors.grey_500,
                fontSize: '14px',
            },
        },
        '.box': {
            '&-title': {
                paddingLeft: '13px',
                '&.limit': {
                    width: 'calc(100% - 120px)',
                },
            },
            '&-action': {
                transition: 'all .3s ease',
                transform: 'translateY(6px)',
                '&.limit': {
                    width: '120px',
                },
                '.action-qrcode': {
                    width: '78px',
                    padding: '4px',
                    lineHeight: 'normal',
                    height: 'auto',
                    borderWidth: '2px',
                    boxShadow: 0,
                    borderRadius: '4px',
                    textTransform: 'capitalize',
                    fontWeight: '400',
                    fontSize: '13px',
                },
                '.action-dots': {
                    width: '40px',
                    height: '40px',
                    zIndex: '3',
                    textAlign: 'center',
                    svg: {
                        marginTop: '5px',
                    },
                },
            },
        },
        '.MuiPaper-root.Mui-expanded': {
            '.box': {
                '&-action': {
                    transform: 'translateY(0)',
                },
            },
        },
        '.MuiAccordionSummary': {
            '&-content': {
                margin: 0,
                '&.mui-expanded': {
                    margin: '0',
                    opacity: '1',
                    transition: 'all 0.2s ease-in-out',
                },
            },
            '&-root': {
                '&.Mui-expanded': {
                    minHeight: 'auto',
                },
            },
        },
        '.MuiAccordionDetails-root': {
            padding: '0 13px 0 13px',
        },
    },
})

export const CssTableMeetingRoom = styled('div')({
    '.booking-table-meetingroom': {
        table: {
            minWidth: 650,
            backgroundColor: colors.grey_200,
        },
        th: {
            color: colors.grey_900,
            borderBottom: 'unset',
        },
        td: {
            fontSize: '15px',
            color: colors.grey_900,
            height: '65px',
            borderBottom: 'unset',
        },
        '.chip-new': {
            marginTop: '-2px',
            marginLeft: '5px',
            lineHeight: 'normal',
            backgroundColor: colors.jaffa_100,
            border: `1px solid ${colors.jaffa_500}`,
            color: colors.grey_900,
            borderRadius: '4px',
            fontSize: '12px',
            height: 'auto',
            span: {
                padding: '2px',
            },
        },
        '.icon': {
            '&-arrow-down': {
                fontSize: '22px',
                color: colors.teal_700,
            },
            '&-check': {
                fontSize: '20px',
                color: colors.jungle_500,
            },
            '&-disturb': {
                fontSize: '20px',
                color: colors.jaffa_400,
            },
        },
        '.text': {
            '&-checkin': {
                fontSize: '15px',
                color: colors.grey_900,
                textTransform: 'capitalize',
                paddingLeft: '5px',
            },
        },
        '.action': {
            '&-edit': {
                marginLeft: '10px',
                cursor: 'pointer',
                a: {
                    height: '24px',
                    cursor: 'pointer',
                },
                img: {
                    width: '24px',
                },
            },
            '&-delete': {
                marginLeft: '10px',
                cursor: 'pointer',
                a: {
                    height: '24px',
                    cursor: 'pointer',
                },
                img: {
                    width: '24px',
                },
            },
            '&-qrcode': {
                backgroundColor: colors.teal_700,
                borderWidth: '2px',
                borderColor: colors.teal_700,
                boxShadow: 0,
                borderRadius: '4px',
                padding: '6px 12px',
                marginLeft: '16px',
                textTransform: 'capitalize',
                fontWeight: '400',
                '&:hover': {
                    boxShadow: 'unset',
                    backgroundColor: colors.teal_700,
                },
            },
            '&-cancel': {
                borderWidth: '2px',
                borderColor: colors.teal_700,
                boxShadow: 0,
                borderRadius: '4px',
                padding: '4px 12px',
                marginLeft: '16px',
                textTransform: 'capitalize',
                fontWeight: '400',
                '&:hover': {
                    borderWidth: '2px',
                    borderColor: colors.teal_700,
                    backgroundColor: 'unset',
                },
            },
            '&-view-guests': {
                borderWidth: '2px',
                borderColor: colors.teal_500,
                boxShadow: 0,
                borderRadius: '4px',
                padding: '2px 8px',
                textTransform: 'capitalize',
                fontWeight: '400',
                '&:hover': {
                    borderWidth: '2px',
                    borderColor: colors.teal_700,
                    backgroundColor: 'unset',
                },
            },
        },
    },
})

export const CssTableMeetingRoomMobile = styled('div')({
    '.booking-accordion': {
        position: 'relative',
        borderRadius: '6px',
        padding: '10px 0',
        backgroundColor: colors.lilac_100,
        marginBottom: '8px',
        '&-item': {
            boxShadow: 'unset',
            backgroundColor: 'unset',
            '&-summary': {
                padding: '0',
            },
        },
        '.chip-new': {
            lineHeight: 'normal',
            backgroundColor: colors.jaffa_100,
            border: `1px solid ${colors.jaffa_500}`,
            color: colors.grey_900,
            borderRadius: '4px',
            fontSize: '12px',
            height: '20px',
            span: {
                padding: '2px',
            },
        },
        '.icon': {
            '&-book': {
                width: '16px',
                position: 'relative',
                top: '2px',
            },
            '&-calendar-outlined': {
                width: '14px',
                position: 'relative',
                top: '3px',
            },
            '&-repeat': {
                width: '16px',
                position: 'relative',
                top: '4px',
            },
            '&-notes': {
                width: '16px',
                position: 'relative',
                top: '2px',
            },
            '&-guests': {
                width: '16px',
                position: 'relative',
                top: '1px',
            },
        },
        '.text': {
            '&-title': {
                color: colors.grey_900,
                fontSize: '15px',
            },
            '&-summary': {
                color: colors.grey_500,
                fontSize: '14px',
            },
            '&-guest-count': {
                color: colors.grey_500,
                fontSize: '14px',
                marginBottom: '5px',
            },
            '&-guest-name': {
                paddingLeft: '8px',
                paddingRight: '8px',
                color: colors.grey_500,
                fontSize: '14px',
            },
        },
        '.box': {
            '&-title': {
                paddingLeft: '13px',
                '&.limit': {
                    width: 'calc(100% - 120px)',
                },
            },
            '&-action': {
                transition: 'all .3s ease',
                transform: 'translateY(6px)',
                '&.limit': {
                    width: '120px',
                },
                '.action-qrcode': {
                    width: '78px',
                    padding: '4px',
                    lineHeight: 'normal',
                    height: 'auto',
                    borderWidth: '2px',
                    boxShadow: 0,
                    borderRadius: '4px',
                    textTransform: 'capitalize',
                    fontWeight: '400',
                    fontSize: '13px',
                },
                '.action-dots': {
                    width: '40px',
                    height: '40px',
                    zIndex: '3',
                    textAlign: 'center',
                    svg: {
                        marginTop: '5px',
                    },
                },
            },
        },
        '.MuiPaper-root': {
            '.summary-singleline': {
                flexDirection: 'row',
            },
            '&.Mui-expanded': {
                '.box': {
                    '&-action': {
                        transform: 'translateY(0)',
                    },
                },
                '.summary-singleline': {
                    flexDirection: 'column',
                    '.box-item': {
                        mb: 0.7,
                        '.MuiTypography-root': {
                            pl: 1,
                        },
                    },
                },
            },
        },
        '.MuiAccordionSummary': {
            '&-content': {
                margin: 0,
                '&.mui-expanded': {
                    margin: '0',
                    opacity: '1',
                    transition: 'all 0.2s ease-in-out',
                },
            },
            '&-root': {
                '&.Mui-expanded': {
                    minHeight: 'auto',
                },
            },
        },
        '.MuiAccordionDetails-root': {
            padding: '0 13px 0 13px',
        },
    },
})

export const CssPopoverMeetingRoom = styled('div')({
    '.popover': {
        '&-wrapper': {
            width: '480px',
            maxWidth: '100%',
            padding: '15px',
        },
        '&-item': {
            marginBottom: '10px',
            position: 'relative',
            '&:last-child': {
                marginBottom: 0,
            },
            '&-guestname': {
                paddingLeft: '15px',
                paddingRight: '15px',
                color: colors.grey_900,
                lineHeight: 1.5,
            },
            '&-guestavatar': {
                width: '30px',
                height: '30px',
            },
            '&-icon-check': {
                fontSize: '22px',
                color: colors.jungle_500,
                position: 'absolute',
                right: 0,
            },
            '&-icon-disturb': {
                fontSize: '22px',
                color: colors.jaffa_400,
                position: 'absolute',
                right: 0,
            },
        },
    },
})

export const CssTableCheckin = styled('div')({
    '.booking-table-checkin': {
        table: {
            minWidth: 650,
            backgroundColor: colors.grey_200,
        },
        th: {
            color: colors.grey_900,
            borderBottom: 'unset',
        },
        td: {
            fontSize: '15px',
            color: colors.grey_900,
            height: '65px',
            borderBottom: 'unset',
        },
        '.chip-new': {
            marginTop: '-2px',
            marginLeft: '5px',
            lineHeight: 'normal',
            backgroundColor: colors.jaffa_100,
            border: `1px solid ${colors.jaffa_500}`,
            color: colors.grey_900,
            borderRadius: '4px',
            fontSize: '12px',
            height: 'auto',
            span: {
                padding: '2px',
            },
        },
        '.icon': {
            '&-arrow-down': {
                fontSize: '22px',
                color: colors.teal_700,
            },
            '&-check': {
                fontSize: '20px',
                color: colors.jungle_500,
            },
            '&-disturb': {
                fontSize: '20px',
                color: colors.jaffa_400,
            },
        },
        '.text': {
            '&-checkin': {
                fontSize: '15px',
                color: colors.grey_900,
                textTransform: 'capitalize',
                paddingLeft: '5px',
            },
        },
        '.action': {
            '&-edit': {
                marginLeft: '10px',
                cursor: 'pointer',
                a: {
                    height: '24px',
                    cursor: 'pointer',
                },
                img: {
                    width: '24px',
                },
            },
            '&-delete': {
                marginLeft: '10px',
                cursor: 'pointer',
                a: {
                    height: '24px',
                    cursor: 'pointer',
                },
                img: {
                    width: '24px',
                },
            },
            '&-qrcode': {
                backgroundColor: colors.teal_700,
                borderWidth: '2px',
                borderColor: colors.teal_700,
                boxShadow: 0,
                borderRadius: '4px',
                padding: '6px 12px',
                marginLeft: '16px',
                textTransform: 'capitalize',
                fontWeight: '400',
                '&:hover': {
                    boxShadow: 'unset',
                    backgroundColor: colors.teal_700,
                },
            },
            '&-cancel': {
                borderWidth: '2px',
                borderColor: colors.teal_700,
                boxShadow: 0,
                borderRadius: '4px',
                padding: '4px 12px',
                marginLeft: '16px',
                textTransform: 'capitalize',
                fontWeight: '400',
                '&:hover': {
                    borderWidth: '2px',
                    borderColor: colors.teal_700,
                    backgroundColor: 'unset',
                },
            },
        },
    },
})

export const CssTableCheckinMobile = styled('div')({
    '.booking-checkin': {
        position: 'relative',
        borderRadius: '6px',
        padding: '10px 0',
        backgroundColor: colors.lilac_100,
        marginBottom: '8px',
        '&-item': {
            boxShadow: 'unset',
            backgroundColor: 'unset',
            padding: '0 14px',
        },
        '.chip-new': {
            lineHeight: 'normal',
            backgroundColor: colors.jaffa_100,
            border: `1px solid ${colors.jaffa_500}`,
            color: colors.grey_900,
            borderRadius: '4px',
            fontSize: '12px',
            height: '20px',
            span: {
                padding: '2px',
            },
        },
        '.icon': {
            '&-dots': {
                width: '40px',
                height: '40px',
                position: 'absolute',
                top: '10px',
                right: '0',
                zIndex: '3',
                textAlign: 'center',
                svg: {
                    marginTop: '5px',
                },
            },
            '&-user-checked': {
                width: '16px',
                position: 'relative',
                top: '1px',
            },
            '&-calendar-outlined': {
                width: '14px',
                position: 'relative',
                top: '3px',
            },
            '&-notes': {
                width: '16px',
                position: 'relative',
                top: '2px',
            },
            '&-check': {
                fontSize: '20px',
                color: colors.jungle_500,
            },
            '&-disturb': {
                fontSize: '20px',
                color: colors.jaffa_400,
            },
        },
        '.text': {
            '&-title': {
                color: colors.grey_900,
                fontSize: '15px',
            },
            '&-summary': {
                color: colors.grey_500,
                fontSize: '14px',
            },
        },
    },
})
