export const areaSetting = {
    width: 530,
    height: 538,
}

const defaultDeskSetting = {
    width: 32,
    height: 32,
    fontSize: 13,
    available: true,
}
export const deskSettings = [
    {
        ...defaultDeskSetting,
        id: 1,
        x: 476,
        y: 466,
        seatPosition: 'bottom',
        available: false,
    },
    {
        ...defaultDeskSetting,
        id: 2,
        x: 476,
        y: 431,
        seatPosition: 'top',
        available: false,
    },
    {
        ...defaultDeskSetting,
        id: 3,
        x: 441,
        y: 466,
        seatPosition: 'bottom',
    },
    {
        ...defaultDeskSetting,
        id: 4,
        x: 441,
        y: 431,
        seatPosition: 'top',
    },
    {
        ...defaultDeskSetting,
        id: 75,
        x: 290,
        y: 78,
        seatPosition: 'top',
    },
    {
        ...defaultDeskSetting,
        id: 10,
        x: 110,
        y: 30,
        seatPosition: 'right',
    },
    {
        ...defaultDeskSetting,
        id: 100,
        x: 210,
        y: 30,
        seatPosition: 'left',
    },
    {
        ...defaultDeskSetting,
        id: 150,
        x: 310,
        y: 30,
        seatPosition: 'bottom',
    },
]

const defaultMeetingRoomSeting = {
    fontSize: 18,
    available: true,
}
export const meetingRoomSettings = [
    {
        ...defaultMeetingRoomSeting,
        id: 1,
        name: 'Meeting Room',
        x: 10,
        y: 130,
        width: 200,
        height: 120,
    },
]

const defaultMiscRoomSeting = {
    name: '',
    fontSize: 13,
}
export const miscRoomSettings = [
    {
        ...defaultMiscRoomSeting,
        id: 1,
        x: 260,
        y: 130,
        width: 8,
        height: 120,
    },
    {
        ...defaultMiscRoomSeting,
        id: 2,
        name: 'Server',
        x: 300,
        y: 130,
        width: 100,
        height: 120,
    },
]
