import React from 'react'
import { Stage, Layer, Rect } from 'react-konva'
import Desk from './Desk'
import MeetingRoom from './MeetingRoom'
import MiscRoom from './MiscRoom'
import { colors } from 'themes'
import { areaSetting, deskSettings, meetingRoomSettings, miscRoomSettings } from './settings'

export const FloorMap = () => {
    return (
        <Stage width={areaSetting.width} height={areaSetting.height}>
            <Layer>
                <Rect
                    x={0}
                    y={0}
                    width={areaSetting.width}
                    height={areaSetting.height}
                    stroke={colors.grey_200}
                    strokeWidth={2}
                    cornerRadius={10}
                />
            </Layer>
            <Layer>
                {deskSettings.map((desk) => (
                    <Desk key={desk.id} setting={desk} />
                ))}
                {meetingRoomSettings.map((meet) => (
                    <MeetingRoom key={meet.id} setting={meet} />
                ))}
                {miscRoomSettings.map((misc) => (
                    <MiscRoom key={misc.id} setting={misc} />
                ))}
            </Layer>
        </Stage>
    )
}

export default FloorMap
