import React from 'react'
import { Rect, Text } from 'react-konva'
import { colors } from 'themes'

export const MeetingRoom = ({ setting }) => {
    const { name, x, y, width, height, fontSize } = setting
    const divider = 2
    const halfDeskHeight = height / divider
    const halfTextHeight = fontSize / divider
    return (
        <>
            {/* Desk */}
            <Rect x={x} y={y} width={width} height={height} fill={colors.grey_300} cornerRadius={4} />
            <Text
                x={x}
                y={y + halfDeskHeight - halfTextHeight}
                text={name}
                width={width}
                height={fontSize}
                fontSize={fontSize}
                fontStyle="bold"
                align="center"
            />
        </>
    )
}

export default MeetingRoom
