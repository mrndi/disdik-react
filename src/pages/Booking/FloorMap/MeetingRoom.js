import React, { useState } from 'react'
import { Rect, Text } from 'react-konva'
import { colors } from 'themes'

export const MeetingRoom = ({ setting }) => {
    const { name, x, y, width, height, fontSize, available } = setting
    const [isSelected, setSelected] = useState(false)
    const divider = 2
    const halfDeskHeight = height / divider
    const halfTextHeight = fontSize / divider
    const toggleSelected = () => {
        setSelected(!isSelected)
    }
    const getFill = () => {
        if (available) {
            if (isSelected) {
                return colors.purple_100
            }
            return colors.green_100
        }
        return colors.red_100
    }
    const getStroke = () => {
        if (available) {
            if (isSelected) {
                return colors.purple_200
            }
            return colors.green_500
        }
        return colors.red_500
    }
    const fillColor = getFill()
    const strokeColor = getStroke()
    return (
        <>
            {/* Desk */}
            <Rect
                x={x}
                y={y}
                width={width}
                height={height}
                fill={fillColor}
                stroke={strokeColor}
                strokeWidth={2}
                cornerRadius={4}
            />
            <Text
                x={x}
                y={y + halfDeskHeight - halfTextHeight}
                text={name}
                width={width}
                height={fontSize}
                fontSize={fontSize}
                fontStyle="bold"
                align="center"
            />
            {/* Clickable Area */}
            <Rect x={x} y={y} width={width} height={height} onClick={toggleSelected} onTouchEnd={toggleSelected} />
        </>
    )
}

export default MeetingRoom
