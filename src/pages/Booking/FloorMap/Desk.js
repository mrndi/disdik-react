import React, { useState } from 'react'
import { Rect, Circle, Text } from 'react-konva'
import { colors } from 'themes'

export const Desk = ({ setting }) => {
    const { id, x, y, width, height, fontSize, seatPosition, available } = setting
    const [isSelected, setSelected] = useState(false)
    const divider = 2
    const halfDeskHeight = height / divider
    const halfTextHeight = fontSize / divider
    const seatOffsetX = width / divider
    const seatOffsetY = 2
    const seatScale = 0.6
    const seatRadius = (width * seatScale) / divider
    const toggleSelected = () => {
        setSelected(!isSelected)
    }
    const getFill = () => {
        if (available) {
            if (isSelected) {
                return colors.purple_100
            }
            return colors.green_100
        }
        return colors.red_100
    }
    const getStroke = () => {
        if (available) {
            if (isSelected) {
                return colors.purple_200
            }
            return colors.green_500
        }
        return colors.red_500
    }
    const getSeatPosX = () => {
        if (seatPosition === 'left') {
            return x + seatOffsetY
        }
        if (seatPosition === 'right') {
            return x + width - seatOffsetY
        }
        return x + seatOffsetX
    }
    const getSeatPosY = () => {
        if (seatPosition === 'bottom') {
            return y + height - seatOffsetY
        }
        if (seatPosition === 'left' || seatPosition === 'right') {
            return y + seatOffsetX
        }
        return y + seatOffsetY
    }
    const seatPosX = getSeatPosX(seatPosition)
    const seatPosY = getSeatPosY(seatPosition)
    const fillColor = getFill()
    const strokeColor = getStroke()
    return (
        <>
            {/* Seat */}
            <Circle
                x={seatPosX}
                y={seatPosY}
                radius={seatRadius}
                fill={fillColor}
                stroke={strokeColor}
                strokeWidth={2}
            />
            {/* Desk */}
            <Rect
                x={x}
                y={y}
                width={width}
                height={height}
                fill={fillColor}
                stroke={strokeColor}
                strokeWidth={2}
                cornerRadius={4}
            />
            <Text
                x={x}
                y={y + halfDeskHeight - halfTextHeight}
                text={id}
                width={width}
                height={fontSize}
                fontSize={fontSize}
                fontStyle="bold"
                align="center"
            />
            {/* Clickable Area */}
            <Rect x={x} y={y} width={width} height={height} onClick={toggleSelected} onTouchEnd={toggleSelected} />
        </>
    )
}

export default Desk
