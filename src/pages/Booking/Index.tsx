import React, { useContext, useCallback, useState } from 'react'
import { useNavigate } from '@tanstack/react-location'
import { Button, Box, Collapse, Grid, Stack, Typography, Modal } from '@mui/material'
import { TableInbox, TableInboxMobile } from './TableInbox'
import { TableMeetingRoom, TableMeetingRoomMobile } from './TableMeetingRoom'
// import { TableCheckin, TableCheckinMobile } from './TableCheckin'
import { TextInput, FileDrop } from '../../component/Form'
import { Tab, TabPanel, TabsList } from '../../component/Tabs'
import { TabsUnstyled } from '@mui/base'
import { DateRangePicker } from '../../component/DateRangePicker'
import { Arrow, Date, Desk, Search, Sort, Scan } from '../../assets/icons'
import { colors } from '../../themes'
import { UserContext } from '../../App'
import { getSuratmasukByHolderId } from '../../query/getSuratmasuk'
import { getSuratkeluarByHolderId } from '../../query/getSuratkeluar'
import { getInbox } from '../../query/getInbox'
import { getOutbox } from '../../query/getOutbox'

const SURATDITERIMA_TAB = 0
const SURATDIKIRIM_TAB = 1
const SURATMASUK_TAB = 2
const SURATKELUAR_TAB = 3

export default function BookingList() {
    const userState = useContext(UserContext)
    const isExternal = userState?.roleId === 7 || userState?.roleId === 6 || userState?.roleId === 5

    const isAdmin = userState?.roleId === 2
    const pageTitle = isAdmin ? 'Daftar Surat' : 'Surat Saya'
    const navigate = useNavigate()
    const [isQRCode, setIsQRCode] = useState(false)
    const [isDateRange, setIsDateRange] = useState(false)
    const [isSearchMobile, setIsSearchMobile] = useState(false)
    const [isDateMobile, setIsDateMobile] = useState(false)
    const [activeTab, setActiveTab] = useState(SURATDITERIMA_TAB)
    const inbox = getInbox(userState?.id!)
    const outbox = getOutbox(userState?.id!)



    const onChangeTab = (val: any) => {
        setActiveTab(val)
    }


    return (
        <Box>
            <Stack direction="row" justifyContent="space-between" alignItems="center">
                <Typography variant="h4" component="h1" style={{ fontWeight: 'bold' }}>
                    {pageTitle}
                </Typography>
                <Box display={{ xs: 'none', sm: 'block' }}>
                    <Stack
                        direction="row"
                        spacing={2}
                        sx={{
                            button: {
                                width: '200px',
                                fontSize: '16px',
                            },
                        }}
                    >
                        <Button
                            variant="contained"
                            color="primary"
                            startIcon={<Desk width={24} height={24} outlined={true} />}
                            onClick={() => navigate({ to: '/surat/upload', replace: true })}
                            size="large"
                            disabled={false}
                        >
                            Kirim Surat
                        </Button>
                        <Button
                            variant="outlined"
                            color="primary"
                            size="large"
                            startIcon={<Scan width={24} height={24} fill={colors.teal_500} />}
                            onClick={() => setIsQRCode(!isQRCode)}
                            disabled
                        >
                            Scan QR Code
                        </Button>
                    </Stack>
                </Box>
            </Stack>

            <Box
                sx={{
                    backgroundColor: 'white',
                    borderRadius: '10px',
                    minHeight: 'calc(100vh - 154px)',
                }}
                p={{ xs: 1, sm: 2 }}
                pb={{ xs: 2 }}
                mt={2}
                mb={{ xs: 12, sm: 0 }}
            >
                {/* Tab */}
                <TabsUnstyled defaultValue={0} onChange={onChangeTab}>
                    <Grid
                        container
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                        mb={{ xs: 0, sm: 2 }}
                        sx={{ p: 0 }}
                    >
                        <Grid item xs={12} sm={'auto'}>
                            <TabsList className={isAdmin ? 'tab-3-items' : 'tab-2-items'} sx={{ alignItems: 'center' }}>
                                <Tab >
                                    <Typography>Surat Diterima</Typography>
                                </Tab>
                                <Tab>
                                    <Typography>Surat Dikirim</Typography>
                                </Tab>
                                {isAdmin ? (
                                    <Tab>
                                        <Typography>Surat Masuk</Typography>
                                    </Tab>
                                ) : (
                                    ''
                                )}
                                {isAdmin ? (
                                    <Tab>
                                        <Typography>Surat Keluar</Typography>
                                    </Tab>
                                ) : (
                                    ''
                                )}
                            </TabsList>
                        </Grid>

                        <Grid item xs={12} sm={5}>
                            <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
                                <Grid
                                    container
                                    direction="row"
                                    justifyContent="space-between"
                                    alignItems="center"
                                    sx={{ p: 0, m: 0 }}
                                >
                                    <Grid item xs={6}>
                                        <TextInput
                                            name='search'
                                            placeholder="Search"
                                            iconLeft={<Search width={27} height={24} stroke={colors.teal_500} />}
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Box
                                            sx={{
                                                pl: 2,
                                                position: 'relative',
                                            }}
                                        >
                                            <DateRangePicker
                                                isShow={isDateRange}
                                                placeholder="Today"
                                                onClose={() => setIsDateRange(!isDateRange)}
                                            />
                                        </Box>
                                    </Grid>
                                </Grid>
                            </Box>
                        </Grid>

                        {/* Mobile */}
                        {activeTab !== SURATKELUAR_TAB ? (
                            <Grid item xs={12} display={{ xs: 'block', sm: 'block', md: 'none' }}>
                                <Box mt={3}>
                                    <Stack direction="row" justifyContent="space-between" alignItems="center" mb={2}>
                                        <Typography>Show 1 - 10 of 100 data</Typography>
                                        <Stack direction="row" spacing={3} alignItems="center">
                                            <Box onClick={() => setIsSearchMobile(!isSearchMobile)}>
                                                <Search width={24} height={24} stroke={colors.teal_500} />
                                            </Box>
                                            <Box onClick={() => setIsDateMobile(!isDateMobile)}>
                                                <Sort width={24} height={24} fill={colors.teal_500} />
                                            </Box>
                                        </Stack>
                                    </Stack>
                                    <Collapse
                                        in={isSearchMobile}
                                        sx={{
                                            '.MuiCollapse-wrapper': {
                                                mb: 1,
                                            },
                                        }}
                                    >
                                        <TextInput
                                            iconLeft={<Search width={24} height={24} stroke={colors.teal_500} />}
                                            placeholder="Search"
                                            label="search"
                                            name="search"
                                        />
                                    </Collapse>
                                    <Collapse
                                        in={isDateMobile}
                                        sx={{
                                            '.MuiCollapse-wrapper': {
                                                mb: 1,
                                            },
                                        }}
                                    >
                                        <TextInput
                                            iconLeft={<Date width={24} height={24} fill={colors.teal_500} />}
                                            iconRight={<Arrow width={20} height={20} fill={colors.teal_500} direction='right' />}
                                            placeholder="Today"
                                            label="date"
                                            name="date"
                                        />
                                    </Collapse>
                                </Box>
                            </Grid>
                        ) : (
                            <Box mt={3} />
                        )}
                        {/* End Mobile */}
                    </Grid>
                    <TabPanel value={SURATDITERIMA_TAB}>
                        <TableInbox dataSurat={inbox} />
                        <TableInboxMobile />
                    </TabPanel>
                    <TabPanel value={SURATDIKIRIM_TAB}>
                        <TableInbox dataSurat={outbox} isOutbox />
                        <TableInboxMobile />
                    </TabPanel>
                    {isAdmin ? (
                        <TabPanel value={SURATMASUK_TAB}>
                            {/* <TableCheckin /> */}
                            {/* <TableCheckinMobile /> */}
                        </TabPanel>
                    ) : (
                        ''
                    )}
                    {isAdmin ? (
                        <TabPanel value={SURATKELUAR_TAB}>
                            {/* <QRCode
                                title={'QR Generator'}
                                name={'Software Seni Office'}
                                isShowId={true}
                                isShowButtons={true}
                            /> */}
                        </TabPanel>
                    ) : (
                        ''
                    )}
                </TabsUnstyled>
            </Box>

            {/* <ModalScanQR isQRCode={isQRCode} onClose={() => setIsQRCode(!isQRCode)} code="Hello World!" /> */}


        </Box>
    )
}


