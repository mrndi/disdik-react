import React, { useState, useContext } from 'react'
import { Link } from '@tanstack/react-location'
import {
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Avatar,
    Button,
    Box,
    Chip,
    Paper,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Typography,
} from '@mui/material'
import { Pagination } from '../../component/Pagination'
import { Popover } from '../../component/Form/Popover'
import { CircularProgress } from '../../component/Progress'
// import { ModalScanQR } from 'containers/ScanQR'
import { ActionPreviewDoc } from './ActionPreviewDoc'
import { CssTableMeetingRoom, CssTableMeetingRoomMobile, CssPopoverMeetingRoom } from './style'
import DoDisturbOnRoundedIcon from '@mui/icons-material/DoDisturbOnRounded'
import CheckCircleRoundedIcon from '@mui/icons-material/CheckCircleRounded'
import KeyboardArrowDownRoundedIcon from '@mui/icons-material/KeyboardArrowDownRounded'
import { Available, Date, Duration, Delete, Desk, Edit, Minus, Meeting, Note, Repeat, VerticalMenu } from '../../assets/icons'
import { colors } from '../../themes'
import { dataMeetingRoom } from '../../fake-db/meetingroom'
import { dataStaff } from '../../fake-db/staff'
import { TableHeader } from '../../containers/TableHeader'
import { UserContext } from '../../App'

const startString = 0
const MAX_NOTE = 25
const startSlice = 0
const endSlice = 3

export const TableMeetingRoom = () => {
    const userState = useContext(UserContext)
    const isAdmin = true

    //Modal
    const [isQRCode, setIsQRCode] = useState(false)
    const [isCancelBooking, setCancelBooking] = useState(false)
    const [isDeleteBooking, setDeleteBooking] = useState(false)

    //Popover: Guests
    const [anchorEl, setAnchorEl] = useState() as any
    const [isPopoverGuests, setOpenPopoverGuests] = useState(false)
    const onClickPopoverGuests = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget)
        setOpenPopoverGuests((prevState) => !prevState)
    }

    //Popover: Repeat
    const [isPopoverRepeat, setOpenPopoverRepeat] = useState(false)
    const [PopoverContentRepeat, setPopoverContentRepeat] = useState('')
    const onClickPopoverRepeat = (event: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(event.currentTarget)
        setOpenPopoverRepeat((prevState) => !prevState)
        setPopoverContentRepeat(event.currentTarget.title)
    }

    //Popover: Notes
    const [isPopoverNotes, setOpenPopoverNotes] = useState(false)
    const [PopoverNotes, setPopoverNotes] = useState('')
    const onClickPopoverNotes = (event: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(event.currentTarget)
        setOpenPopoverNotes((prevState) => !prevState)
        setPopoverNotes(event.currentTarget.title)
    }

    const titles = [
        ...(isAdmin ? [{ name: 'Staff Name', alias: 'name' }] : []),
        { name: 'Room', alias: 'room' },
        { name: 'Guests', alias: 'guests' },
        { name: 'Date', alias: 'date' },
        { name: 'Time', alias: 'time' },
        { name: 'Repeat', alias: 'repeat' },
        { name: 'Check In', alias: 'checkin' },
        { name: 'Note', alias: 'note' },
        { name: '', alias: '' },
    ]
    const onSortHandler = (alias: string, direction: string) => {
        return console.log(alias, direction)
    }

    return (
        <Box display={{ xs: 'none', sm: 'block' }}>
            <CssTableMeetingRoom>
                <TableContainer component={Paper} elevation={0} className="booking-table-meetingroom">
                    <Table>
                        <TableHeader titles={titles} onSortChange={onSortHandler} />
                        <TableBody>
                            {dataMeetingRoom.map((data, idx) => (
                                <TableRow key={idx} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                    {isAdmin ? (
                                        <TableCell>
                                            {data.staff_name}
                                            <Chip label="New" size="small" className="chip-new" />
                                        </TableCell>
                                    ) : null}
                                    <TableCell>{data.room}</TableCell>
                                    <TableCell size='medium' sx={{ width: '115px', padding: '0px' }}>
                                        <Button
                                            onClick={onClickPopoverGuests}
                                            variant="outlined"
                                            color="primary"
                                            size="medium"
                                            className="action-view-guests"
                                        >
                                            View Guest
                                        </Button>
                                    </TableCell>
                                    <TableCell>{data.date}</TableCell>
                                    <TableCell>{data.time}</TableCell>
                                    <TableCell>
                                        {data.repeat_notes !== '' ? (
                                            <Stack
                                                direction="row"
                                                alignItems="center"
                                                onClick={onClickPopoverRepeat}
                                                title={'On ' + data.repeat_type + ', ' + data.repeat_notes}
                                                sx={{ '&:hover': { cursor: 'pointer' } }}
                                            >
                                                {data.repeat_type}
                                                <KeyboardArrowDownRoundedIcon className="icon-arrow-down" />
                                            </Stack>
                                        ) : (
                                            'None'
                                        )}
                                    </TableCell>
                                    <TableCell>
                                        <Stack direction="row" alignItems="center">
                                            {data.checkin === 'yes' ? (
                                                <CheckCircleRoundedIcon className="icon-check" />
                                            ) : (
                                                <DoDisturbOnRoundedIcon className="icon-disturb" />
                                            )}
                                            <Typography className="text-checkin">{data.checkin}</Typography>
                                        </Stack>
                                    </TableCell>
                                    <TableCell>
                                        {data.notes.length > MAX_NOTE ? (
                                            <Box
                                                onClick={onClickPopoverNotes}
                                                title={data.notes}
                                                sx={{ cursor: 'pointer' }}
                                            >
                                                {`${data.notes.substring(startString, MAX_NOTE)}...`}
                                            </Box>
                                        ) : (
                                            data.notes
                                        )}
                                    </TableCell>
                                    <TableCell sx={{ textAlign: 'right' }}>
                                        <Stack direction="row" alignItems="center" justifyContent="flex-end">
                                            {data.status === 'active' ? (
                                                <Box display="flex" alignItems="center" className="action-edit">
                                                    <Link to="/book">
                                                        <Edit width={24} height={24} fill={colors.teal_500} />
                                                    </Link>
                                                </Box>
                                            ) : (
                                                ''
                                            )}

                                            {isAdmin && data.status === 'active' ? (
                                                <Box
                                                    display="flex"
                                                    alignItems="center"
                                                    className="action-delete"
                                                    onClick={() => setDeleteBooking(!isDeleteBooking)}
                                                >
                                                    <Delete width={24} height={24} fill={colors.teal_500} />
                                                </Box>
                                            ) : (
                                                ''
                                            )}

                                            {!isAdmin && data.status === 'active' ? (
                                                <Stack direction="row" alignItems="center">
                                                    <Button
                                                        variant="contained"
                                                        color="secondary"
                                                        onClick={() => setIsQRCode(!isQRCode)}
                                                        className="action-qrcode"
                                                        sx={{ fontSize: '0.75rem' }}
                                                    >
                                                        Scan QR
                                                    </Button>
                                                    <Button
                                                        variant="outlined"
                                                        color="secondary"
                                                        className="action-cancel"
                                                        onClick={() => setCancelBooking(!isCancelBooking)}
                                                    >
                                                        Cancel
                                                    </Button>
                                                </Stack>
                                            ) : (
                                                ''
                                            )}
                                        </Stack>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>

                <Popover
                    isShow={isPopoverGuests}
                    onClose={() => setOpenPopoverGuests(!isPopoverGuests)}
                    anchorEl={anchorEl}
                    content={
                        <CssPopoverMeetingRoom>
                            <Box className="popover-wrapper">
                                {/* TODO: Remove limit dynamic API */}
                                {dataStaff.slice(startSlice, endSlice).map((data, idx) => {
                                    return (
                                        <Stack
                                            direction="row"
                                            alignItems="center"
                                            justifyContent="flex-start"
                                            className="popover-item"
                                            key={idx}
                                        >
                                            <Avatar
                                                src={data.image}
                                                className="popover-item-guestavatar"
                                                alt={data.label}
                                            />
                                            <Typography variant="body2" className="popover-item-guestname">
                                                {data.nickname} ({data.email})
                                            </Typography>
                                            {/* TODO: IF Allow or Disallow */}
                                            <CheckCircleRoundedIcon className="popover-item-icon-check" />
                                            {/* <DoDisturbOnRoundedIcon className="popover-item-icon-disturb" /> */}
                                        </Stack>
                                    )
                                })}
                            </Box>
                        </CssPopoverMeetingRoom>
                    }
                />

                <Pagination />

                <Popover
                    isShow={isPopoverRepeat}
                    onClose={() => setOpenPopoverRepeat(!isPopoverRepeat)}
                    anchorEl={anchorEl}
                    content={
                        <Box sx={{ maxWidth: '350px' }} p={2}>
                            <Typography variant="body2">{PopoverContentRepeat}</Typography>
                        </Box>
                    }
                />

                <Popover
                    isShow={isPopoverNotes}
                    onClose={() => setOpenPopoverNotes(!isPopoverNotes)}
                    anchorEl={anchorEl}
                    content={
                        <Box sx={{ maxWidth: '350px' }} p={2}>
                            <Typography variant="body2">{PopoverNotes}</Typography>
                        </Box>
                    }
                />

                {/* <ModalScanQR isQRCode={isQRCode} onClose={() => setIsQRCode(!isQRCode)} code="Hello World!" /> */}

                <ActionPreviewDoc
                    isShow={isCancelBooking}
                    onClose={() => setCancelBooking(!isCancelBooking)}
                    data={'Are you sure want to cancel this?'}
                    btnYes={() => console.log('Yes button clicked')}
                    btnYesText={'Yes'}
                    btnNoText={'No'}
                />

                <ActionPreviewDoc
                    isShow={isDeleteBooking}
                    onClose={() => setDeleteBooking(!isDeleteBooking)}
                    data={'Are you sure want to delete this?'}
                    btnYes={() => console.log('Yes button clicked')}
                    btnYesText={'Yes'}
                    btnNoText={'No'}
                />
            </CssTableMeetingRoom>
        </Box >
    )
}

export const TableMeetingRoomMobile = () => {
    const userState = useContext(UserContext)
    const isAdmin = true

    //Popover
    const [anchorEl, setAnchorEl] = useState() as any
    const [isPopoverActions, setOpenPopoverActions] = useState(false)
    const onClickPopoverActions = (e: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(e.currentTarget)
        setOpenPopoverActions((prevState) => !prevState)
    }

    //Modal
    const [isQRCode, setIsQRCode] = useState(false)
    const [isDeleteBooking, setDeleteBooking] = useState(false)

    return (
        <Box display={{ xs: 'block', sm: 'none' }}>
            <CssTableMeetingRoomMobile>
                {dataMeetingRoom.map((data, idx) => (
                    <Box className="booking-accordion" key={idx}>
                        <Accordion className="booking-accordion-item">
                            <AccordionSummary
                                expandIcon={false}
                                aria-controls="panel1a-content"
                                id={`accordion-${idx}`}
                                className="booking-accordion-item-summary"
                            >
                                <Stack direction="row" justifyContent="space-between" sx={{ width: '100%' }}>
                                    <Box className="box-title">
                                        <Stack direction="row" alignItems="center" spacing={0.7}>
                                            <Box>
                                                {isAdmin ? (
                                                    <Typography className="text-title">{data.staff_name}</Typography>
                                                ) : (
                                                    <Typography className="text-title">{data.room}</Typography>
                                                )}
                                            </Box>
                                            <Box>
                                                <Stack direction="row" alignItems="center" spacing={0.7}>
                                                    {isAdmin ? (
                                                        <Chip label="New" size="small" className="chip-new" />
                                                    ) : (
                                                        ''
                                                    )}
                                                    {data.status === 'inactive' && data.checkin === 'yes' ? (
                                                        <Available width={20} height={20} fill={colors.jungle_500} />
                                                    ) : (
                                                        ''
                                                    )}
                                                    {data.status === 'inactive' && data.checkin === 'no' ? (
                                                        <Minus width={18} height={18} fill={colors.jaffa_400} />
                                                    ) : (
                                                        ''
                                                    )}
                                                </Stack>
                                            </Box>
                                        </Stack>
                                        <Stack direction="row" alignItems="center">
                                            <Stack
                                                alignItems="flex-start"
                                                className="summary-singleline"
                                                sx={{ mt: 1, mb: 0.5 }}
                                            >
                                                {isAdmin ? (
                                                    <Box display="flex" className="box-item" mr={0.8} mb={0.7}>
                                                        <Box sx={{ mt: '2px', position: 'relative' }}>
                                                            <Desk
                                                                width={17}
                                                                height={17}
                                                                fill={colors.teal_500}
                                                                outlined
                                                            />
                                                        </Box>
                                                        <Typography className="text-summary" pl={1}>
                                                            {data.room}
                                                        </Typography>
                                                    </Box>
                                                ) : (
                                                    ''
                                                )}
                                                <Box display="flex" className="box-item" mb={0.7}>
                                                    <Date width={18} height={18} fill={colors.teal_500} />
                                                    <Typography className="text-summary" pl={0.7}>
                                                        {data.date}
                                                    </Typography>
                                                </Box>
                                            </Stack>
                                        </Stack>
                                    </Box>
                                    <Box className="box-action">
                                        <Stack
                                            direction="row"
                                            alignItems="center"
                                            justifyContent={
                                                !isAdmin && data.status === 'active' ? 'space-between' : 'flex-end'
                                            }
                                        >
                                            {!isAdmin && data.status === 'active' ? (
                                                <Button
                                                    variant="outlined"
                                                    color="primary"
                                                    className="action-qrcode"
                                                    onClick={() => setIsQRCode(!isQRCode)}
                                                >
                                                    Scan Code
                                                </Button>
                                            ) : (
                                                ''
                                            )}
                                            {isAdmin || (data.status === 'active' && data.checkin === 'no') ? (
                                                <Box className="action-dots" onClick={onClickPopoverActions}>
                                                    <VerticalMenu width={30} height={30} fill={colors.teal_500} />
                                                </Box>
                                            ) : (
                                                ''
                                            )}
                                        </Stack>
                                    </Box>
                                </Stack>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Stack direction="row" alignItems="flex-start" spacing={0.5} sx={{ mb: 0.5 }}>
                                    <Box sx={{ ml: '-1px' }}>
                                        <Duration width={22} height={22} fill={colors.teal_500} />
                                    </Box>
                                    <Box sx={{ pl: 0.3 }}>
                                        <Typography className="text-summary">{data.time}</Typography>
                                    </Box>
                                </Stack>
                                {data.repeat_type !== 'None' ? (
                                    <Stack direction="row" alignItems="flex-start" spacing={1.2} sx={{ mb: 0.5 }}>
                                        <Box sx={{ pt: 0.5, ml: '-3px' }}>
                                            <Repeat width={23} height={24} fill={colors.teal_500} />
                                        </Box>
                                        <Box>
                                            <Typography className="text-summary">
                                                {data.repeat_type}, {data.repeat_notes}
                                            </Typography>
                                        </Box>
                                    </Stack>
                                ) : (
                                    ''
                                )}
                                <Stack direction="column">
                                    <Stack direction="row" alignItems="flex-start" spacing={1.2}>
                                        <Meeting width={18} height={18} fill={colors.teal_500} />
                                        <Typography className="text-guest-count">4 Guests</Typography>
                                    </Stack>
                                    {/* TODO: Remove limit dynamic API */}
                                    {dataStaff.slice(startSlice, endSlice).map((data, idx) => {
                                        return (
                                            <Box key={idx} sx={{ width: '100%' }}>
                                                <Stack direction="row" alignItems="center" sx={{ pl: 3, pb: 0.5 }}>
                                                    <Avatar
                                                        alt={data.nickname}
                                                        src={data.image}
                                                        sx={{ width: 25, height: 25 }}
                                                    />
                                                    <Typography variant="body2" className="text-guest-name">
                                                        {data.nickname}
                                                    </Typography>
                                                    <Box>
                                                        <Available width={20} height={20} fill={colors.jungle_500} />
                                                    </Box>
                                                    <Box
                                                        sx={{
                                                            top: '-1px',
                                                            position: 'relative',
                                                        }}
                                                    >
                                                        <Minus width={18} height={18} fill={colors.jaffa_400} />
                                                    </Box>
                                                </Stack>
                                            </Box>
                                        )
                                    })}
                                </Stack>
                                <Stack direction="row" alignItems="flex-start" spacing={1.2} mt={1}>
                                    <Box>
                                        <Note width={18} height={18} fill={colors.teal_500} />
                                    </Box>
                                    <Box>
                                        <Typography className="text-summary">{data.notes}</Typography>
                                    </Box>
                                </Stack>
                            </AccordionDetails>
                        </Accordion>
                    </Box>
                ))}

                <Box sx={{ mt: 2, mb: 0.5, textAlign: 'center' }}>
                    <CircularProgress text={'Loading...'} />
                </Box>

                <Popover
                    isShow={isPopoverActions}
                    onClose={() => setOpenPopoverActions(!isPopoverActions)}
                    anchorEl={anchorEl}
                    style={{
                        '.MuiPaper-root': {
                            borderRadius: '10px',
                        },
                        '.MuiButton-text': {
                            justifyContent: 'flex-start',
                            padding: '10px 25px',
                            textTransform: 'capitalize',
                            color: colors.grey_900,
                            borderBottom: `1px solid ${colors.grey_50}`,
                            '&:last-child': {
                                borderBottom: 'unset',
                            },
                        },
                    }}
                    content={
                        <Stack direction="column" sx={{ width: '120px' }}>
                            <Button variant="text">Edit</Button>
                            <Button variant="text" onClick={() => setDeleteBooking(!isDeleteBooking)}>
                                Delete
                            </Button>
                        </Stack>
                    }
                />

                {/* <ModalScanQR isQRCode={isQRCode} onClose={() => setIsQRCode(!isQRCode)} code="Hello World!" /> */}

                <ActionPreviewDoc
                    isShow={isDeleteBooking}
                    onClose={() => setDeleteBooking(!isDeleteBooking)}
                    data={'Are you sure want to delete this?'}
                    btnYes={() => console.log('Yes button clicked')}
                    btnYesText={'Yes'}
                    btnNoText={'No'}
                />
            </CssTableMeetingRoomMobile>
        </Box>
    )
}
