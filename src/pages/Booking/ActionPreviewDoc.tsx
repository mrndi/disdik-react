import { useState } from 'react'
import { Button, Box, Typography, Stack, CardMedia } from '@mui/material'
import { Modal } from '../../component/Modal'

interface ActionPreviewDocProps {
    isShow: boolean
    onClose: () => void
    data: string
    btnYesText: string
    btnNoText: string
    btnYes: () => void
    isDisabled?: boolean
    btnNo?: () => void
}

export const ActionPreviewDoc = ({ isShow, onClose, data, btnYesText, btnNoText, btnYes, isDisabled, btnNo }: ActionPreviewDocProps) => {
    return (
        <Modal
            isShow={isShow}
            onClose={onClose}
            sx={{ maxWidth: '100%', outline: 'none', height: '30em' }}
        >
            <Box
                sx={{
                    height: '80%',
                    marginBottom: '0.5em',
                }}>
                <iframe src={data} width="100%" height="300" />
            </Box >

            <Box>
                <Stack
                    direction="row"
                    justifyContent="center"
                    spacing={2}
                    sx={{
                        button: {
                            width: '150px',
                            maxWidth: '100%',
                        },
                    }}
                >
                    <Button variant="outlined" color="primary" size="large" onClick={btnNo}>
                        {btnNoText}
                    </Button>
                    <Button variant="contained" color="primary" size="large" onClick={btnYes} disabled={isDisabled}>
                        {btnYesText}
                    </Button>
                </Stack>
            </Box>
        </Modal>
    )
}
