import React, { useState } from 'react'
import {
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Button,
    Box,
    Chip,
    Paper,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Typography,
} from '@mui/material'
import { toast } from 'react-hot-toast'
import { Pagination } from '../../component/Pagination'
import { Popover } from '../../component/Form'
import { CircularProgress } from '../../component/Progress'
import { ActionPreviewDoc } from './ActionPreviewDoc'
import { CssTableInbox, CssTableInboxMobile } from './style'
import { Available, Status, Date, Delete, Desk, Edit, Minus, Note, Repeat, VerticalMenu, Send, Envelope, Scan } from '../../assets/icons'
import { colors } from '../../themes'
import { dataWorkingDesk } from '../../fake-db/workingdesk'
import { TableHeader } from '../../containers/TableHeader'
import { convertDateToString } from '../../utils/convertDate'
import { FilePresentOutlined, HistoryEduOutlined } from '@mui/icons-material'
import { StatusSurat } from './StatusSurat'


const startString = 0
const MAX_NOTE = 25

interface TableDisposisiProps {
    dataSurat: any
    isExternal?: boolean
    isOutbox?: boolean
}

export const TableDisposisi = ({ dataSurat, isOutbox }: TableDisposisiProps) => {

    const [anchorEl, setAnchorEl] = useState() as any

    const [namaFile, setNamaFile] = useState('')
    const [isSelesai, setSelesai] = useState(false)
    //Modal
    const [isProcessDoc, setProcessDoc] = useState(false)

    //Popover: Repeat
    const [isPopoverRepeat, setOpenPopoverRepeat] = useState(false)
    const [PopoverContentRepeat] = useState() as any

    //Popover: Notes
    const [isPopoverNotes, setOpenPopoverNotes] = useState(false)
    const [PopoverNotes, setPopoverNotes] = useState() as any

    const onClickPopoverNotes = (e: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(e.currentTarget)
        setOpenPopoverNotes((prevState) => !prevState)
        setPopoverNotes(e.currentTarget.title)
    }

    const titles = [
        { name: 'No. Surat', alias: 'no-surat' },
        { name: 'Diteruskan Oleh', alias: 'desk' },
        { name: 'Diverifikasi Oleh', alias: 'desk' },
        { name: 'Tanggal Disposisi', alias: 'date-disposisi' },
        { name: 'Tanggal Proses', alias: 'date-proses' },
        { name: 'Status', alias: 'status' },
        { name: 'Catatan', alias: 'note' },
        { name: 'Aksi', alias: 'action' },
    ]

    const handleSortChange = (alias: string, direction: string) => {
        console.log(alias, direction)
    }




    return (
        <Box display={{ xs: 'none', sm: 'block' }}>
            <CssTableInbox>
                <TableContainer component={Paper} elevation={0} className="booking-table-workingdesk">
                    <Table>
                        <TableHeader titles={titles} onSortChange={handleSortChange} />
                        <TableBody>
                            {dataSurat === null ?
                                <TableRow>
                                    <TableCell />
                                    <TableCell />
                                    <TableCell>
                                        <Box sx={{ mt: 2, mb: 0.5, textAlign: 'center' }}>
                                            <CircularProgress text={'Loading...'} />
                                        </Box>
                                    </TableCell>
                                </TableRow>
                                : dataSurat?.map((data: any, idx: any) => (
                                    <TableRow key={idx} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                        <TableCell>{data.noSurat || "Belum ada No Surat"}</TableCell>
                                        <TableCell>{data.pengoreksi?.name}</TableCell>
                                        <TableCell>{data.penerima?.name}</TableCell>
                                        <TableCell>{convertDateToString(data.tglDisposisi)}</TableCell>
                                        <TableCell>{convertDateToString(data.tglProses)}</TableCell>
                                        <TableCell>
                                            <StatusSurat status={data.status as string} />
                                        </TableCell>
                                        <TableCell>{data.catatan}</TableCell>
                                        <TableCell sx={{ textAlign: 'left' }}>
                                            <Stack direction="row" alignItems="center" justifyContent="flex-start">
                                                <Box
                                                    display="flex"
                                                    alignItems="center"
                                                    onClick={() => toast.error('Fitur belum tersedia')}
                                                    sx={{ cursor: 'pointer' }}
                                                >
                                                    <FilePresentOutlined
                                                        width={40}
                                                        height={40}
                                                        color="primary" />
                                                </Box>
                                            </Stack>
                                        </TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>

                <Pagination />

                <Popover
                    isShow={isPopoverRepeat}
                    onClose={() => setOpenPopoverRepeat(!isPopoverRepeat)}
                    anchorEl={anchorEl}
                    content={
                        <Box sx={{ maxWidth: '350px' }} p={2}>
                            <Typography variant="body2">{PopoverContentRepeat}</Typography>
                        </Box>
                    }
                />

                <Popover
                    isShow={isPopoverNotes}
                    onClose={() => setOpenPopoverNotes(!isPopoverNotes)}
                    anchorEl={anchorEl}
                    content={
                        <Box sx={{ maxWidth: '350px' }} p={2}>
                            <Typography variant="body2">{PopoverNotes}</Typography>
                        </Box>
                    }
                />

                {/* <ModalScanQR isQRCode={isQRCode} onClose={() => setIsQRCode(!isQRCode)} code="Hello World!" /> */}

                <ActionPreviewDoc
                    isShow={isProcessDoc}
                    onClose={() => setProcessDoc(!isProcessDoc)}
                    data={`https://docs.google.com/gview?url=http://103.56.148.155:3000/static/file/${namaFile}&embedded=true`}
                    btnYes={() => console.log('Yes button clicked')}
                    btnYesText={'Process'}
                    btnNoText={'Download'}
                    isDisabled={isSelesai}
                    btnNo={() => window.location.href = `http://103.56.148.155:3000/static/file/${namaFile}`}
                />
            </CssTableInbox>
        </Box>
    )
}

export const TableDisposisiMobile = () => {
    // const userState = useSelector((state) => state.user)
    const isAdmin = true

    //Popover
    const [anchorEl, setAnchorEl] = useState() as any
    const [isPopoverActions, setOpenPopoverActions] = useState(false)
    const onClickPopoverActions = (e: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(e.currentTarget)
        setOpenPopoverActions((prevState) => !prevState)
    }

    //Modal
    const [isQRCode, setIsQRCode] = useState(false)
    const [isProcessDoc, setProcessDoc] = useState(false)

    return (
        <Box display={{ xs: 'block', sm: 'none' }}>
            <CssTableInboxMobile>
                {dataWorkingDesk.map((data, idx) => (
                    <Box className="booking-accordion" key={idx}>
                        <Accordion className="booking-accordion-item">
                            <AccordionSummary
                                expandIcon={false}
                                id={`accordion-${data.id}`}
                                className="booking-accordion-item-summary"
                            >
                                <Stack direction="row" justifyContent="space-between" sx={{ width: '100%' }}>
                                    <Box className="box-title">
                                        <Stack direction="row" spacing={0.7} sx={{ mb: 1 }}>
                                            {isAdmin ? (
                                                <>
                                                    <Typography className="text-title limit">
                                                        {data.staff_name}
                                                    </Typography>
                                                    <Chip label="New" size="small" className="chip-new" />
                                                </>
                                            ) : (
                                                <Typography className="text-title">
                                                    {data.floor}, {data.desk}
                                                </Typography>
                                            )}
                                            {data.status === 'inactive' && data.checkin === 'yes' ? (
                                                <Available width={20} height={20} fill={colors.jungle_500} />
                                            ) : (
                                                ''
                                            )}
                                            {data.status === 'inactive' && data.checkin === 'no' ? (
                                                <Minus width={18} height={18} fill={colors.jaffa_400} />
                                            ) : (
                                                ''
                                            )}
                                        </Stack>

                                        <Stack direction="row" alignItems="center">
                                            <Stack direction="row" alignItems="flex-start" spacing={1} sx={{ mb: 0.5 }}>
                                                {isAdmin ? (
                                                    <>
                                                        <Box sx={{ mt: '2px', position: 'relative' }}>
                                                            <Desk
                                                                width={17}
                                                                height={17}
                                                                fill={colors.teal_500}
                                                                outlined
                                                            />
                                                        </Box>
                                                        <Typography className="text-summary">
                                                            {data.desk}, {data.floor}
                                                        </Typography>
                                                    </>
                                                ) : (
                                                    ''
                                                )}
                                                <Date width={18} height={18} fill={colors.teal_500} />
                                                <Typography className="text-summary">{data.date}</Typography>
                                            </Stack>
                                        </Stack>
                                    </Box>
                                    <Box className={'box-action' + (!isAdmin ? ' limit' : '')}>
                                        <Stack
                                            direction="row"
                                            alignItems="center"
                                            justifyContent={
                                                !isAdmin && data.status === 'active' ? 'space-between' : 'flex-end'
                                            }
                                        >
                                            {!isAdmin && data.status === 'active' ? (
                                                <Button
                                                    variant="outlined"
                                                    color="primary"
                                                    className="action-qrcode"
                                                    onClick={() => setIsQRCode(!isQRCode)}
                                                >
                                                    Scan Code
                                                </Button>
                                            ) : (
                                                ''
                                            )}
                                            {isAdmin || (data.status === 'active' && data.checkin === 'no') ? (
                                                <Box className="action-dots" onClick={onClickPopoverActions}>
                                                    <VerticalMenu width={30} height={30} fill={colors.teal_500} />
                                                </Box>
                                            ) : (
                                                ''
                                            )}
                                        </Stack>
                                    </Box>
                                </Stack>
                            </AccordionSummary>
                            <AccordionDetails>
                                {data.repeat_type !== 'None' ? (
                                    <Stack direction="row" alignItems="flex-start" spacing={0.7} sx={{ mb: 0.5 }}>
                                        <Box sx={{ pt: 0.5, ml: '-3px' }}>
                                            <Repeat width={23} height={24} fill={colors.teal_500} />
                                        </Box>
                                        <Box>
                                            <Typography className="text-summary">
                                                {data.repeat_type}, {data.repeat_notes}
                                            </Typography>
                                        </Box>
                                    </Stack>
                                ) : (
                                    ''
                                )}
                                <Stack direction="row" alignItems="flex-start" spacing={1.2}>
                                    <Box>
                                        <Note width={18} height={18} fill={colors.teal_500} />
                                    </Box>
                                    <Box>
                                        <Typography className="text-summary">{data.notes}</Typography>
                                    </Box>
                                </Stack>
                            </AccordionDetails>
                        </Accordion>
                    </Box>
                ))}

                <Box sx={{ mt: 2, mb: 0.5, textAlign: 'center' }}>
                    <CircularProgress text={'Loading...'} />
                </Box>

                <Popover
                    isShow={isPopoverActions}
                    onClose={() => setOpenPopoverActions(!isPopoverActions)}
                    anchorEl={anchorEl}
                    className="popover-actions"
                    style={{
                        '.MuiPaper-root': {
                            borderRadius: '10px',
                        },
                        '.MuiButton-text': {
                            justifyContent: 'flex-start',
                            padding: '10px 25px',
                            textTransform: 'capitalize',
                            color: colors.grey_900,
                            borderBottom: `1px solid ${colors.grey_50}`,
                            '&:last-child': {
                                borderBottom: 'unset',
                            },
                        },
                    }}
                    content={
                        <Stack direction="column" sx={{ width: '120px' }}>
                            <Button variant="text">Edit</Button>
                            <Button variant="text" onClick={() => setProcessDoc(!isProcessDoc)}>
                                Delete
                            </Button>
                        </Stack>
                    }
                />

            </CssTableInboxMobile>
        </Box>
    )
}
