import { useNavigate } from '@tanstack/react-location'
import { ThemeProvider } from '@mui/material/styles'
import { Button, Box, Typography } from '@mui/material'
import { mainTheme } from '../../themes'
import { colors } from '../../themes'

const NotFound = () => {
    const navigate = useNavigate()

    return (
        <ThemeProvider theme={mainTheme}>
            <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                flexDirection="column"
                sx={{
                    p: 0,
                    m: 0,
                    textAlign: 'center',
                    backgroundColor: colors.zircon_500,
                    minHeight: '100vh',
                }}
            >
                <Box display="flex" justifyContent="center" alignItems="center" flexDirection="column">
                    <Typography
                        component="h1"

                        fontSize={{ xs: '7rem', sm: '10rem' }}
                        color={colors.teal_500}
                    >
                        404
                    </Typography>
                    <Typography
                        component="h2"

                        fontSize={{ xs: '1.7rem', sm: '2rem' }}
                        color={colors.teal_500}
                    >
                        Page Not Found
                    </Typography>
                    <Box width={{ xs: '200px', sm: 'auto' }} mt={1}>
                        <Typography
                            component="div"

                            color={colors.teal_500}
                            textAlign="center"
                            lineHeight={1.5}
                        >
                            Sorry we can&apos;t find the page you&apos;re looking for.
                        </Typography>
                    </Box>
                </Box>
                <Box display="flex" justifyContent="center" alignItems="center" mt={6}>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => navigate({ to: '/' })}
                        sx={{
                            textTransform: 'unset',
                        }}
                    >
                        Back to dashboard
                    </Button>
                </Box>
            </Box>
        </ThemeProvider>
    )
}

export default NotFound
