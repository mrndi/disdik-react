import React from "react"


export enum suratmasuk_status {
    Diterima = "Diterima",
    Proses = "Proses",
    Disposisi = "Disposisi",
    Ditolak = "Ditolak",
    Disetujui = "Disetujui",
    Selesai = "Selesai"
}

enum disposisi_status {
    Disposisi = "Disposisi",
    Proses = "Proses",
    Selesai = "Selesai"
}



interface suratMasukFormProps {
    onSubmit: (e: React.FormEvent<HTMLFormElement>) => void
    onFileChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

export const SuratmasukForm = ({ onSubmit, onFileChange }: suratMasukFormProps) => {
    return (
        <form onSubmit={onSubmit} className="p-5 sm:px-10 md:p-10 md:px-8">
            <div className="form-group mb-6">
                <input type="text" name="perihal" className="form-control block
                                                w-full
                                                px-3
                                                py-1.5
                                                text-base
                                                font-normal
                                                text-gray-700
                                                bg-white bg-clip-padding
                                                border border-solid border-gray-300
                                                rounded
                                                transition
                                                ease-in-out
                                                m-0
                                                focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="Perihal Surat" />
            </div>
            <div className="form-group mb-6">
                <input type="text" name="asalSurat" className="form-control block
                                                w-full
                                                px-3
                                                py-1.5
                                                text-base
                                                font-normal
                                                text-gray-700
                                                bg-white bg-clip-padding
                                                border border-solid border-gray-300
                                                rounded
                                                transition
                                                ease-in-out
                                                m-0
                                                focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="Asal Surat" />
            </div>

            <div className="form-group mb-6">
                <input type="text" name="layanan" className="form-control block
                                                    w-full
                                                    px-3
                                                    py-1.5
                                                    text-base
                                                    font-normal
                                                    text-gray-700
                                                    bg-white bg-clip-padding
                                                    border border-solid border-gray-300
                                                    rounded
                                                    transition
                                                    ease-in-out
                                                    m-0
                                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="layanan" />
            </div>
            <div className="form-group mb-6">
                <input type="file" name="file" onChange={onFileChange} className="file-input file-input-bordered w-full max-w-xs" placeholder="Upload Surat" />
            </div>

            <button type="submit" className="
                                                w-full
                                                px-6
                                                py-2.5
                                                bg-blue-600
                                                text-white
                                                font-medium
                                                text-xs
                                                leading-tight
                                                uppercase
                                                rounded
                                                shadow-md
                                                hover:bg-blue-700 hover:shadow-lg
                                                focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0
                                                active:bg-blue-800 active:shadow-lg
                                                transition
                                                duration-150
                                                ease-in-out">Send Letter</button>
        </form>
    )

}
