import { Link, useNavigate } from "@tanstack/react-location"
import axios from "axios"
import { useQueryClient, useQuery, useMutation } from "react-query"
import { suratmasuk_status } from "./SuratmasukForm"
import React from "react"
import { toast } from "react-hot-toast"
import { getToken, tokenPayload } from "../../helper/AuthHelper"
import { TableSuratmasuk } from "./TableSuratmasuk"

type SS = keyof typeof suratmasuk_status

export const Suratmasuk = () => {
    const navigate = useNavigate()
    const queryClient = useQueryClient()
    const config = {
        headers: {
            "Authorization": "Bearer " + getToken()
        }

    }

    const [idSurat, setIdSurat] = React.useState<number>(0)
    const [roleId, setRoleId] = React.useState<string>('')
    const [subbidangId, setSubbidangId] = React.useState<string>('')
    const [penerimaId, setPenerimaId] = React.useState<string>('')
    const [isDissabled, setIsDissabled] = React.useState<boolean>(true)
    const [status, setStatus] = React.useState<string>('')
    const [tabActive, setTabActive] = React.useState<string>('all')

    const userToken = getToken()
    const userAuth = userToken ? tokenPayload() : null
    const userId = userAuth?.id




    const roles = useQuery('roles', async () => await axios.get("http://103.56.148.155:3000/api/v1/roles"))
    const subbidang = useQuery('subbidang', async () => await axios.get("http://103.56.148.155:3000/api/v1/subbidang"))
    const users = useQuery('users', async () => await axios.get("http://103.56.148.155:3000/api/v1/users" + `?roleId=${roleId}&subId=${subbidangId}`, config))

    React.useEffect(() => {
        if (roleId) {
            users.refetch()
        }
        if (subbidangId) {
            users.refetch()
        }

    }, [roleId, subbidangId])

    const mutation = useMutation((data: any) => axios.put(`http://103.56.148.155:3000/api/v1/suratmasuk/${idSurat}`, data), {
        onSuccess: () => {
            const message = 'Surat Telah Diupdate'
            toast.success(message)
            window.location.reload()
        },
        onError: (error) => {
            if (axios.isAxiosError(error)) {
                const message = error.response?.data.message
                toast.error(message)
            }
        },
    })

    const handleClickSurat = (id: number) => {
        setIdSurat(id)
        document.getElementById('modal-process')!.classList.add('modal-open')
    }

    const handleCloseModalProcess = () => {
        document.getElementById('modal-process')!.classList.remove('modal-open')
    }


    const handleEditSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const target = e.target as typeof e.target & {
            perihal: { value: string };
            layanan: { value: string };
            status: { value: SS };
            noSurat: { value: string };
        };
        const noSurat = target.noSurat.value;
        const perihal = target.perihal.value;
        const layanan = target.layanan.value;
        const status = target.status.value;

        const data = { perihal, layanan, status, noSurat }

        mutation.mutate(data)
    }

    const handleProscessSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const target = e.target as typeof e.target & {
            noSurat: { value: string };
            status: { value: SS };
        };
        const noSurat = target.noSurat.value;
        const holderId = penerimaId
        const status = target.status.value

        const data = { noSurat: noSurat ? noSurat : undefined, holderId, status }

        mutation.mutate(data)
    }

    const handleDeleteSurat = (id: number) => {
        const message = 'Surat Telah Dihapus'
        axios.delete(`http://103.56.148.155:3000/api/v1/suratmasuk/${id}`, config)
            .then(() => {
                toast.success(message)
                window.location.reload()
            }).catch((error) => {
                if (axios.isAxiosError(error)) {
                    const message = error.response?.data.message
                    toast.error(message)
                }
            })
    }


    const suratMasuk = useQuery('suratMasuk', () => axios.get("http://103.56.148.155:3000/api/v1/suratmasuk" + `?holderId=${tabActive == 'holded' ? userId : ""}&usersId=${tabActive == 'owned' ? userId : ""}`),
        {
            onError: (error: any) => {
                if (axios.isAxiosError(error)) {
                    const message = error.response?.data.message
                    toast.error(message)
                }
            }
        })

    const handleclickSubmit = (status: string) => {
        if (status === "") {
            setIsDissabled(true)
        }
        else {
            setIsDissabled(false)
        }
    }

    const handleEditbuttonClick = (id: number) => {
        setIdSurat(id)
    }

    const handleTabClick = (e: React.MouseEvent<HTMLButtonElement>) => {
        const tabArray = ['all', 'holded', 'owned']
        const target = e.target as HTMLButtonElement
        const id = target.id

        if (tabArray.includes(id)) {
            setTabActive(id)
            document.getElementById(id)!.classList.add('tab-active')

            tabArray.forEach((tab) => {
                if (tab !== id) {
                    document.getElementById(tab)!.classList.remove('tab-active')
                }
            })
        }
    }


    React.useEffect(() => {
        if (tabActive === "all") {
            queryClient.invalidateQueries('suratMasuk')
            suratMasuk.refetch()
        }
        if (tabActive === "holded") {
            queryClient.invalidateQueries('suratMasuk')
            suratMasuk.refetch({
            })
        }
        if (tabActive === "owned") {
            queryClient.invalidateQueries('suratMasuk')
            suratMasuk.refetch()
        }

        return () => {
            suratMasuk.refetch()
        }

    }, [tabActive])

    if (suratMasuk?.data?.data.length === 0) {
        return (
            <div className="flex flex-col items-center justify-center h-full">
                <div className="flex flex-col items-center justify-center">
                    <h1 className="text-2xl font-bold text-gray-500">Tidak ada data</h1>
                </div>
            </div>
        )
    }
    return (
        <div className="overflow-x-auto">
            <div className="tabs ml-3 w-full justify-start">
                <button id="all" className="tab tab-lg tab-lifted tab-active" onClick={handleTabClick}>
                    Semua surat
                </button>
                <button id="holded" className="tab tab-lg tab-lifted " onClick={handleTabClick}>
                    Surat diterima
                </button>
                <button id="owned" className="tab tab-lg tab-lifted " onClick={handleTabClick}>
                    Surat dimiliki
                </button>
            </div >

            <>
                {
                    <TableSuratmasuk
                        mutation={mutation}
                        suratMasukData={suratMasuk?.data?.data}
                        onProses={handleClickSurat}
                        onSubmitEdit={handleEditSubmit}
                        isSubmitDissable={isDissabled}
                        onDelete={handleDeleteSurat}
                        onEditClick={handleEditbuttonClick}
                    />
                }

            </>

            <input type="checkbox" className="modal-toggle" />
            <label id="modal-process" className="modal">
                <label className="modal-box  w-11/12 max-w-3xl relative" htmlFor="">
                    <div className="modal-header">
                        <h2 className="modal-title">Proses Surat Masuk</h2>

                    </div>
                    <label htmlFor="modal-process" onClick={handleCloseModalProcess} className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
                    <div className="modal-body">
                        <div className="modal-content">
                            <div className="card">
                                <form onSubmit={handleProscessSubmit}>
                                    <div className="form-control">
                                        <label className="label">
                                            <span className="label-text">No Surat</span>
                                        </label>
                                        <input type="text" name="noSurat" placeholder="*Optional" className="input input-bordered" />
                                    </div>
                                    <div className="form-control">
                                        <label className="label">
                                            <span className="label-text">Kirim Ke</span>
                                        </label>
                                        <div className="ml-10 mt-1 form-control">
                                            <select
                                                name="role"
                                                className="select select-bordered w-full max-w-xs"
                                                onChange={(e) => setRoleId(e.target.value)}
                                            >
                                                <option value="">Pilih Jabatan</option>
                                                {roles?.data?.data.map((item: any, index: number) => (
                                                    <option key={index} value={item.id}>{item.role}</option>
                                                ))}
                                            </select>
                                        </div>
                                        <div className="ml-10 mt-5 form-control">
                                            <select name="bidang" className="select select-bordered w-full max-w-xs"
                                                onChange={(e) => setSubbidangId(e.target.value)}
                                            >
                                                <option value="">Pilih Bidang</option>
                                                {subbidang?.data?.data.map((item: any, index: number) => (
                                                    <option key={index} value={item.id} >{item.namaBidang}</option>
                                                ))}
                                            </select>
                                        </div>

                                    </div>
                                    <div className="form-control">
                                        <label className="label">
                                            <span className="label-text">Nama Penerima</span>
                                        </label>
                                        <select name="penerima" className="select select-bordered w-full max-w-xs"
                                            onChange={(e) => setPenerimaId(e.target.value)}
                                        >
                                            <option value="">Pilih Penerima</option>
                                            {
                                                users?.data?.data.map((item: any, index: number) => (
                                                    <option key={index} value={item.id}>{item.username} - {item.name} - {item.subbidang.namaBidang}</option>
                                                ))
                                            }
                                        </select>
                                    </div>
                                    <div className="form-control">
                                        <label className="label">
                                            <span className="label-text">Status</span>
                                        </label>
                                        <select name="status" className="select select-bordered w-full max-w-xs"
                                            onChange={(e) => handleclickSubmit(e.target.value)}>
                                            <option value="">Pilih</option>
                                            {(Object.keys(suratmasuk_status) as SS[]).map((key) => (
                                                <option key={key} value={key}>{suratmasuk_status[key]}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className="form-control mt-5">
                                        <button type="submit" className="btn btn-primary" disabled={isDissabled}>
                                            {mutation.isLoading ? 'Loading...' : 'Submit'}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </label>
            </label>
        </div >


    )

}

