import { useNavigate } from "@tanstack/react-location"
import axios from "axios"
import { useMutation, useQueryClient } from "react-query"
import { tokenPayload } from "../../helper/AuthHelper"
import { SuratmasukForm } from "./SuratmasukForm"
import React from "react"
import toast from "react-hot-toast"

enum suratmasuk_status {
    Diterima = "Diterima",
    Proses = "Proses",
    Disposisi = "Disposisi",
    Ditolak = "Ditolak",
    Disetujui = "Disetujui",
    Selesai = "Selesai"
}

export const UploadSurat = () => {
    const [file, setFile] = React.useState<File | null>(null)

    const navigate = useNavigate()
    const user = tokenPayload().id

    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    const mutation = useMutation((data: any) => axios.post("http://103.56.148.155:3000/api/v1/suratmasuk", data, config), {
        onSuccess: () => {
            const message = 'Surat anda Diterima'
            toast.success(message)
            return navigate({ to: '/dashboard' })
        },
        onError: (error: any) => {
            const message = error.response.data.message
            return message
        }
    })

    const handleFile = (e: React.ChangeEvent) => {
        const target = e.target as HTMLInputElement
        setFile(target.files![0])
    }

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const target = e.target as typeof e.target & {
            perihal: { value: string };
            asalSurat: { value: string };
            layanan: { value: string };
        };

        const perihal = target.perihal.value;
        const asalSurat = target.asalSurat.value;
        const layanan = target.layanan.value;
        const status = suratmasuk_status.Diterima;
        const userId = user;

        mutation.mutate({ perihal, asalSurat, userId, status, layanan, file })
    }

    return (
        <div className="flex justify-center mt-10">

            {mutation.isLoading ?
                ('loading...')
                : (
                    <>
                        {mutation.isError ? (
                            <div>{mutation.error.response.data.message as string}</div>
                        ) : (
                            <div>{mutation.isSuccess ?
                                'Success'
                                : <SuratmasukForm onSubmit={handleSubmit} onFileChange={handleFile} />}</div>
                        )}
                    </>
                )}

        </div>
    );
}
