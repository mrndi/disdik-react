import { UseQueryResult } from "react-query"
import { suratmasuk_status } from "./SuratmasukForm"


type SS = keyof typeof suratmasuk_status
interface TableSuratmasukProps {
    mutation: any,
    suratMasukData: any,
    onProses: any,
    onSubmitEdit: any,
    isSubmitDissable: boolean,
    onDelete: any,
    onEditClick: any,
}



export const TableSuratmasuk = (
    {
        mutation,
        suratMasukData,
        onProses,
        onSubmitEdit,

        isSubmitDissable,

        onDelete,

        onEditClick,

    }: TableSuratmasukProps
) => {

    return (
        <>
            <table className="table table-compact table-zebra w-full mt-2 mx-5">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No Surat</th>
                        <th>Pengirim</th>
                        <th>Pemegang</th>
                        <th>Tanggal Surat</th>
                        <th>Perihal</th>
                        <th>File</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {suratMasukData?.map((item: any, index: number) => (
                        <tr key={index} className="hover ">
                            <td>{index + 1}</td>
                            <td>{item.noSurat}</td>
                            <td>{item.users?.name}</td>
                            <td>{item.usersHolder?.name}</td>
                            <td>{new Date(item.tglMasuk).toLocaleDateString()}</td>
                            <td>{item.perihal}</td>
                            <td>{item.namaFile}</td>
                            <td>{item.status}</td>
                            <td>
                                <span className="btn btn-outline mr-1" onClick={() => onProses(item.id)}>
                                    Proses
                                </span>
                                <label htmlFor="update-surat-modal" className="btn btn-outline btn-warning mr-1" onClick={() => onEditClick(item.id)}>Edit</label>
                                <input type="checkbox" id="update-surat-modal" className="modal-toggle" />
                                <label htmlFor="update-surat-modal" className="modal">
                                    <label className="modal-box  w-11/12 max-w-3xl relative" htmlFor="">
                                        <div className="modal-header">
                                            <h2 className="modal-title">Edit Surat Masuk</h2>
                                        </div>
                                        <label htmlFor="update-surat-modal" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
                                        <div className="modal-body">
                                            <div className="modal-content">
                                                <div className="card">
                                                    <form onSubmit={onSubmitEdit}>
                                                        <div className="form-control">
                                                            <label className="label">
                                                                <span className="label-text">No Surat</span>
                                                            </label>
                                                            <input type="text" name="noSurat" placeholder="No Surat" className="input input-bordered" />
                                                        </div>
                                                        <div className="form-control">
                                                            <label className="label">
                                                                <span className="label-text">Perihal</span>
                                                            </label>
                                                            <input type="text" name="perihal" placeholder="Perihal" className="input input-bordered" />
                                                        </div>
                                                        <div className="form-control">
                                                            <label className="label">
                                                                <span className="label-text">Layanan</span>
                                                            </label>
                                                            <input type="text" name="layanan" placeholder="Layanan" className="input input-bordered" />
                                                        </div>
                                                        <div className="form-control">
                                                            <label className="label">
                                                                <span className="label-text">Status</span>
                                                            </label>
                                                            <select name="status" className="select select-bordered w-full max-w-xs">
                                                                <option value="">{item.status}</option>
                                                                {(Object.keys(suratmasuk_status) as SS[]).map((key) => (
                                                                    <option key={key} value={key}>{suratmasuk_status[key]}</option>
                                                                ))}
                                                            </select>
                                                        </div>
                                                        <div className="form-control mt-5">
                                                            <button type="submit"
                                                                className="btn btn-primary"
                                                                disabled={isSubmitDissable}
                                                            >
                                                                {mutation.isLoading ? 'Loading...' : 'Submit'}
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </label>
                                </label>
                                <span className="btn btn-outline btn-error" onClick={() => onDelete(item.id)}>
                                    Delete
                                </span>

                            </td>
                        </tr>
                    ))}
                </tbody>
            </table >


        </>
    )
}