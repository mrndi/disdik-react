import { Backdrop, Box, Modal as ModalMui, Fade, Typography, Stack } from '@mui/material'

import { Close } from '../../assets/icons'
import { colors } from '../../themes'

const style = {

}

interface ModalProps {
    title?: string
    isShow: boolean
    onClose: () => void
    children: any
    sx?: any
}

// Basic Modal
export const Modal = ({ title, isShow, onClose, children, sx }: ModalProps) => {
    return (
        <ModalMui
            aria-labelledby={title}
            aria-describedby="transition-modal-description"
            open={isShow}
            onClose={onClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
            sx={{
                width: sx?.width || '100%',
                maxWidth: sx?.maxWidth || '100%',
                outline: sx?.outline || 'none',
                left: sx?.left || '50%',
                top: sx?.top || '50%',
                height: sx?.height || '100%',
            }}
            disableEnforceFocus
            disableAutoFocus

        >
            <Fade in={isShow}>
                <Box sx={{
                    position: 'absolute',
                    width: '100%',
                    maxWidth: 768,
                    bgcolor: 'background.paper',
                    boxShadow: 24,
                    p: 3,
                    borderRadius: '6px',
                    transform: 'translate(-50%,-50%)',
                    height: '100%',
                }}>
                    <Stack
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                        className="modal-head"
                        sx={{ mb: 3 }}
                    >
                        <Typography variant="h5" component="h3" sx={{ fontWeight: 'bold' }}>
                            {title}
                        </Typography>
                        <Box
                            onClick={onClose}
                            sx={{
                                cursor: 'pointer',
                            }}
                        >
                            <Close width={20} height={20} fill={colors.grey_300} />
                        </Box>
                    </Stack>
                    {children}
                </Box>
            </Fade>
        </ModalMui>
    )
}