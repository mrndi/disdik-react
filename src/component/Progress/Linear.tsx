import Box from '@mui/material/Box'
import LinearProgress from '@mui/material/LinearProgress'
import { colors } from '../../themes'
import Logo from '/src/assets/images/Kemdikbud.png'




export const Linear = () => {
    return (
        <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            flexWrap="wrap"
            flexDirection="column"
            sx={{
                width: '100%',
                minHeight: '100vh',
                backgroundColor: 'white',
                textAlign: 'center',
                '.MuiLinearProgress-root': {
                    width: '250px',
                    backgroundColor: colors.grey_100,
                    borderRadius: '4px',
                },
                '.MuiLinearProgress-bar': {
                    backgroundColor: colors.teal_300,
                    borderRadius: '4px',
                },
                '.dibs-logo': {
                    width: '170px',
                    display: 'block',
                    mb: 4,
                },
            }}
        >
            <img src={Logo} alt="DIBS" className="dibs-logo" />
            <LinearProgress />
        </Box>
    )
}
