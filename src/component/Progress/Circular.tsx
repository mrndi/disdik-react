import { Box, Typography } from '@mui/material'
import CircularProgress from '@mui/material/CircularProgress'
import { colors } from '../../themes'


interface CircularProps {
    text: string
}

export const Circular = ({ text }: CircularProps) => {
    return (
        <Box display="flex" justifyContent="center" alignItems="center">
            <CircularProgress size={22} sx={{ color: colors.teal_700 }} />
            <Typography sx={{ color: colors.teal_700, pl: 2 }}>{text}</Typography>
        </Box>
    )
}