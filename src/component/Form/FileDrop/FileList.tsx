import React from 'react'

import { Box, LinearProgress, Stack, Typography } from '@mui/material'
import { File, Minus, Available } from '../../../assets/icons'
import { colors } from '../../../themes'

const DEFAULT_PROGRESS = 0
const COMPLETED_PROGRESS = 100

interface FileListProps {
    file: File
    onDelete: () => void
    useProgress?: boolean
    isLoading?: boolean
    isCompleted?: boolean
}

export const FileList = ({ file, onDelete, useProgress = false, isLoading = false, isCompleted = false }: FileListProps) => {
    const handleDeleteClick = () => {
        if (!isCompleted) {
            return onDelete()
        }
        return null
    }

    return (
        <Stack direction="row" alignItems="center">
            <File width={28} height={28} fill={colors.grey_500} />
            <Box sx={{ ml: 1, width: '100%' }}>
                <Stack direction="row" alignItems="center" justifyContent="space-between">
                    <Typography>{file.name}</Typography>
                    <Box
                        onClick={handleDeleteClick}
                        sx={{
                            cursor: 'pointer',
                        }}
                    >
                        {isCompleted ?
                            <Available width={16} height={16} fill={colors.green_500} />
                            : <Minus width={16} height={16} fill={colors.froly_500} />
                        }
                    </Box>
                </Stack>
                {!!useProgress && <LinearProgress
                    variant={isLoading ? "indeterminate" : "determinate"}
                    value={isCompleted ? COMPLETED_PROGRESS : DEFAULT_PROGRESS}
                />}
                <Typography variant="body2" sx={{ color: colors.grey_300 }}>
                    {file.size} bytes
                </Typography>
            </Box>
        </Stack>
    )
}