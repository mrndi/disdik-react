import React, { useEffect } from 'react'
import { useDropzone } from 'react-dropzone'
import { Button, Box, Typography } from '@mui/material'
import { Import } from '../../../assets/icons'
import { colors } from '../../../themes'

interface FileDropProps {
    onChange: (files: File[]) => void
}

export const FileDrop = ({ onChange = () => { } }: FileDropProps) => {
    const { acceptedFiles, getRootProps, getInputProps } = useDropzone()

    useEffect(() => {
        if (acceptedFiles.length) {
            onChange(acceptedFiles)
        }
    }, [acceptedFiles])

    return (
        <Box
            style={{
                height: '100%',
                width: '100%',
                padding: 10,
                backgroundColor: colors.teal_100,
            }}
        >
            <div
                {...getRootProps({ className: 'dropzone' })}
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    height: '100%',
                }}
            >
                <Box mb={{ xs: 3, sm: 5 }} sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}>
                    <input {...getInputProps()} />
                    <Box sx={{ textAlign: 'center', pt: 2, mb: 2 }}>
                        <Import width={150} height={150} fill={colors.teal_700} />
                    </Box>
                    <Typography align="center">Drag and drop files here</Typography>
                </Box>
                <Box sx={{ pb: 2 }}>
                    <Typography align="center" sx={{ color: colors.grey_300 }}>
                        Supported files
                    </Typography>
                    <Typography align="center" sx={{ color: colors.grey_300 }}>
                        5MB max file size DOC, DOCX, PDF
                    </Typography>
                </Box>
            </div>
        </Box>
    )
}