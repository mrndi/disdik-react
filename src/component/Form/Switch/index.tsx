import React, { useState } from 'react'
import SwitchUnstyled from '@mui/base/SwitchUnstyled'
import { styled } from '@mui/system'
import { colors } from '../../../themes'

const RootStyle = styled('span')({
    '&.MuiSwitch-root': {
        fontSize: '0',
        position: 'relative',
        display: 'inline-block',
        width: '40px',
        height: '20px',
        margin: '10px',
        cursor: 'pointer',

        '&.Mui-checked': {
            '.MuiSwitch-thumb': {
                left: '22px',
                top: '3px',
                backgroundColor: 'white',
            },
            '.MuiSwitch-track': {
                background: colors.teal_500,
            },
        },

        '.Mui-disabled': {
            opacity: '0.4',
            cursor: 'not-allowed',
        },

        '.MuiSwitch-track': {
            background: colors.grey_300,
            borderRadius: '10px',
            display: 'block',
            height: '100%',
            width: '100%',
            position: 'absolute',
        },

        '.MuiSwitch-thumb': {
            display: 'block',
            width: '14px',
            height: '14px',
            top: '3px',
            left: '3px',
            borderRadius: '16px',
            backgroundColor: 'white',
            position: 'relative',
            transition: 'all 200ms ease',
        },

        '.MuiSwitch-input': {
            cursor: 'inherit',
            position: 'absolute',
            width: '100%',
            height: '100%',
            top: '0',
            left: '0',
            opacity: '0',
            zIndex: '1',
            margin: '0',
        },
    },
})

interface SwitchProps {
    checked: boolean
    onChange: (checked: boolean) => void
}

export const Switch = ({ checked = true, onChange, ...otherProps }: SwitchProps) => {
    const [isChecked, setChecked] = useState(checked)
    const label = { componentsProps: { input: { 'aria-label': 'Demo switch' } } }

    const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChecked(!isChecked)
        onChange(event.target.checked)
    }

    return (
        <SwitchUnstyled
            checked={isChecked}
            onChange={handleOnChange}
            component={RootStyle}
            {...label}
            {...otherProps}
        />
    )
}