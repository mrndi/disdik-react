import { Popover as PopoverMui } from '@mui/material'


interface PopoverProps {
    style?: any
    isShow: boolean
    onClose: () => void
    anchorEl: any
    content: any
    className?: string
}

export const Popover = ({ style, isShow, onClose, anchorEl, content, className }: PopoverProps) => {
    return (
        <PopoverMui
            open={isShow}
            onClose={onClose}
            anchorEl={anchorEl}
            className={className}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            sx={style}
        >
            {content}
        </PopoverMui>
    )
}

