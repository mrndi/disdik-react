import { Box, InputBase, Stack } from '@mui/material'
import { colors } from '../../../themes'

interface TextInputProps {
    placeholder?: string
    label?: string
    iconLeft?: any
    iconRight?: any
    onClick?: () => void
    onChange?: ()=> void
    name: string
}

export const TextInput = ({ placeholder, label, iconLeft, iconRight,onChange, onClick, name, ...otherProps }: TextInputProps) => {
    return (
        <Stack
            direction="row"
            alignItems="center"
            onClick={onClick}
            onChange= {onChange}
            sx={{
                position: 'relative',
                backgroundColor: colors.lilac_100,
                borderRadius: '4px',
                '.MuiInputBase-root': {
                    paddingTop: '0',
                    paddingBottom: '0',
                    ...(iconLeft && {
                        paddingLeft: '45px',
                        paddingRight: '10px',
                    }),
                    ...(iconRight && {
                        paddingRight: '35px',
                    }),
                },
            }}
        >
            {iconLeft ? (
                <Box
                    sx={{
                        position: 'absolute',
                        zIndex: '3',
                        left: '10px',
                        height: '100%',
                        display: 'flex',
                        alignItems: 'center',
                    }}
                >
                    {iconLeft}
                </Box>
            ) : (
                ''
            )}
            <InputBase
                placeholder={placeholder}
                inputProps={{ 'aria-label': label }}
                name={name}
                sx={{
                    pl: 2,
                    height: 48,
                    width: '100%',
                    '.MuiInputBase-input': {
                        p: 0,
                    },
                }}
                {...otherProps}
            />
            {iconRight ? (
                <Box
                    display="flex"
                    alignItems="center"
                    sx={{
                        position: 'absolute',
                        zIndex: '3',
                        right: '8px',
                    }}
                >
                    {iconRight}
                </Box>
            ) : (
                ''
            )}
        </Stack>
    )
}