import { useState, useEffect } from 'react'
import { Box, LinearProgress, Stack, Typography } from '@mui/material'
import { colors } from '../../../themes'
import { File, Minus } from '../../../assets/icons'


const defaultProgress = 0
const stepIncrement = 10
const maxProgress = 100
const interval = 500


interface ProgressProps {
    onDelete: () => void
}


export const Progress = ({ onDelete }: ProgressProps) => {
    const [progress, setProgress] = useState(defaultProgress)

    useEffect(() => {
        const timer = setInterval(() => {
            setProgress((oldProgress) => {
                const diff = Math.random() * stepIncrement
                return Math.min(oldProgress + diff, maxProgress)
            })
        }, interval)

        return () => {
            clearInterval(timer)
        }
    }, [])

    return (
        <Stack direction="row" alignItems="center">
            <File width={28} height={28} fill={colors.grey_500} />
            <Box sx={{ ml: 1, width: '100%' }}>
                <Stack direction="row" alignItems="center" justifyContent="space-between">
                    <Typography>staff-management-2022.csv</Typography>
                    <Box
                        onClick={onDelete}
                        sx={{
                            cursor: 'pointer',
                        }}
                    >
                        <Minus width={16} height={16} fill={colors.froly_500} />
                    </Box>
                </Stack>
                <LinearProgress variant="determinate" value={progress} />
                <Typography variant="body2" sx={{ color: colors.grey_300 }}>
                    2.12 MB / 3,54MB
                </Typography>
            </Box>
        </Stack>
    )
}
