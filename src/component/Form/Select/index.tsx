import { useState, useEffect } from 'react'
import { Box, FormControl, Stack, Select, MenuItem, Typography, SelectChangeEvent } from '@mui/material'
import { colors } from '../../../themes'


interface FormSelectProps {
    style?: any
    label?: string
    defaultValue?: string
    children?: any
    iconLeft?: any
    iconRight?: any
    backgroundColor?: string
    onChange?: (value: string) => void
    value?: string | any
    otherProp?: any
}

export const FormSelect = ({
    style,
    label,
    defaultValue,
    children,
    iconLeft,
    iconRight,
    backgroundColor,
    onChange,
    value,
    ...otherProp
}: FormSelectProps) => {
    const [selectValue, setSelectValue] = useState(value || '')

    const handleChange = (event: SelectChangeEvent) => {
        setSelectValue(event.target.value)
        if (onChange) {
            onChange(event.target.value)
        }
    }

    useEffect(() => {
        setSelectValue(value || '')
    }, [value])

    return (
        <FormControl sx={style}>
            <Stack
                direction="row"
                alignItems="center"
                sx={{
                    width: '100%',
                    position: 'relative',
                    '.MuiOutlinedInput-root': {
                        ...(!backgroundColor && {
                            backgroundColor: colors.lilac_100,
                        }),
                        ...(backgroundColor && {
                            backgroundColor: backgroundColor,
                        }),
                        color: colors.grey_900,
                        minHeight: '48px',
                        fontSize: '16px',
                    },
                    '.MuiSelect-select': {
                        paddingTop: '0',
                        paddingBottom: '0',
                        ...(iconLeft && {
                            paddingLeft: '50px',
                        }),
                    },
                    '.MuiSelect-icon': {
                        display: 'none',
                    },
                }}
            >
                {iconLeft ? (
                    <Box
                        sx={{
                            position: 'absolute',
                            zIndex: '3',
                            top: '0',
                            left: '0',
                            height: '100%',
                            paddingLeft: '15px',
                            display: 'flex',
                            alignItems: 'center',
                        }}
                    >
                        {iconLeft}
                    </Box>
                ) : (
                    ''
                )}
                <Select
                    onChange={handleChange}
                    displayEmpty
                    inputProps={{ 'aria-label': label }}
                    sx={{ height: 44, border: '0', width: '100%' }}
                    defaultValue={defaultValue}
                    value={selectValue}
                    {...otherProp}
                >
                    {defaultValue && (
                        <MenuItem value="" disabled>
                            <Typography>{defaultValue}</Typography>
                        </MenuItem>
                    )}
                    {children}
                </Select>
            </Stack>
            {iconRight ? (
                <Box
                    sx={{
                        position: 'absolute',
                        zIndex: '3',
                        top: '12px',
                        right: '15px',
                    }}
                >
                    {iconRight}
                </Box>
            ) : (
                ''
            )}
        </FormControl>
    )
}
