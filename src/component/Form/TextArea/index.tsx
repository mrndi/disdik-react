import React from 'react'
import { Box, TextareaAutosize } from '@mui/material'
import { colors } from '../../../themes'

interface TextAreaProps {
    placeholder?: string
    row?: number
    onChange?: (event: React.ChangeEvent<HTMLTextAreaElement>) => void
}

export const TextArea = ({ placeholder, row, onChange }: TextAreaProps) => {
    return (
        <Box
            sx={{
                textarea: {
                    backgroundColor: colors.lilac_100,
                    padding: '12px 14px',
                    color: colors.grey_900,
                    minHeight: '48px',
                    fontSize: '16px',
                    border: '0',
                    borderRadius: '4px',
                    '&:focus': {
                        boxShadow: 'unset',
                        border: 'unset',
                        outline: 'unset',
                    },
                },
            }}
        >
            <TextareaAutosize
                aria-label="minimum height"
                minRows={row}
                placeholder={placeholder}
                style={{ width: '100%' }}
                onChange={onChange}
            />
        </Box>
    )
}
