import React, { useState } from 'react'
import { Box, InputBase, Stack, Chip } from '@mui/material'
import PropTypes from 'prop-types'
import { colors } from '../../../themes'

interface ItemInputProps {
    placeholder?: string
    label?: string
    iconLeft?: any
    iconRight?: any
    onItemsChange: (items: string[]) => void
}

export const ItemInput = ({ placeholder, label, iconLeft, iconRight, onItemsChange, ...otherProps }: ItemInputProps) => {
    const [inputValue, setInputValue] = useState('')
    const [itemsValue, setItemsValue] = useState([]) as any
    const handleKeyPress = (event: React.KeyboardEvent<HTMLDivElement>) => {
        if (event.key === 'Enter') {
            const newItems = [...itemsValue, inputValue]
            setItemsValue(newItems)
            setInputValue('')
            onItemsChange(newItems)
        }
    }
    const handleItemDelete = (deletedValue: any) => {
        const filteredItems = itemsValue.filter((value: any) => value !== deletedValue)
        setItemsValue(filteredItems)
        onItemsChange(filteredItems)
    }
    return (
        <>
            <Stack
                direction="row"
                alignItems="center"
                sx={{
                    position: 'relative',
                    backgroundColor: colors.lilac_100,
                    borderRadius: '4px',
                    '.MuiInputBase-root': {
                        paddingTop: '0',
                        paddingBottom: '0',
                        ...(iconLeft && {
                            paddingLeft: '45px',
                            paddingRight: '10px',
                        }),
                        ...(iconRight && {
                            paddingRight: '35px',
                        }),
                    },
                }}
            >
                {iconLeft ? (
                    <Box
                        sx={{
                            position: 'absolute',
                            zIndex: '3',
                            left: '10px',
                            height: '100%',
                            display: 'flex',
                            alignItems: 'center',
                        }}
                    >
                        {iconLeft}
                    </Box>
                ) : (
                    ''
                )}
                <InputBase
                    value={inputValue}
                    placeholder={placeholder}
                    inputProps={{ 'aria-label': label }}
                    sx={{
                        pl: 2,
                        height: 48,
                        width: '100%',
                        '.MuiInputBase-input': {
                            p: 0,
                        },
                    }}
                    onChange={(e) => setInputValue(e.target.value)}
                    onKeyPress={handleKeyPress}
                    {...otherProps}
                />
                {iconRight ? (
                    <Box
                        display="flex"
                        alignItems="center"
                        sx={{
                            position: 'absolute',
                            zIndex: '3',
                            right: '8px',
                        }}
                    >
                        {iconRight}
                    </Box>
                ) : (
                    ''
                )}
            </Stack>
            <Box sx={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', padding: '10px' }}>
                {itemsValue.map((item: any, itemIndex: any) => {
                    return (
                        <Chip
                            key={itemIndex}
                            label={item}
                            onDelete={() => handleItemDelete(item)}
                            color="primary"
                            variant="outlined"
                        />
                    )
                })}
            </Box>
        </>
    )
}

ItemInput.propTypes = {
    placeholder: PropTypes.string,
    label: PropTypes.string,
    iconLeft: PropTypes.element,
    iconRight: PropTypes.element,
    onItemsChange: PropTypes.func,
}
