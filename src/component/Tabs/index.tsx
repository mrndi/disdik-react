import { styled } from '@mui/system'
import { TabsListUnstyled, TabPanelUnstyled } from '@mui/base'
import TabUnstyled from '@mui/base/TabUnstyled'
import { colors, mainTheme } from '../../themes'

export const Tab = styled(TabUnstyled)({
    backgroundColor: 'unset',
    color: colors.grey_900,
    cursor: 'pointer',
    padding: '10px 20px',
    border: 'none',
    display: 'flex',
    justifyContent: 'center',
    transition: 'all 0.2s ease-in-out',
    borderRadius: '5px',
    marginRight: '5px',

    [mainTheme.breakpoints.down('sm')]: {
        padding: '10px',
        borderRadius: '15px',
        marginRight: '0',
    },

    '.MuiTypography-body1': {
        fontSize: '16px',
        color: colors.grey_900,
        [mainTheme.breakpoints.down('sm')]: {
            fontSize: '15px',
        },
    },

    '&.Mui-selected': {
        backgroundColor: colors.teal_500,
    },

    '&.Mui-selected .MuiTypography-body1': {
        color: 'white',
    },

    '&:last-child': {
        marginRight: 0,
    },

    '&:hover': {
        color: 'white',
        backgroundColor: colors.teal_500,
    },

    '&:hover .MuiTypography-body1': {
        color: 'white',
    },
})


export const TabPanel = styled(TabPanelUnstyled)({
    width: '100%',
})


export const TabsList = styled(TabsListUnstyled)({
    backgroundColor: colors.lilac_100,
    padding: '8px',
    borderRadius: '10px',
    fontSize: '16px',
    display: 'flex',
    alignContent: 'space-between',
    justifyContent: 'space-between',

    [mainTheme.breakpoints.down('sm')]: {
        padding: '5px',
    },

    '&.tab-2-items': {
        [mainTheme.breakpoints.down('sm')]: {
            'button.TabUnstyled-root': {
                width: '100%',
            },
        },
    },
})

