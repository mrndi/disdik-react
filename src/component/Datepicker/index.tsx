import { Box, TextField } from '@mui/material'
import { DatePicker } from '@mui/x-date-pickers';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { colors } from '../../themes'


interface DatepickerProps {
    iconLeft?: any
    iconRight?: any
    onChange: (value: any) => void
    value: any
}

export const Datepicker = ({ iconLeft, iconRight, onChange, value }: DatepickerProps) => {
    return (
        <Box sx={{ position: 'relative' }}>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
                {iconLeft ? (
                    <Box
                        sx={{
                            position: 'absolute',
                            zIndex: '3',
                            top: '0',
                            left: '0',
                            height: '100%',
                            paddingLeft: '15px',
                            display: 'flex',
                            alignItems: 'center',
                        }}
                    >
                        {iconLeft}
                    </Box>
                ) : (
                    ''
                )}
                <DatePicker
                    label="Select"
                    value={value}
                    onChange={onChange}
                    renderInput={(params: any) => (
                        <TextField
                            {...params}
                            sx={{
                                width: '100%',
                                backgroundColor: colors.lilac_100,
                                borderRadius: '4px',
                                padding: 0,
                                fontSize: '16px',
                                maxHeight: '48px',
                                label: {
                                    fontSize: '16px',
                                },
                                '.MuiOutlinedInput-input': {
                                    color: colors.grey_900,
                                    fontSize: '16px',
                                    paddingLeft: '50px',
                                    paddingTop: '0',
                                    paddingBottom: '0',
                                    paddingRight: '25px',
                                    height: '48px',
                                    '::-webkit-input-placeholder': {
                                        visibility: 'hidden',
                                    },
                                    ':-ms-input-placeholder': {
                                        visibility: 'hidden',
                                    },
                                    '::placeholder': {
                                        visibility: 'hidden',
                                    },
                                },
                                'label[data-shrink=false]+.MuiInputBase-formControl .css-nxo287-MuiInputBase-input-MuiOutlinedInput-input':
                                {
                                    '::-webkit-input-placeholder': {
                                        color: colors.grey_500,
                                        opacity: '1',
                                    },
                                    ':-ms-input-placeholder': {
                                        color: colors.grey_500,
                                        opacity: '1',
                                    },
                                    '::placeholder': {
                                        color: colors.grey_500,
                                        opacity: '1',
                                    },
                                },
                                '.MuiInputLabel-root': {
                                    color: colors.grey_500,
                                    left: '35px',
                                    top: '-3px',
                                },
                                '.MuiInputLabel-shrink': {
                                    opacity: '0',
                                },
                                '.MuiInputAdornment-root': {
                                    width: '100%',
                                    height: '100%',
                                    maxHeight: '48px',
                                    position: 'absolute',
                                    top: 0,
                                    left: 0,
                                    marginLeft: 0,
                                },
                                '.MuiButtonBase-root': {
                                    width: '100%',
                                    height: '100%',
                                    position: 'absolute',
                                    top: 0,
                                    left: 0,
                                    '&:hover': {
                                        backgroundColor: 'unset',
                                    },
                                    '.MuiSvgIcon-root': {
                                        display: 'none',
                                    },
                                    '.MuiTouchRipple-root': {
                                        display: 'none',
                                    },
                                },
                            }}
                        />
                    )}
                />
                {iconRight ? (
                    <Box
                        sx={{
                            position: 'absolute',
                            zIndex: '3',
                            top: '12px',
                            right: '15px',
                        }}
                    >
                        {iconRight}
                    </Box>
                ) : (
                    ''
                )}
            </LocalizationProvider>
        </Box>
    )
}

export default Datepicker
