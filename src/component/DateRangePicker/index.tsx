import React, { useState } from 'react'
import { Button, Box, Paper, Stack, TextField, Typography, Backdrop, Slide, Modal } from '@mui/material'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
// import { StaticDateRangePicker } from '@mui/lab/StaticDateRangePicker'
import { TextInput } from '../Form'
import { Arrow, Date } from '../../assets/icons'
import { colors } from '../../themes'
import { useMediaQuery } from '@mui/material'

export const DateRangePicker = (props: any) => {

    const isMobile = useMediaQuery((theme: any) => theme.breakpoints.down('sm'))
    const [value, setValue] = useState([null, null])
    const [open, setOpen] = useState(false)
    const [selected, setSelected] = useState('Today')
    const [isCustom, setIsCustom] = useState(false)
    // eslint-disable-next-line no-unused-vars
    const [option, setOption] = useState([
        {
            name: 'Today',
            action: () => {
                setSelected('Today')
            },
        },
        {
            name: 'Yesterday',
            action: () => {
                setSelected('Yesterday')
            },
        },
        {
            name: 'Last 30 Days',
            action: () => {
                setSelected('Last 30 Days')
            },
        },
        {
            name: 'This Month',
            action: () => {
                setSelected('This Month')
            },
        },
        {
            name: 'Last Month',
            action: () => {
                setSelected('Last Month')
            },
        },
        {
            name: 'Custom Range',
            action: () => {
                setSelected('Custom Range')
                if (isMobile) {
                    setIsCustom(!isCustom)
                }
            },
        },
    ])

    return (
        <Box
            sx={{
                position: 'relative',
                '.MuiTypography-root.MuiTypography-body1.css-1v14c13-MuiTypography-root': {
                    color: 'white',
                    backgroundColor: colors.teal_500,
                },
            }}
        >
            <TextInput
                placeholder={selected}
                onClick={() => setOpen(!open)}
                iconLeft={<Date width={28} height={22} fill={colors.teal_500} />}
                iconRight={<Arrow width={20} height={20} fill={colors.teal_500} direction='right' />}
                name="daterange"
            />
            {open ? (
                <Box>
                    <Paper
                        elevation={3}
                        sx={{
                            position: 'absolute',
                            right: 0,
                            zIndex: 1250,
                        }}
                        className="md-hide"
                    >
                        <Stack direction="row">
                            <LocalizationProvider dateAdapter={AdapterDateFns}>
                                {/* <StaticDateRangePicker
                                    className="daterangepicker-custom"
                                    displayStaticWrapperAs="desktop"
                                    value={value}
                                    onChange={(newValue: any) => {
                                        setValue(newValue)
                                    }}
                                    renderInput={(startProps: any, endProps: any) => (
                                        <>
                                            <TextField {...startProps} />
                                            <Box sx={{ mx: 2 }}> to </Box>
                                            <TextField {...endProps} />
                                        </>
                                    )}
                                /> */}
                            </LocalizationProvider>
                            <Box
                                sx={{
                                    width: '200px',
                                    borderLeft: '2px solid rgba(0, 0, 0, 0.12)',
                                    pt: 2,
                                }}
                            >
                                {option.map((data, idx) => (
                                    <Typography
                                        key={idx}
                                        sx={{
                                            py: 1,
                                            pl: 2,
                                            cursor: 'pointer',
                                            '&:hover': {
                                                color: 'white',
                                                backgroundColor: colors.teal_500,
                                            },
                                            color: selected === data.name ? 'white' : colors.grey_900,
                                            backgroundColor: selected === data.name ? colors.teal_500 : 'white',
                                        }}
                                        onClick={data.action}
                                    >
                                        {data.name}
                                    </Typography>
                                ))}
                            </Box>
                        </Stack>
                        <Stack
                            direction="row"
                            justifyContent="space-between"
                            alignItems="center"
                            sx={{ p: 2, borderTop: '2px solid rgba(0, 0, 0, 0.12)' }}
                            className="md-hide"
                        >
                            <Stack direction="row" alignItems="center">
                                <Date width={100} height={90} fill={colors.teal_500} />
                                <Typography sx={{ ml: 1 }}>22 Nov 2021 - 09 Dec 2021</Typography>
                            </Stack>
                            <Stack direction="row" spacing={1}>
                                <Button
                                    variant="outlined"
                                    color="primary"
                                    onClick={() => setOpen(!open)}
                                    sx={{
                                        minWidth: '130px',
                                        minHeight: '50px',
                                    }}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={() => setOpen(!open)}
                                    sx={{
                                        minWidth: '130px',
                                        minHeight: '50px',
                                    }}
                                >
                                    Apply
                                </Button>
                            </Stack>
                        </Stack>
                    </Paper>
                    <Paper className="md-block">
                        <Box>
                            <Box>
                                {option.map((data, idx) => (
                                    <Typography
                                        key={idx}
                                        sx={{
                                            py: 1,
                                            pl: 2,
                                            cursor: 'pointer',
                                            '&:hover': {
                                                color: 'white',
                                                backgroundColor: colors.teal_500,
                                            },
                                            color: selected === data.name ? 'white' : colors.grey_900,
                                            backgroundColor: selected === data.name ? colors.teal_500 : 'white',
                                        }}
                                        onClick={data.action}
                                    >
                                        {data.name}
                                    </Typography>
                                ))}
                            </Box>
                            <Modal
                                className="daterangepicker-custom"
                                aria-labelledby={props.title}
                                aria-describedby="transition-modal-description"
                                open={isCustom}
                                onClose={() => setIsCustom(!isCustom)}
                                closeAfterTransition
                                BackdropComponent={Backdrop}
                                BackdropProps={{
                                    timeout: 500,
                                }}
                                sx={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    margin: 1,
                                }}
                            >
                                <Slide in={isCustom}>
                                    <Box
                                        style={{
                                            backgroundColor: 'white',
                                            padding: '16px',
                                            borderRadius: '4px',
                                            zIndex: 10,
                                        }}
                                    >
                                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                                            {/* <StaticDateRangePicker
                                                displayStaticWrapperAs="mobile"
                                                value={value}
                                                onChange={(newValue: any) => {
                                                    setValue(newValue)
                                                }}
                                                showToolbar={false}
                                                renderInput={(startProps: any, endProps: any) => (
                                                    <React.Fragment>
                                                        <TextField {...startProps} />
                                                        <Box sx={{ mx: 2 }}> to </Box>
                                                        <TextField {...endProps} />
                                                    </React.Fragment>
                                                )}
                                            /> */}
                                        </LocalizationProvider>
                                        <Stack
                                            justifyContent="space-between"
                                            alignItems="center"
                                            sx={{
                                                borderTop: `2px solid ${colors.grey_50}`,
                                            }}
                                        >
                                            <Stack
                                                direction="row"
                                                alignItems="center"
                                                sx={{
                                                    width: '100%',
                                                    py: 2,
                                                    borderBottom: `2px solid ${colors.grey_50}`,
                                                }}
                                            >
                                                <Date width={24} height={24} fill={colors.teal_500} />
                                                <Typography sx={{ ml: 1 }}>22 Nov 2021 - 09 Dec 2021</Typography>
                                            </Stack>
                                            <Stack
                                                direction="row"
                                                spacing={1}
                                                sx={{ pt: 2, width: '100%' }}
                                                justifyContent="flex-end"
                                            >
                                                <Button
                                                    variant="outlined"
                                                    color="primary"
                                                    onClick={() => {
                                                        setIsCustom(!isCustom)
                                                        setOpen(!open)
                                                    }}
                                                >
                                                    Cancel
                                                </Button>
                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    onClick={() => {
                                                        setIsCustom(!isCustom)
                                                        setOpen(!open)
                                                    }}
                                                >
                                                    Apply
                                                </Button>
                                            </Stack>
                                        </Stack>
                                    </Box>
                                </Slide>
                            </Modal>
                        </Box>
                    </Paper>
                </Box>
            ) : (
                ''
            )}
        </Box>
    )
}

export default DateRangePicker
