
import { Alert, Box, Typography } from '@mui/material'
import { colors } from '../../themes'


interface IconAlertProps {
    icon: React.ReactNode
    text: string
    action: React.ReactNode
}

export const IconAlert = ({ icon, text, action }: IconAlertProps) => {
    return (
        <Alert
            icon={false}
            severity="error"
            sx={{
                mb: 3,
                backgroundColor: colors.teal_100,
            }}
        >
            <Box display="flex" flexDirection="row" justifyContent="space-between" alignItems="center" flexWrap="wrap" minWidth='100%'>
                <Box flex={1} display="flex" flexDirection="row" alignItems="center">
                    <Box width={18} pt="3px">
                        {icon}
                    </Box>
                    <Typography sx={{ px: 2 }}>{text}</Typography>
                </Box>
                <Box width={{ xs: '100%', sm: 'auto' }}>{action}</Box>
            </Box>
        </Alert>
    )
}