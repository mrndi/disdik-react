import { Link } from "@tanstack/react-location"
import { userSignOut } from "../helper/AuthHelper"



export const Navbar = () => {
    return (
        <div>
            <div className="navbar bg-base-100 justify-between">
                <div className="navbar-start">
                    <div className="dropdown">
                        <label tabIndex={0} className="btn btn-ghost lg:hidden">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h8m-8 6h16" /></svg>
                        </label>
                        <ul tabIndex={0} className="menu menu-compact dropdown-content mt-3 p-2 shadow bg-base-100 rounded-box w-56">
                            <li >
                                <span>
                                    Surat Masuk
                                    <svg className="fill-current" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" /></svg>
                                </span>
                                <ul className="rounded-box p-2 bg-base-100">
                                    <li>
                                        <span>
                                            <Link to="/suratMasuk">Surat Masuk</Link>
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <Link to="/suratMasuk/upload">Upload Surat</Link>
                                        </span>
                                    </li>
                                </ul>
                            </li>
                            <li >
                                <span>
                                    <Link to="/suratKeluar">Surat Keluar</Link>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <Link to="dashboard" className="btn btn-ghost normal-case text-xl">Disdik-Document</Link>
                </div>
                <div className="navbar-center hidden lg:flex">
                    <ul className="menu menu-horizontal px-1">

                        <li tabIndex={0} className="dropdown">
                            <a>
                                Surat Masuk
                                <svg className="fill-current" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" /></svg>
                            </a>
                            <ul className="p-2 shadow bg-base-100 dropdown-content mt-3 rounded-box w-56 ">
                                <li className="hover-bordered">
                                    <span>
                                        <Link to="/suratMasuk">Surat Masuk</Link>
                                    </span>
                                </li>
                                <li className="hover-bordered">
                                    <span>
                                        <Link to="/suratMasuk/diterima">Upload Surat</Link>
                                    </span>
                                </li>
                                <li className="hover-bordered">
                                    <span>
                                        <Link to="/suratMasuk/upload">Upload Surat</Link>
                                    </span>
                                </li>
                            </ul>
                        </li>
                        <li><a>Surat Keluar</a></li>
                    </ul>
                </div>
                <div className="dropdown dropdown-end self-end">
                    <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
                        <div className="w-10 rounded-full">
                            <img src="images/avatar.jpg" />
                        </div>
                    </label>
                    <ul tabIndex={0} className="menu menu-compact dropdown-content mt-3 p-2 shadow bg-base-100 rounded-box w-52">
                        <li>
                            <a className="justify-between">
                                Profile
                                <span className="badge">New</span>
                            </a>
                        </li>
                        <li><a>Settings</a></li>
                        <li><a onClick={() => userSignOut()}>Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}