import { useState } from 'react'
import { Box, MenuItem, Typography } from '@mui/material'
import { Pagination as PaginationMui } from '@mui/material'
import { FormSelect } from '../Form'
import { Arrow } from '../../assets/icons'
import { colors } from '../../themes'


const defaultSize = 10
const defaultTotalItems = 0
const defaultCurrentPage = 1
const defaultTotalPages = 1
const defaultPage = 1
const subtractPage = 1


interface PaginationProps {
    size?: number
    onSizeChange?: (val: number) => void
    totalItems?: number
    currentPage?: number
    totalPages?: number
    onPageChange?: (val: number) => void
}

export const Pagination = ({
    size = defaultSize,
    onSizeChange,
    totalItems = defaultTotalItems,
    currentPage = defaultCurrentPage,
    totalPages = defaultTotalPages,
    onPageChange,
}: PaginationProps) => {
    const [page, setPage] = useState(defaultPage)
    const [pageSize, setPageSize] = useState(size)
    const startItemIndex = (currentPage - subtractPage) * pageSize + defaultPage
    const maxItemSize = pageSize * currentPage
    const startIndexOrTotal = startItemIndex > totalItems ? startItemIndex : totalItems
    const totalOrMax = maxItemSize > totalItems ? totalItems : maxItemSize
    const endItemIndex = pageSize > totalItems ? startIndexOrTotal : totalOrMax
    const handleSizeChange = (value: any) => {
        setPageSize(value)
        if (onSizeChange) {
            onSizeChange(value)
        }
    }
    const handlePageChange = (value: any) => {
        setPage(value)
        if (onPageChange) {
            onPageChange(value)
        }
    }
    return (
        <Box
            sx={{
                mt: 3,
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
            }}
        >
            <Box
                sx={{
                    display: 'flex',
                    alignItems: 'center',
                }}
            >
                <Typography sx={{ mr: 3 }}>Row per page</Typography>
                <FormSelect
                    value={pageSize}
                    iconRight={<Arrow width={20} height={20} fill={colors.teal_500} direction='down' />}
                    style={{
                        minWidth: '80px',
                        '.MuiSelect-select': {
                            color: colors.grey_900,
                        },
                    }}
                    onChange={handleSizeChange}
                >
                    <MenuItem value={10}>10</MenuItem>
                    <MenuItem value={20}>20</MenuItem>
                    <MenuItem value={30}>30</MenuItem>
                </FormSelect>
            </Box>
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography sx={{ mr: 3 }}>
                    Show {startItemIndex}-{endItemIndex} of {totalItems}
                </Typography>
                <PaginationMui
                    count={totalPages}
                    shape="rounded"
                    page={page}
                    onChange={handlePageChange}
                    sx={{
                        li: {
                            '.MuiButtonBase-root': {
                                color: colors.grey_900,
                                padding: '10px 15px',
                                height: 'auto',

                                '&.Mui-selected': {
                                    backgroundColor: colors.teal_500,
                                    color: 'white',
                                },
                            },
                            '&:hover': {
                                '.MuiButtonBase-root': {
                                    backgroundColor: colors.teal_500,
                                    color: 'white',
                                },
                            },
                        },
                    }}
                />
            </Box>
        </Box>
    )
}

