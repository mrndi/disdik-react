export const dataNotification = [
    {
        id: 1,
        title: 'Letraset sheets containing',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        time: '5 minutes ago',
    },
    {
        id: 2,
        title: 'Various versions have evolved over the years',
        description:
            'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout',
        time: '30 minutes ago',
    },
    {
        id: 3,
        title: 'Lorem Ipsum comes from sections',
        description:
            'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form',
        time: '1 hour ago',
    },
]
