function createData_Checkin(staff_name, status, date, checkin, notes) {
    return {
        staff_name,
        status,
        date,
        checkin,
        notes,
    }
}
export const dataCheckin = [
    createData_Checkin(
        'Bagus Herdiawan',
        'Reserved',
        'Thu, 04 Dec 2021',
        'yes',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat provident consequuntur, rem fuga autem culpa cumque.',
    ),
    createData_Checkin(
        'Andi Setiawan',
        'Invited',
        'Mon, 12 Nov 2021',
        'yes',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat provident consequuntur, rem fuga autem culpa cumque.',
    ),
    createData_Checkin('Fandy Barestu', 'Visitor', 'Fri, 24 Now 2021', 'yes', 'Laoreet nec enim neque.'),
    createData_Checkin('Bagus Herdiawan', 'Invited', 'Fri, 19 Oct 2021', 'yes', 'Laoreet nec enim neque.'),
    createData_Checkin('Andi Setiawan', 'Visitor', 'Tue, 22 Oct 2021', 'no', 'Laoreet nec enim neque.'),
    createData_Checkin('Fandy Barestu', 'Reserved', 'Wed, 09 Sept 2021', 'no', 'Laoreet nec enim neque.'),
    createData_Checkin('Eko Prasetyo', 'Visitor', 'Sat, 07 Sept 2021', 'no', 'Laoreet nec enim neque.'),
    createData_Checkin('Yanuwar Rismoyo', 'Invited', 'Thu, 31 Sept 2021', 'no', 'Laoreet nec enim neque.'),
    createData_Checkin('Bagus Herdiawan', 'Invited', 'Fri, 19 Oct 2021', 'yes', 'Laoreet nec enim neque.'),
    createData_Checkin('Andi Setiawan', 'Visitor', 'Tue, 22 Oct 2021', 'no', 'Laoreet nec enim neque.'),
]
