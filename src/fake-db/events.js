let eventGuid = 0
let todayStr = new Date().toISOString().replace(/T.*$/, '') // YYYY-MM-DD of today

export const dataEvents = [
    {
        id: createEventId(),
        title: 'Bagus Herdiawan',
        desk: 'Desk 42',
        floor: '2nd Floor',
        start: todayStr,
    },
    {
        id: createEventId(),
        title: 'Adryn Kusnata',
        desk: 'Desk 08',
        floor: '1st Floor',
        start: todayStr,
    },
    {
        id: createEventId(),
        title: 'Andi Setiawan',
        desk: 'Desk 10',
        floor: '3rd Floor',
        start: todayStr,
    },
    {
        id: createEventId(),
        title: 'Ganis Atmawarin',
        desk: 'Desk 15',
        floor: '2nd Floor',
        start: todayStr,
    },
    {
        id: createEventId(),
        title: 'Fandy Barestu',
        desk: 'Desk 05',
        floor: '1st Floor',
        start: todayStr,
    },
]

export function createEventId() {
    return String(eventGuid++)
}
