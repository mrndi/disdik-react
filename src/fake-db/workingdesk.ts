function createData_WorkingDesk(id: string, staff_name: string, desk: string, floor: string, date: string, repeat_type: string, checkin: string, notes: string, repeat_notes: string, status: string) {
    return {
        id,
        staff_name,
        desk,
        floor,
        date,
        repeat_type,
        checkin,
        notes,
        repeat_notes,
        status,
    }
}
export const dataWorkingDesk = [
    createData_WorkingDesk(
        '1',
        'Bagus Herdiawan',
        'Desk 42',
        '2nd Floor',
        'Thu, 04 Mar 2022',
        'Weekly',
        'no',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat provident consequuntur, rem fuga autem culpa cumque.',
        'Begin on Thursday, 04 Nov 2021. End by Thursday, 25 November 2021.',
        'active',
    ),
    createData_WorkingDesk(
        '2',
        'Andi Setiawan',
        'Desk 42',
        '2nd Floor',
        'Mon, 26 Feb 2022',
        'Daily',
        'no',
        'Laoreet nec enim neque.',
        'Begin on Thursday, 11 Oct 2021. End by Sat, 25 October 2021.',
        'active',
    ),
    createData_WorkingDesk(
        '3',
        'Fandy Barestu',
        'Desk 42',
        '2nd Floor',
        'Fri, 24 Now 2021',
        'Montly',
        'yes',
        'Laoreet nec enim neque.',
        'Begin on Monday, 01 Sep 2021. End by Wed, 25 September 2021.',
        'inactive',
    ),
    createData_WorkingDesk(
        '4',
        'Bagus Herdiawan',
        'Desk 42',
        '2nd Floor',
        'Fri, 19 Oct 2021',
        'None',
        'yes',
        'Laoreet nec enim neque.',
        '',
        'inactive',
    ),
    createData_WorkingDesk(
        '5',
        'Andi Setiawan',
        'Desk 42',
        '2nd Floor',
        'Tue, 22 Oct 2021',
        'Weekly',
        'yes',
        'Laoreet nec enim neque.',
        'Begin on Thursday, 04 Nov 2021. End by Thursday, 25 November 2021.',
        'inactive',
    ),
    createData_WorkingDesk(
        '6',
        'Fandy Barestu',
        'Desk 42',
        '2nd Floor',
        'Wed, 09 Sept 2021',
        'Weekly',
        'no',
        'Laoreet nec enim neque.',
        'Begin on Friday, 28 Nov 2021. End by Monday, 31 November 2021.',
        'inactive',
    ),
    createData_WorkingDesk(
        '7',
        'Eko Prasetyo',
        'Desk 42',
        '2nd Floor',
        'Sat, 07 Sept 2021',
        'Daily',
        'yes',
        'Laoreet nec enim neque.',
        'Begin on Thursday, 11 Oct 2021. End by Sat, 25 October 2021.',
        'inactive',
    ),
    createData_WorkingDesk(
        '8',
        'Yanuwar Rismoyo',
        'Desk 42',
        '2nd Floor',
        'Thu, 31 Sept 2021',
        'Weekly',
        'no',
        'Laoreet nec enim neque.',
        'Begin on Thursday, 04 Nov 2021. End by Thursday, 25 November 2021.',
        'inactive',
    ),
    createData_WorkingDesk(
        '5',
        'Andi Setiawan',
        'Desk 42',
        '2nd Floor',
        'Tue, 22 Oct 2021',
        'Weekly',
        'yes',
        'Laoreet nec enim neque.',
        'Begin on Thursday, 04 Nov 2021. End by Thursday, 25 November 2021.',
        'inactive',
    ),
    createData_WorkingDesk(
        '6',
        'Fandy Barestu',
        'Desk 42',
        '2nd Floor',
        'Wed, 09 Sept 2021',
        'Weekly',
        'no',
        'Laoreet nec enim neque.',
        'Begin on Friday, 28 Nov 2021. End by Monday, 31 November 2021.',
        'inactive',
    ),
]
