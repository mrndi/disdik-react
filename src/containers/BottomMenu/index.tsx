import { Box, Typography, Stack } from '@mui/material'
import {
    Home as HomeIcon,
    List as ListIcon,
    Plus as PlusIcon,
    Report as ReportIcon,
    Users as UsersIcon,
} from '../../assets/icons'
import { colors } from '../../themes'
import { Link, useLocation } from '@tanstack/react-location'

const BottomMenu = () => {
    const location = useLocation().current
    const isAdmin = true
    const adminLeftMenu = [
        {
            name: 'Home',
            path: '/dashboard',
            icon: <HomeIcon width={20} height={20} fill={colors.grey_300} />,
            iconactive: <HomeIcon width={20} height={20} fill={colors.teal_500} />,
            isVisible: true,
        },
        {
            name: 'Surat',
            path: '/surat',
            icon: <ListIcon width={20} height={20} fill={colors.grey_300} />,
            iconactive: <ListIcon width={20} height={20} fill={colors.teal_500} />,
            isVisible: true,
        },
    ]
    const staffLeftMenu = [
        {
            name: 'Home',
            path: '/dashboard',
            icon: <HomeIcon width={20} height={20} fill={colors.grey_300} />,
            iconactive: <HomeIcon width={20} height={20} fill={colors.teal_500} />,
            isVisible: true,
        },
    ]
    const leftMenu = isAdmin ? adminLeftMenu : staffLeftMenu

    const adminRightMenu = [
        {
            name: 'Users',
            path: '/manage-user',
            icon: <UsersIcon width={20} height={20} fill={colors.grey_300} />,
            iconactive: <UsersIcon width={20} height={20} fill={colors.teal_500} />,
            isVisible: true,
        },
        {
            name: 'Reports',
            path: '/booking-report',
            icon: <ReportIcon width={20} height={20} fill={colors.grey_300} />,
            iconactive: <ReportIcon width={20} height={20} fill={colors.teal_500} />,
            isVisible: true,
        },
    ]
    const staffRightMenu = [
        {
            name: 'Bookings',
            path: '/booking-list',
            icon: <ListIcon width={20} height={20} fill={colors.grey_300} />,
            iconactive: <ListIcon width={20} height={20} fill={colors.teal_500} />,
            isVisible: true,
        },
    ]
    const rightMenu = isAdmin ? adminRightMenu : staffRightMenu

    return (
        <Box
            sx={{
                position: 'fixed',
                bottom: 0,
                zIndex: 10,
                backgroundColor: 'white',
                width: '100%',
                boxShadow: '0px -4px 17px rgba(13, 3, 120, 0.06)',
                display: { xs: 'block', sm: 'none' },
            }}
        >
            <Stack
                direction="row"
                justifyContent="center"
                sx={{ height: '70px', position: 'relative' }}
                alignItems="center"
            >
                <Box
                    sx={{
                        textAlign: 'center',
                        position: 'absolute',
                        top: -15,
                    }}
                >
                    <Link to="/surat/upload">
                        <Box
                            sx={{
                                width: '60px',
                                height: '60px',
                                backgroundColor: 'white',
                                borderRadius: '50%',
                                position: 'relative',
                            }}
                        >
                            <Box
                                display="flex"
                                justifyContent="center"
                                alignItems="center"
                                sx={{
                                    width: '50px',
                                    height: '50px',
                                    backgroundColor: colors.teal_500,
                                    borderRadius: '50%',
                                    position: 'absolute',
                                    top: '5px',
                                    left: '5px',
                                }}
                            >
                                <PlusIcon width={24} height={24} fill="white" />
                            </Box>
                        </Box>
                    </Link>
                </Box>
                <Stack direction="row" justifyContent="space-around" sx={{ width: '40%', mr: 3 }}>
                    {leftMenu.map((data, idx) => (
                        <Link
                            to={data.path}
                            key={idx}
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}
                        >
                            <Box sx={{ textAlign: '-webkit-center' }}>
                                {location.pathname === data.path ? data.iconactive : data.icon}
                                {location.pathname === data.path ? (
                                    <Typography
                                        sx={{
                                            color: `${colors.teal_500} !important`,
                                        }}
                                    >
                                        {data.name}
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Box>
                        </Link>
                    ))}
                </Stack>
                <Stack direction="row" justifyContent="space-around" sx={{ width: '40%', ml: 3 }}>
                    {rightMenu.map((data, idx) => (
                        <Link
                            to={data.path}
                            key={idx}
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}
                        >
                            <Box sx={{ textAlign: '-webkit-center' }}>
                                {location.pathname === data.path ? data.iconactive : data.icon}
                                {location.pathname === data.path ? (
                                    <Typography
                                        sx={{
                                            color: `${colors.teal_500} !important`,
                                        }}
                                    >
                                        {data.name}
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Box>
                        </Link>
                    ))}
                </Stack>
            </Stack>
        </Box>
    )
}

export default BottomMenu
