import React, { useState } from 'react'
import { Box, Stack, TableCell, TableHead, TableRow } from '@mui/material'
import { SortDoubleArrow } from '../../assets/icons'
import { colors } from '../../themes'

interface TableHeaderProps {
    titles: Array<{ name: string; alias: string }>
    onSortChange: (alias: string, direction: string) => void
}

export const TableHeader = ({ titles, onSortChange }: TableHeaderProps) => {
    const [alias, setAlias] = useState('')
    const [sortDirection, setSortDirection] = useState('')

    const handleSortChange = (titleAlias: string, direction: string) => {
        setAlias(titleAlias)
        setSortDirection(direction === 'desc' ? 'asc' : 'desc')
        if (alias !== titleAlias) {
            onSortChange(titleAlias, 'asc')
            setSortDirection('desc')
        } else {
            onSortChange(titleAlias, direction)
        }
    }

    return (
        <TableHead>
            <TableRow>
                {titles.map((title, idx) => (
                    <TableCell key={idx}>
                        <Stack
                            direction="row"
                            alignItems="center"
                            className="cursor-pointer"
                            onClick={() => handleSortChange(title.alias, sortDirection)}
                        >
                            <Box>{title.name}</Box>
                            {title.name ? (
                                <Box sx={{ pl: 0.5 }}>
                                    <SortDoubleArrow width={15} height={14} fill={colors.teal_200} />
                                </Box>
                            ) : (
                                ''
                            )}
                        </Stack>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    )
}
