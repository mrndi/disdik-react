import React, { useState } from 'react'
import { Box, List, ListItem, ListItemIcon, Typography } from '@mui/material'
import { colors } from '../../themes'
import { Link, useLocation } from '@tanstack/react-location'



export interface MENU {
    path: string
    name: string
    icon: any
    iconactive: any
    isVisible: boolean
}

export interface menuListProps {
    menuList: Array<MENU>
}

export default function MenuList({ menuList }: menuListProps) {
    const location = useLocation().current

    const [isHover, setIsHover] = useState('')

    const toggleHover = (data: any) => {
        setIsHover(data)
    }

    return (
        <List sx={{ p: 0 }}>
            {menuList.map((data, idx) =>
                data.isVisible ? (
                    <ListItem
                        key={idx}
                        sx={{
                            width: '100%',
                            p: 0,
                            display: 'block',
                            '.active': {
                                width: '100%',
                                display: 'block',
                                borderRight: '5px solid' + colors.teal_500,
                                backgroundColor: colors.teal_100,
                                '.menuIcon,.menuText': {
                                    fontWeight: 'bold',
                                    color: colors.teal_700,
                                },
                            },
                            '&:hover': {
                                fontWeight: 'bold',
                                backgroundColor: colors.teal_100,
                                '.menuIcon,.menuText': {
                                    color: colors.teal_700,
                                },
                            },
                        }}
                        onMouseEnter={() => toggleHover(data.path)}
                        onMouseLeave={() => toggleHover(data.path)}
                    >
                        <Link
                            to={data.path}
                            className={location.pathname === data.path ? 'active' : ''}
                        >
                            <Box
                                sx={{
                                    p: '10px 20px',
                                    display: 'flex',
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    transition: 'all .2s ease-in-out',
                                }}
                            >
                                <ListItemIcon sx={{ minWidth: '40px' }}>
                                    {location.pathname === data.path || isHover === data.path
                                        ? data.iconactive
                                        : data.icon}
                                </ListItemIcon>
                                <Typography className="menuText" sx={{ color: colors.grey_900, fontSize: '16px' }}>
                                    {data.name}
                                </Typography>
                            </Box>
                        </Link>
                    </ListItem>
                ) : (
                    ''
                ),
            )}
        </List>
    )
}
