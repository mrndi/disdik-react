import { styled } from '@mui/material/styles'
import { Box } from '@mui/material'
import MuiDrawer from '@mui/material/Drawer'
import MenuList from './MenuList'
import { Home as HomeIcon, List as ListIcon, Report as ReportIcon, Users as UsersIcon } from '../../assets/icons'
import { colors } from '../../themes'

const drawerWidth = 220

const openedMixin = (theme: any) => ({
    width: drawerWidth,
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
})

const closedMixin = (theme: any) => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    width: '56px',
})

const Drawer = styled(MuiDrawer, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    flexShrink: 0,
    whiteSpace: 'nowrap',
    boxSizing: 'border-box',
    overflowX: 'hidden',
    ...(open && {
        ...openedMixin(theme),
        '& .MuiDrawer-paper': openedMixin(theme),
    }),
    ...(!open && {
        ...closedMixin(theme),
        '& .MuiDrawer-paper': closedMixin(theme),
    }),
}))

interface SideMenuProps {
    isDrawerOpen: boolean
}

export default function SideMenu({ isDrawerOpen }: SideMenuProps) {
    const isAdmin = true

    const MENU = [
        {
            name: 'Home',
            path: '/dashboard' || '/',
            icon: <HomeIcon width={20} height={20} fill={colors.grey_300} />,
            iconactive: <HomeIcon width={20} height={20} fill={colors.teal_500} />,
            isVisible: true,
        },
        {
            name: isAdmin ? 'Surat' : 'Surat Saya',
            path: '/surat',
            icon: <ListIcon width={20} height={20} fill={colors.grey_300} />,
            iconactive: <ListIcon width={20} height={20} fill={colors.teal_500} />,
            isVisible: true,
        },
        {
            name: 'Manage User',
            path: '/manage-user',
            icon: <UsersIcon width={20} height={20} fill={colors.grey_300} />,
            iconactive: <UsersIcon width={20} height={20} fill={colors.teal_500} />,
            isVisible: isAdmin,
        },
        {
            name: 'Booking Report',
            path: '/booking-report',
            icon: <ReportIcon width={20} height={20} fill={colors.grey_300} />,
            iconactive: <ReportIcon width={20} height={20} fill={colors.teal_500} />,
            isVisible: isAdmin,
        },
    ]

    return (
        <Box
            sx={{
                display: { xs: 'none', sm: 'block' },
                position: 'relative',
            }}
        >
            <Drawer
                PaperProps={{
                    style: {
                        top: '0',
                        position: 'absolute',
                        backgroundColor: 'white',
                        borderRadius: '5px',
                        height: '100%',
                        border: 'unset',
                    },
                }}
                variant="permanent"
                anchor="left"
                open={isDrawerOpen}
            >
                <MenuList menuList={MENU} />
            </Drawer>
        </Box>
    )
}
