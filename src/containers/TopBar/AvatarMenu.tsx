import React, { useState } from 'react'
import { Avatar, Box, Divider, Grid, Menu, MenuItem, Tooltip, Typography, IconButton } from '@mui/material'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import { Logout, User } from '../../assets/icons'
import { colors } from '../../themes'
import { useContext } from 'react'
import { userSignOut } from '../../helper/AuthHelper'
import { UserContext } from '../../App'
import { useNavigate, Link } from '@tanstack/react-location'

export default function AvatarMenu() {
    const userState = useContext(UserContext)
    const navigate = useNavigate()
    const [anchorEl, setAnchorEl] = useState() as any
    const [menuOpen, setMenuOpen] = useState(false)


    const handleAvatarMenuClick = (e: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(e.currentTarget)
        setMenuOpen(true)
    }


    const handleClose_AvatarMenu = () => {
        setMenuOpen(false)
    }

    const handleLogout = () => {
        localStorage.removeItem('signToken')
        window.location.href = '/'
    }

    const signOut = () => {
        handleLogout()
    }

    return (
        <>
            <Tooltip title="Account settings">
                <Box
                    sx={{
                        minHeight: '42px',
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        '&:hover': {
                            cursor: 'pointer',
                            button: {
                                backgroundColor: 'unset',
                                transition: 'unset',
                                borderRadius: 0,
                                '&:focus': {
                                    backgroundColor: 'unset',
                                },
                            },
                        },
                    }}
                    onClick={handleAvatarMenuClick}
                >
                    <IconButton size="small">
                        <Avatar sx={{ width: 32, height: 32 }} variant="rounded">
                            M
                        </Avatar>
                        <Typography
                            component="div"
                            sx={{
                                color: colors.grey_900,
                                ml: 1,
                                display: { xs: 'none', sm: 'block' },
                            }}
                        >
                            Hello, {userState?.name || ''}
                        </Typography>
                        <KeyboardArrowDownIcon
                            sx={{
                                olor: colors.grey_900,
                                display: { xs: 'none', sm: 'block' },
                            }}
                        />
                    </IconButton>
                </Box>
            </Tooltip>
            <Menu
                anchorEl={anchorEl}
                open={menuOpen}
                onClose={handleClose_AvatarMenu}
                onClick={handleClose_AvatarMenu}
                PaperProps={{
                    elevation: 0,
                    sx: {
                        minWidth: '180px',
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                        mt: 1.5,
                        'ul.MuiList-root': {
                            paddingTop: 0,
                            paddingBottom: 0,
                        },
                        '& .MuiAvatar-root': {
                            width: 32,
                            height: 32,
                            ml: -0.5,
                            mr: 1,
                        },
                        '&:before': {
                            content: '""',
                            display: 'none',
                            position: 'absolute',
                            top: 0,
                            right: 14,
                            width: 10,
                            height: 10,
                            bgcolor: 'background.paper',
                            transform: 'translateY(-50%) rotate(45deg)',
                            zIndex: 0,
                        },
                    },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >
                <MenuItem>
                    <Link to="my-profile">
                        <Grid container direction="row" alignItems="center" sx={{ pt: 0.7, pb: 0.7, pl: 0.3, m: 0 }}>
                            <Grid
                                item
                                xs="auto"
                                sx={{
                                    width: '30px',
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'flex-end',
                                }}
                            >
                                <User width={22} height={22} fill={colors.teal_500} />
                            </Grid>
                            <Grid item xs="auto">
                                <Typography
                                    sx={{
                                        color: colors.grey_900,
                                        paddingLeft: 1.5,
                                        paddingTop: 0.2,
                                    }}
                                >
                                    Edit Profile
                                </Typography>
                            </Grid>
                        </Grid>
                    </Link>
                </MenuItem>
                <Divider
                    sx={{
                        marginTop: '0 !important',
                        marginBottom: '0 !important',
                    }}
                />
                <MenuItem onClick={signOut}>
                    <Grid container direction="row" alignItems="center" sx={{ pt: 0.7, pb: 0.7, pl: 0.3, m: 0 }}>
                        <Grid
                            item
                            xs="auto"
                            sx={{
                                width: '30px',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'flex-end',
                            }}
                        >
                            <Logout width={22} height={22} fill={colors.teal_500} />
                        </Grid>
                        <Grid item xs="auto">
                            <Typography
                                sx={{
                                    color: colors.grey_900,
                                    paddingLeft: 1.5,
                                    paddingTop: 0.2,
                                }}
                            >
                                Logout
                            </Typography>
                        </Grid>
                    </Grid>
                </MenuItem>
            </Menu>
        </>
    )
}
