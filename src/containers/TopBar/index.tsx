import React, { useContext } from 'react'
import { AppBar, Box, Toolbar, Typography, IconButton } from '@mui/material'
import MenuIcon from '@mui/icons-material/Menu'
import AvatarMenu from './AvatarMenu'
import Notification from './Notification'
import { colors } from '../../themes'
import { UserContext } from '../../App'
import { Link } from '@tanstack/react-location'




export default function TopBar(onclick: any) {
    const userState = useContext(UserContext)

    const handleDrawerOpen = () => {
        onclick.onClick()
    }

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar
                position="static"
                sx={{
                    backgroundColor: 'white',
                    boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.06)',
                }}
                elevation={0}
            >
                <Toolbar>
                    <Box
                        sx={{
                            flex: 1,
                            display: 'flex',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                        }}
                    >
                        <IconButton
                            size="large"
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            onClick={handleDrawerOpen}
                            sx={{
                                display: { xs: 'none', sm: 'block' },
                                mr: 2,
                                color: colors.grey_900,
                            }}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Box
                            sx={{
                                display: { xs: 'block', sm: 'none' },
                            }}
                        >
                            <Typography variant='subtitle1'>Hello, {userState?.name || ''}</Typography>
                        </Box>
                    </Box>

                    <Box
                        sx={{
                            display: { xs: 'none', sm: 'flex' },
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: '100px',
                        }}
                    >
                        <Link to="/dashboard">
                            <Typography variant="h6" component="div" sx={{ flexGrow: 1, width: 200 }}>
                                DISDIK DOCUMENT
                            </Typography>
                        </Link>
                    </Box>

                    <Box
                        sx={{
                            flex: 1,
                            display: 'flex',
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                            gap: 1,
                        }}
                    >
                        <Notification />
                        <AvatarMenu />
                    </Box>
                </Toolbar>
            </AppBar>
        </Box>
    )
}
