import React, { useState } from 'react'
import { Box, Chip, Divider, Grid, Menu, Tooltip, Typography } from '@mui/material'
import { Notif } from '../../assets/icons'
import { colors } from '../../themes'
import { dataNotification } from '../../fake-db/notification'

export default function Notification() {
    const [anchorEl, setAnchorEl] = React.useState(null)
    const [notificationOpen, setNotificationOpen] = useState(false)
    const handleClick_Notification = (event: any) => {
        setAnchorEl(event.currentTarget)
        setNotificationOpen(true)
    }
    const handleClose_Notification = () => {
        setNotificationOpen(false)
    }

    return (
        <>
            <Tooltip title="Notification">
                <Box
                    onClick={handleClick_Notification}
                    sx={{
                        width: '40px',
                        minHeight: '42px',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        '&:hover': {
                            cursor: 'pointer',
                        },
                    }}
                >
                    <Notif width={22} height={22} fill={colors.grey_900} withBadge />
                </Box>
            </Tooltip>
            <Menu
                anchorEl={anchorEl}
                open={notificationOpen}
                onClose={handleClose_Notification}
                onClick={handleClose_Notification}
                PaperProps={{
                    elevation: 0,
                    sx: {
                        minWidth: '320px',
                        maxWidth: '320px',
                        maxHeight: '400px',
                        padding: '10px 20px',
                        borderRadius: '8px',
                        overflowY: 'auto',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                        mt: 1.5,
                        '& .MuiAvatar-root': {
                            width: 32,
                            height: 32,
                            ml: -0.5,
                            mr: 1,
                        },
                        '&:before': {
                            content: '""',
                            display: 'block',
                            position: 'absolute',
                            top: 0,
                            right: 14,
                            width: 10,
                            height: 10,
                            bgcolor: 'background.paper',
                            transform: 'translateY(-50%) rotate(45deg)',
                            zIndex: 0,
                        },
                    },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >
                <Grid container direction="row" justifyContent="space-between" alignItems="center" sx={{ p: 0, m: 0 }}>
                    <Grid item xs="auto">
                        <Typography fontWeight={700}>Notification</Typography>
                    </Grid>
                    <Grid item xs="auto">
                        <Chip
                            label="3 New"
                            sx={{
                                backgroundColor: colors.teal_700,
                                color: 'white',
                                height: '27px',
                                fontSize: '12px',
                            }}
                        />
                    </Grid>
                </Grid>

                {dataNotification.map((row, key) => (
                    <Box
                        sx={{
                            p: 0,
                            m: 0,
                            '&:last-child': {
                                '.MuiDivider-fullWidth': {
                                    display: 'none',
                                },
                            },
                        }}
                        key={key}
                    >
                        <Grid
                            container
                            direction="column"
                            justifyContent="space-between"
                            alignItems="flex-start"
                            sx={{ p: 0, m: 0 }}
                        >
                            <Grid item xs="auto" sx={{ mt: 1, mb: 0.5 }}>
                                <Typography>{row.title}</Typography>
                            </Grid>
                            <Grid item xs="auto" sx={{ mb: 0.5 }}>
                                <Typography>{row.description}</Typography>
                            </Grid>
                            <Grid item xs="auto">
                                <Typography sx={{ color: colors.grey_500 }}>{row.time}</Typography>
                            </Grid>
                        </Grid>
                        <Divider sx={{ mt: 0.8 }} />
                    </Box>
                ))}
            </Menu>
        </>
    )
}
