import { useEffect, useState } from 'react'
import { ThemeProvider } from '@mui/material/styles'
import { Box, Container, LinearProgress } from '@mui/material'
import TopBar from '../containers/TopBar'
import SideMenu from '../containers/SideMenu'
import BottomMenu from '../containers/BottomMenu'
import { mainTheme } from '../themes'
import { colors } from '../themes'
import { Outlet, useLocation } from '@tanstack/react-location'
import { SignIn, SignUp } from '../pages/auth/Auth'
import { getToken } from '../helper/AuthHelper'


export const AppLayout = () => {

    const location = useLocation().current
    const [isDrawerOpen, setDrawerOpen] = useState(true)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const token = getToken()
        if (!token) {
            setLoading(true)
        }
        const timer = setTimeout(() => setLoading(false), 1000);
        return () => clearTimeout(timer);
    }, []);

    const handleDrawerOpen = () => {
        setDrawerOpen(!isDrawerOpen)
    }

    const isSignin = location.pathname === '/signin'
    const isSignup = location.pathname === '/signup'

    if (isSignin) {
        return <SignIn />
    }

    if (isSignup) {
        return <SignUp />
    }

    if (loading === true) {
        return <LinearProgress color='success' />
    }

    if (getToken() === null) {
        return <SignIn />
    }

    return (
        <ThemeProvider theme={mainTheme}>
            <Box maxWidth="100%" overflow="hidden">
                {location.pathname === '/scan-success' ? '' : <TopBar onClick={handleDrawerOpen} />}
                <Box
                    sx={{
                        display: 'flex',
                        backgroundColor: colors.zircon_500,
                        minHeight: 'calc(100vh - 70px)',
                        paddingTop: '20px',
                        maxWidth: '100%',
                        justifyContent: 'space-between',
                    }}
                >
                    <SideMenu isDrawerOpen={isDrawerOpen} />
                    <Box
                        sx={{
                            flex: 1,
                            width: `calc(100% - ${isDrawerOpen ? '220px' : '56px'})`,
                            transition: 'all 0.3s ease-out',
                        }}
                    >
                        <Container maxWidth={false} sx={{ marginBottom: { xs: '90px', sm: 0 } }}>
                            <Outlet />
                        </Container>
                    </Box>
                </Box>
                <BottomMenu />
            </Box>
        </ThemeProvider>
    )
}
