import { api } from "../helper/ApiHelper"
import { useQuery } from "react-query";

export const getOutbox = (id: string) => {
    const outbox = useQuery(['outbox'], async () => {
        const response: any = await api.get(`/outbox/${id}`)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (outbox.isLoading) return null

    const iterate = [...outbox?.data?.data?.data?.suratkeluar, ...outbox?.data?.data?.data?.suratmasuk]

    return iterate
}