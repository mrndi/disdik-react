import { api } from "../helper/ApiHelper"
import { useQuery } from "react-query";


export const getSubbidang = () => {
   const subBidang = useQuery(['subbidang'], async () => {
      const response: any = await api.get("/subbidang")
      if (!response.ok) {
         throw new Error(response?.data?.message as string)
      }
      return response
   },
      {
         refetchOnWindowFocus: false,
         retry: false,
      }
   )

   if (subBidang.isLoading) return null
   return subBidang?.data?.data
}
