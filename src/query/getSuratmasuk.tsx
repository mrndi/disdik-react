import { api } from "../helper/ApiHelper"
import { useQuery } from "react-query";


export const getSuratmasukById = (id: string) => {
    const suratMasuk = useQuery(['suratMasuk', id], async () => {
        const response: any = await api.get(`/suratmasuk/${id}`)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (suratMasuk.isLoading) return null
    return suratMasuk?.data?.data
}

export const getSuratmasukByHolderId = (id: string) => {
    const suratMasuk = useQuery(['suratMasuk', id], async () => {
        const response: any = await api.get(`/suratmasuk/holded/${id}`)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (suratMasuk.isLoading) return null
    return suratMasuk?.data?.data
}

export const getSuratmasukBySenderId = (id: string) => {
    const suratMasuk = useQuery(['suratMasuk', id], async () => {
        const response: any = await api.get(`/suratmasuk/sended/${id}`)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (suratMasuk.isLoading) return null
    return suratMasuk?.data?.data
}