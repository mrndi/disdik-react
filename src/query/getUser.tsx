import { api } from "../helper/ApiHelper"
import { useQuery } from "react-query";


export const getUser = (id: string) => {
    const user = useQuery(['user'], async () => {
        const response: any = await api.get(`/user/${id}`)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (user.isLoading) return null
    return user?.data?.data
}
