import { api } from "../helper/ApiHelper"
import { useQuery } from "react-query";

export const getSuratkeluarById = (id: string) => {
    const suratKeluar = useQuery(['suratKeluar', id], async () => {
        const response: any = await api.get(`/suratkeluar/${id}`)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (suratKeluar.isLoading) return null
    return suratKeluar?.data?.data
}

export const getSuratkeluarByHolderId = (id: string) => {
    const suratKeluar = useQuery(['suratKeluar', id], async () => {
        const response: any = await api.get(`/suratkeluar/holded/${id}`)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            staleTime: 1000 * 60 * 60 * 24,
            retry: false,
        }
    )

    if (suratKeluar.isLoading) return null
    return suratKeluar?.data?.data
}

export const getSuratkeluarBySenderId = (id: string) => {
    const suratKeluar = useQuery(['suratKeluar', id], async () => {
        const response: any = await api.get(`/suratkeluar/sended/${id}`)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (suratKeluar.isLoading) return null
    return suratKeluar?.data?.data
}