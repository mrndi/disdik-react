import { api } from "../helper/ApiHelper"
import { useQuery } from "react-query";

export const getDisposisi = (typeSurat: string, id: string) => {
    const disposisi = useQuery(['disposisi'], async () => {
        const response: any = await api.get(`/disposisi/${typeSurat}/${id}`)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (disposisi.isLoading) return null



    return disposisi.data?.data?.data
}