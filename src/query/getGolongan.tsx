import { api } from "../helper/ApiHelper"
import { useQuery } from "react-query";


export const getGolongan = () => {
    const golongan = useQuery(['golongan'], async () => {
        const response: any = await api.get("/golongan")
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (golongan.isLoading) return null
    return golongan?.data?.data
}
