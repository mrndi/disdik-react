import { api } from "../helper/ApiHelper"
import { useQuery } from "react-query";


export const getRoles = () => {
    const roles = useQuery(['roles'], async () => {
        const response: any = await api.get("/roles")
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (roles.isLoading) return null
    return roles?.data?.data
}
