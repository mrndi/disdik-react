import { api } from "../helper/ApiHelper"
import { useQuery } from "react-query";

export const getInbox = (id: string) => {
    const inbox = useQuery(['inbox'], async () => {
        const response: any = await api.get(`/inbox/${id}`)
        if (!response.ok) {
            throw new Error(response?.data?.message as string)
        }
        return response
    },
        {
            refetchOnWindowFocus: false,
            retry: false,
        }
    )

    if (inbox.isLoading) return null

    const iterate = [...inbox?.data?.data?.data?.suratkeluar, ...inbox?.data?.data?.data?.suratmasuk]

    return iterate
}